'use strict';

var a = -5;
var b = -2;
var c = -6;
var d = 5;
var e = -1;

if (a > b && a > c && a > c && a > d && a > e) {
  document.write('größte Zahl: ' + a);
} else if (b > a && b > c && b > d && b > e) {
  document.write('größte Zahl: ' + b);
} else if (c > a && c > b && c > d && c > e) {
  document.write('größte Zahl: ' + c);
} else if (d > a && d > b && d > c && d > e) {
  document.write('größte Zahl: ' + d);
} else {
  document.write('größte Zahl: ' + e);
}

////////// ecercise 03///////////////////////////////////////

var html = '<h3>verschiedene Zeichenfolgen</h3>';
document.write(html + '<hr>');

// 1,2,3,4,5,6,7,8,9,10
var zahlenfolge = '<p>Zahlenfolge: </p>';
var i = 1;
while (i < 10) {
  zahlenfolge += i + ', ';
  i++;
}
document.write(zahlenfolge += i + '<hr>');

// 2,4,6,8,10
var zahlenfolge = '<p>Zahlenfolge: </p>';
var j = 2;
// while ((j < 10) && ((j+2) <= 10)) {
while (j < 10) {
  zahlenfolge += j + ', ';
  j = j + 2;
}
document.write(zahlenfolge += j + '<hr>');

// 49,42,35,28,21
var zahlenfolge = '<p>Zahlenfolge: </p>';
var k = 49;
while (k > 21) {
  zahlenfolge += k + ', ';
  k = k - 7;
}
document.write(zahlenfolge += k + '<hr>');

// 1,2,4,8,16,32,64,128,256,512,1024
var zahlenfolge = '<p>Zahlenfolge: </p>';
var l = 1;
while (l*2 <= 1024) {
  zahlenfolge += l + ', ';
  l = l * 2;
}
document.write(zahlenfolge += l + '<hr>');



// Von 1 bis n zählen // n = Nutzereingabe
var zahlenfolge = '<p>Zahlenfolge: </p>';
var userinput = prompt('Bitte Zahl eingeben: ' , '6');
var m = 1;
while (m < userinput) {
  zahlenfolge += m++ + ', '
}
document.write(zahlenfolge += m + '<hr>');


// 1,2,4,8,16,32,64,128,256,512,1024
var zahlenfolge = '<p>Zahlenfolge: </p>';
for (i = 1; i <= 1024; i = i * 2 ) {

  zahlenfolge += i + ', ';
}
document.write (zahlenfolge += '<hr>');


// 49,42,35,28,21
var zahlenfolge = '<p>Zahlenfolge: </p>';
for (i = 49; i >= 21; i = i - 7 ) {
  zahlenfolge += i + ', ';
}
document.write (zahlenfolge += '<hr>');


// 49,42,35,28,21 // var 2
var zahlenfolge = '<p>Zahlenfolge: </p>';
var fac1;
var fac2 = 7;
for (fac1 = 7; fac1 >=3; fac1-- ) {
  zahlenfolge += fac1 * fac2 + ', ';
}
document.write (zahlenfolge += '<hr>');


// Von 1 bis n zählen // n = Nutzereingabe // FOR-Schleife
// var zahlenfolge = '<p>Zahlenfolge: </p>';
// var n = prompt('Bitte Zahl eingeben:' , '');
// for ( i = 1 ; i < n ; i++) {
//   zahlenfolge += i + ', ';
// }
// console.log(zahlenfolge += + '<hr>');
// document.write(zahlenfolge += + '<hr>');
var test;
for(var i = 0; i > 4; i++) {
  document.write (test += i + '<hr>');

}


////////// ecercise 05 ///////////////////////////////////////

///// 01 a /////

var html;
var haustiere = [
  'Hund', // => 0
  'Katze', // => 1
  'Adler', // => 2
  'Meerschweinchen', // => 3
  'Hamster', // => 4
  'Amsel', // => 5
  'Maus', // => 6
  'Ratte',// => 7
  'Fink', // => 8
  'Kaninchen' // => 9
  ];

html = haustiere[3] + ', ' + haustiere[4] + ', ' + haustiere[6] + ', ' + haustiere[7];

console.log(html);
document.write(html);

///// 01 b /////

var html = '';
var federvieh = [];
for(var i = 2 ; i < haustiere.length ; i += 3) {
  html += federvieh.push(haustiere[i]);
}
html += federvieh.join(', '); // stringkette aus array
document.write(html);

///// 01 c /////

// zufälliges item erzeugen:

var randomHaustier = haustiere[Math.floor(Math.random()*haustiere.length)];
console.log(randomHaustier);
document.write('<hr>');
document.write('Zufallstier: ' + randomHaustier + '<br>');

// wieviel Elemente hat array?
console.log(haustiere.length);
document.write('Anzahl Tiere: ' + haustiere.length + '<br>');
document.write(haustiere.lastItem + '<br>');

// was ist das letzte Element?
var lastHaustier = haustiere[haustiere.length - 1]; // 6

// 02 Pferdeboxen ////////////////////////////////////////////////

var pferdeboxen = [];
pferdeboxen.length = 10;
console.log(pferdeboxen);
document.write(pferdeboxen + '<br>');

pferdeboxen.splice(0,1,'Peggy');
pferdeboxen.splice(3,1,'Moritz');
pferdeboxen.splice(9,1,'Dangerous');

var len = pferdeboxen.length; 

for (var i = 0 ; i <len ; i++) {
  console.log(pferdeboxen[i]);
  if (pferdeboxen[i] == undefined) {
  pferdeboxen[i] = 'Y';
  }
}
console.log(pferdeboxen);


html = '';
for (var i = 0; i < len; i++) {
  html += 'In Pferdebox ' + (i + 1) + ' steht Pferd namens ' + pferdeboxen[i] + '<br>';
  
}
console.log(html);
document.write(html);


// Aufgabe 03 - Namen /////////////////////////////////////////////////////////

var namen = [ 
  1,
  'Bernd', 
  2, 
  'Anna', 
  3, 
  'Michael', 
  4, 
  'Katharina', 
  5, 
  'Frank', 
  6, 
  'Susanne' 
  ];

  var nameOnly = [];
  var len = namen.length;
  html = '';

// mit FOR
for (let i = 1 ; i < len ; i += 2) {
  nameOnly.push(namen[i]);
}
console.log(nameOnly);
document.write(nameOnly + '<br>');

// mit FOR und Prüfung auf Integer
for (let i = 1 ; i < len ; i++) {
  if (!Number.isInteger(namen[i]) ) {
    html += namen[i] + '<br>';
  }
}

// mit WHILE
var i = 1;
  while (i < len) {
    html += namen[i] + '<br>';
    i = i + 2;
  }
  document.write(html + '<br>');

// Aufgabe 04 - Würfel /////////////////////////////////////////////////////////

var len = 2000;
var html = '';

var dice = Math.floor((Math.random() * 6) + 1);
var results = [0,0,0,0,0,0]; // mit 0 vorbelegen, da später dazuaddiert wird
// man kann nur zu einem als Zahl definierten Wert addieren, ansonsten NaN !!!


for (let i = 0 ; i < len ; i++) {
  dice = Math.floor((Math.random() * 6) + 1);
  /// IF Variante
  if (dice == 1) {
    results[0]++; // bei jedem 1er Wurf wird Anzahl der Würfel mit 1 um 1 erhöht
  }
  if (dice == 2) {
    results[1]++; // bei jedem 2er Wurf wird Anzahl der Würfel mit 2 um 1 erhöht
  }
  if (dice == 3) {
    results[2]++;
  }
  if (dice == 4) {
    results[3]++;
  }
  if (dice == 5) {
    results[4]++;
  }
  if (dice == 6) {
    results[5]++;
  }
}
// /// SWITCH Variante
// switch(dice) {
//   case 1:
//     results[0]++;
//     break
//   case 2:
//     results[1]++;
//     break
// }

console.log(results);
document.write(results  + '<br>');


for (let i = 0 ; i < 6 ; i++) {
  html += 'Augenzahl ' + (i + 1) + ': ' + results[i] + '<br>'; 
}
document.write(html);


//////////////////////// Index mit größter Zahl:
// var maxValue = Math.max(results); // warum geht das nicht?

var maxValue = Math.max.apply(Math, results);
document.write(maxValue + '<br>');
console.log(maxValue);

var count = 0;
for(var i = 0; i < results.length; ++i) {
    if(results[i] == maxValue)
        count++;
}

document.write(count + '<br>' + '<hr>');


var position = 0; // Startpunkt in Schleife bei jedem Durchlauf erhöhen
html = html + '<hr>';

for (let i = 0; i < count; i++)
{
    position = results.indexOf(maxValue, position);
    html = html + 'Augenzahl ' + (position+1) + ' kam ' + maxValue + ' Mal vor.' + '<br>';
    position ++;
}
document.write(html + '<hr>');

//////////////////////// Index mit kleinster Zahl:

var minValue = Math.min.apply(Math, results);
document.write(maxValue + '<br>');
console.log(maxValue);

var count = 0;
for(var i = 0; i < results.length; ++i) {
    if(results[i] == minValue)
        count++;
}

document.write(count + '<br>' + '<hr>');


var position = 0; // Startpunkt in Schleife bei jedem Durchlauf erhöhen
html = html + '<hr>';

for (let i = 0; i < count; i++) {
    position = results.indexOf(minValue, position);
    html = html + 'Augenzahl ' + (position+1) + ' kam ' + minValue + ' Mal vor.' + '<br>';
    position ++;
}
document.write(html + '<hr>');


////////////// Lottozahlen /////////////////////////////////////////////////
document.write('Lottozahlen:' + '<br>');

// var max = 49;
// var number = Math.floor((Math.random() * max) + 1);
// document.write(number);
// var balls = [];

// var lottery = [];
// lottery.length = 6;
// var len;

// var i = 1;
// while (i < lottery.length) {
 
//   lottery.push(number);
//   i++;
// }
// document.write(lottery);
// console.timeLog(lottery);

///// testen //////////////

// var max = 49;
// var number = Math.floor((Math.random() * max) + 1);

///////////////////////////////////////////////////////////

// function generatenumbers() {
//   var max = 49;
//   var number = Math.floor((Math.random() * max) + 1);
//   var lotto = new Array();

//   while (lotto.lenght < 6) {
//     if (!lotto.includes(number)) {
//       lotto.push(number);
//     }
//   }
//   console.log(number);
// }

///////////////////////////////////////////////////////////

  var max = 49;
  var lotto = [];
  while (lotto.length < 6) {
    var number = Math.floor((Math.random() * max) + 1);
    if (!lotto.includes(number)) {
      lotto.push(number);
    }
  }
  console.log(lotto);

///////////////////////////////////////////////////////////////

// Stefans Lösung
var kugeln = [];
var kugel;
var anzKugeln = 10;
var anzZiehungen = 6;
for (var i = 0; i < anzZiehungen; i++) {
        kugel = Math.floor((Math.random() * anzKugeln) + 1);
        for (let j = 0; j < kugeln.length; j++) {
            if (kugel == kugeln[j]) {
                kugel = Math.floor((Math.random() * anzKugeln) + 1);
                j=1;
            }
        }
        kugeln[i] = kugel;
        html = html + "Kugel " + (i + 1) + ": " + kugel + "<br>"
    }
    console.log(kugeln);
// Ende Stefans Lösung

///////////////////////////////////////////////////////////////

// 1 bis 49 in Array schreiben:
var allnumbers = [];
for (i = 1 ; i <= 49 ; i++) {
  allnumbers.push(i);
}
console.log(allnumbers);

////////////////////////////////////////////////////////////////

document.write('<hr');

////////////////////////////////////////////////////////////////

// Funktion: Zahlenfolge von ... bis in Array schreiben:

function numberSeries(lastnumber) {
  var array = [];
  
  for (var i = 1 ; i <= lastnumber ; i++) {
    array.push(i);
  }
  return array;
}
var result = numberSeries(60);

// var result = [];

//////////////////////////////////////////////////////////////////////


// function foobar1 (afoobar) {
//   return afoobar+'blabla';
// }
// var testfoobar = 'hierstehteintext';
// var foobartmp;

// console.log(testfoobar);

// foobattmp = foobar1(lalala);

// console.log(testfoobar);

//////////////////////////////////////////////////////////////////////

// function foobar2 (afoobar) {
//   afoobar.pop();
//   afoobar.pop();
// }
// var array = ['hund' , 'katze' , 'maus'];
// console.log(array);
// foobar2 (array);
// console.log(array);

//////////////////////////////////////////////////////////////////////

function square(x) {
  
}