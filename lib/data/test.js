alert('Test!');

var data = {
  x1: 12,
  x2: 30,
};

(function (window, document) {
  var myData = { z: 'text' };
  /* x wird als Eigenschaft im document-Objekt angelegt */
  document.x = myData;
})(window, document);

var global = 'Inhalt';
console.log(window.global);

document.meineEigenschaft = 'Test';
/* Alle globalen Variablen sind automatisch Eigenschaft im window-Objekt.
Man kann jedem JS-Objekt, auch Typ-Element, beliebig freiwählbare Eigenschaften geben. Der Browser stellt eine Eigenschaft die es nicht als DOM-Property bzw. Attribut gibt nicht im Quelltext bzw. im Inspektor. */
var key = 'mykey' + Date.now();
document.body[key] = { vorname: 'Jon', nachname: 'Snow' };

function testFunktion(a) {
  console.log(a);
}
