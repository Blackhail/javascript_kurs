<?php
/**
* Dateiname: pathfinder.php
* 28.05.2019
* Version: 1
*/

// echo '<pre>';
// var_dump( $_SERVER );
// echo '</pre>';

/* Wurde die Seite über AJAX mit XHR-Objekt angefragt oder direkt über das GET-Protokol im Browser geöffnet? */
$ajax = isset( $_SERVER['HTTP_X_REQUESTED_WITH'] )
&& strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) === 'xmlhttprequest';

/* AJAX-Anfrage $ajax === true
GET-Aufruf der URL $ajax === false */
// var_dump ( $ajax );

/* Wenn es keine AJAX-Anfrage ist den Code innerhalb der geschweiften Klammern ausgeben.
Bei einer AJAX-Anfrage wird der Inhalt nicht erzeugt.  */
if ( !$ajax ) {
    ##############################################################################################
    /* Folgender HTML-Block wird nicht bei einer AJAX-Anfrage erzeugt. */
    ?>
<!--
    Kopf- und Fußbereich können später über include eingefügt werden oder mit Funktionen erzeugt.
    Alle Inhalte von  <!DOCTYPE html> bis <body> werden beim include in eine externe Datei ausgelagert. include 'kopfbereich.inc.php';
    Die inkludierten Dateien können reine HTML oder PHP-Dateien sein.
    -->
<!DOCTYPE html>
<html lang='de'>

<head>
  <meta charset='UTF-8'>
  <title>Pathfinder ( pathfinder.php )</title>
  <link rel='stylesheet' href='../css/main.css'>
</head>

<body>
  <div id='main'>
    <?php
}
// end if ( !$ajax ) {
##############################################################################################
?>

    <h1>Pathfinder</h1>
    <p>
      Das
      <a href='sortiment/rollenspiele/was-ist-rollenspiel/'>Rollenspiel</a>
      <em>Pathfinder</em> ist eine Weiterentwicklung von
      <em>Dungeons &amp;
        Dragons</em>, dem bekanntesten Rollenspiels der Welt. Es wurde von Zehntausenden Spielern ausführlich
      getestet, verbessert und für gut befunden. Für den deutschsprachigen Markt erscheint
      <em>Pathfinder</em> exklusiv bei Ulisses Spiele. Die zeitnahe und hochwertige Bearbeitung ist das Werk eines
      ebenso enthusiastischen
      wie eingespielten Übersetzerteams.
    </p>
    <p>
      In einer klassischen
      <a href='sortiment/rollenspiele/pathfinder/die-welt-golarion/'>Fantasy-Welt</a> wagen Sie sich an
      geheimnisumwitterte Orte, schlagen grimmige Schlachten und erleben selbst dann noch
      epische Abenteuer, &nbsp;
      wenn Bücher oder Computerspiele schon lange zu Ende wären.</p>
    <p>
      Ein Tipp: Dank ihrer vereinfachten Regeln und ihrem Fokus auf aktionsgeladene Abenteuer ist die
      <a href='sortiment/rollenspiele/pathfinder/produkte/'>
        <em>Pathfinder-Einsteigerbox</em>
      </a> ideal für erste Schritte in die Welt des
      <em>Pathfinder</em>-Rollenspiels und der beste Ausgangspunkt für ein ( Helden- ) Leben voller turbulenter
      Abenteuer!</p>
    <p>
      Einen Überblick über die verschiedenen Produktreihen und ihre Aufgliederung finden Sie
      <a href='http://www.ulisses-spiele.de/forum/viewtopic.php?f=84&amp;t=714#p8799' target='_blank'>hier </a>in
      unserem Forum.</p>
    <h2>
      Charakterbögen</h2>
    <p>
      <a href='http://www.ulisses-spiele.de/download/39/' target='_blank'>Charakterbogen der Einsteigerbox</a>
    </p>
    <p>
      <a href='http://www.ulisses-spiele.de/download/161/' target='_blank'>Offizieller Charakterbogen</a>
    </p>
    <p>
      <a href='http://www.ulisses-spiele.de/download/162/' target='_blank'>Charakterbogen zum Ausfüllen am Computer</a>
    </p>
    <p>
      <a href='http://www.ulisses-spiele.de/download/163/' target='_blank'>Charakterbogen zum Ausfüllen am Computer -
        mit Berechnung!</a>
    </p>

    <?php
if ( !$ajax ) {
    ##############################################################################################
    /* Folgender HTML-Block wird nicht bei einer AJAX-Anfrage erzeugt. */
    ?>
    <!--
    Kopf- und Fußbereich können später über include eingefügt werden oder mit Funktionen erzeugt.
    Alle Inhalte von  <noscript> bis </body></html>werden beim include in eine externe Datei ausgelagert. include 'fussbereich.inc.php';
    Die inkludierten Dateien können reine HTML oder PHP-Dateien sein.
    -->
  </div>
  <noscript><b>Bitte aktivieren Sie JavaScript um den vollen Funktionsumfang der Seite nutzen zu können.</b></noscript>
</body>

</html>
<?php
}
// end if ( !$ajax ) {
##############################################################################################
?>