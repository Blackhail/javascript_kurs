-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 09. Apr 2020 um 10:40
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `kunde`
--
CREATE DATABASE IF NOT EXISTS `kunde` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `kunde`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `personal`
--

CREATE TABLE `personal` (
  `id` int(11) NOT NULL,
  `vorname` varchar(50) COLLATE utf8_german2_ci NOT NULL,
  `nachname` varchar(150) COLLATE utf8_german2_ci NOT NULL,
  `personalnummer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

--
-- Daten für Tabelle `personal`
--

INSERT INTO `personal` (`id`, `vorname`, `nachname`, `personalnummer`) VALUES
(1, 'Taylor', 'Davis', 2223),
(2, 'Peter', 'Hollins', 2356),
(3, 'Max', 'Herre', 5687),
(4, 'Lindsey', 'Stirling', 2323),
(5, 'Vanessa', 'Mai', 2563);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `personal`
--
ALTER TABLE `personal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
