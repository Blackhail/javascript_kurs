  <?php
  /* $events könnte auch aus einer Datenbankanfrage erzeugt worden sein. */
  $events = [
    'play' => [
      'headline' => 'PLAY – Creative Gaming Festival 2018',
      'text' => 'Unter dem Motto „Ready Game Change – Create a New Tomorrow“ findet das Games-Festvial zum elften Mal in Hamburg statt – inklusive Ausstellungen, Vorträgen und der Verleihung des „Creative Gaming Award“ an die originellsten Indie-Spiele (Bewerbungsfrist endet am 31.7.2018). Veranstalter sind die Initiative Creative Gaming e. V: und der Verein für
    medienpädagogische Praxis Hamburg e. V. (jaf)'
    ],
    'game' => [
      'headline' => 'GAME ON: Arbeitest du noch oder spielst du schon?',
      'text' => 'Wie der deutsche Mittelstand von Gamification und Serious Games profitieren kann, thematisiert die Konferenz „GAME ON“. Anhand anschaulicher Beispiele und „Erlebnisstationen“ zeigen Unternehmen, welche digitalen Lösungen sich in der Praxis bewährt haben. Die Veranstaltung wird von Bundeswirtschaftsminister Peter Altmaier eröffnet – die Keynote hält
    Philipp Reinartz von Pfeffermind Consulting. Nach vorheriger Anmeldung ist die Teilnahme kostenlos'
    ],
    'loot' => [
      'headline' => 'Loot für die Welt 5 – LFDW5',
      'text' => 'Fünf der bekanntesten deutschen Youtuber (LeFloid, Frodo, RobBubble und die beiden Spacefrogs) nutzen zum fünften Mal die vorweihnachtliche Spendenbereitschaft, um ihre vielen Millionen Fans für die gute Sache zu begeistern. Am 48-Stunden-Live-Stream beteiligen sich Sponsoren und prominente Gäste. Wird der Vorjahres-Erlös von 239.000 Euro übertroffen?'
    ],
    'clash' => [
      'headline' => 'Clash of Realities 2018',
      'text' => 'Die neunte Auflage der englischsprachigen Konferenz besteht aus einer Eröffnungsveranstaltung inklusive Keynote von Carolyn Petit zum Thema „Gender and Sexuality“, einem „Summit Day“ und einem Konferenz-Tag mit Vorträgen und Präsentationen. Zugesagt haben unter anderem Videospieltheoretiker Jesper Juul und weitere namhafte Wissenschaftler aus der ganzen Welt'
    ]
  ];

  /* Wenn die Seite direkt aufgerufen wird. Als keine Eigenschaft term im $_REQUEST liegt werden alle Datensätze aus dem Array $events ausgegeben. Es wird das gleiche wie in events.html vom Server geliefert. */
  if (empty($_REQUEST['term'])) {
    foreach ($events as $id => $event) {
      echo '<div id="' . $id . '">';
      echo '<h2>' . $event['headline'] . '</h2>';
      echo '<p>' . $event['text'] . '</p>';
      echo '</div>';
    }
  } /* Es wurde ein Query-String  übergeben und der übergebene Wert befindet sich als Schlüssel in unserem assoziativen Array.*/ else if (!empty($_REQUEST['term']) && in_array($_REQUEST['term'], array_keys($events))) {
    $event = $events[$_REQUEST['term']];
    echo '<h2>' . $event['headline'] . '</h2>';
    echo '<p>' . $event['text'] . '</p>';
  } /* In allen anderen Fälle geben wir eien Fehlermeldung aus */ else {
    echo '<b>Eintrag nicht gefunden</b>';
  }


  ?>