<?php
  /* Hier steht später eine Datenbankabfrage */
  $file = file_get_contents('person.json');
  $data = json_decode($file);
  var_dump($data);

  echo '<h2>JSON-Objekt in PHP ausgeben</h2>';
  echo $data->Vorname . '<br>';
  echo $data->Nachname . '<br>';

  echo '<h2>foreach</h2>';
  echo '<ul>';
  foreach($data as $key => $value) {
    echo '<li>' . $key . ': '. $value . '</li>';
  }
  echo '</ul>';
  echo '<hr>';

  echo '<h2>AJAX-Anfrage verarbeiten</h2>';
  if(isset($_GET['term'])) {
    $key = $_GET['term']; //=> 'Vorname'
    echo $key . ' : ' .  $data -> $key . '<br>';
  }

?>