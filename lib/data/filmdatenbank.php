<?php
/* Beispieldarstellung vereinfacht. Später Datenbankanfrage */
  $genres = array(
    'Science Fiction' => array(
      'Star Trek', 
      'Star Wars', 
      'Alien'
    ),
    'Drama' => array(
        'Les Amant de Pont Neuf', 
        'War & Peace', 
        'Bridehead Revisited'
      ),
    'Crime' => array(
      'Heat', 
      'Pulp Fiction', 
      'Messerine'
      )
    );

    /* input1 muss im Query-String vorhanden sein.
    Beim Formular ist es automatisch durch den Feldnamen input[name=input1]vordefinert. */
    if(isset($_GET['input1'])) {
      $html = '';
      $search = $_GET['input1'];
      foreach($genres as $key => $value) {
        if( strtolower($key) == strtolower($search)) {
          /* implode ist in PHP das was wir in JS mit join machen. */
          $html .= '<ul><li>' . implode('</li><li>', $value) . '</li></ul>';
        }
      }

      if($html != '') {
        echo $html;
      } else {
        echo '<p><b>Kein Genre gefunden</b></p>';
      }
    }

?>