;(function (window, document) {
  window.addEventListener('load', function (e) {
    /* MAGIC: jQuery oder doch kein jQuery!? */
    var $ = document.querySelector.bind(document);
    var $$ = document.querySelectorAll.bind(document);

    function copyStringToClipboard(str) {
      // Temporäres Element erzeugen
      var el = document.createElement('textarea');
      // Den zu kopierenden String dem Element zuweisen
      el.value = str;
      // Element nicht editierbar setzen und aus dem Fenster schieben
      el.setAttribute('readonly', '');
      el.style = {
        position: 'absolute',
        left: '-9999px'
      };
      document.body.appendChild(el);
      // Text innerhalb des Elements auswählen
      el.select();
      // Ausgewählten Text in die Zwischenablage kopieren
      document.execCommand('copy');
      // Temporäres Element löschen
      document.body.removeChild(el);
    }

    function copy(e) {
      var code;
      // WENN das PRE-Element angeklickt wurde 
      if (e.target.nodeName === 'PRE') 
      {
        // Vom PRE-Element auf das Kindelement CODE gehen
        code = e.target.getElementsByTagName('code')[0];
      }  // WENN das CODE-Element angeklickt wurde 
      else if (e.target.nodeName === 'CODE') 
      { 
        //  Mit dem angeklickten Element CODE arbeiten
        code = e.target;
      }
      // Inhalt des CODE-Elements auslesen und von Leerzeichen (links&rechts) befreien
      code = code.textContent.trim();
      // Über Hilfsfunktion den String in die Zwischenablage (Clipboard) kopieren
      copyStringToClipboard(code);
      // Text zur Ansicht in die Konsole ausgeben
      console.log('Kopierter Text: ', code);

    }

    //  Alle PRE-Element einlesen und einen click-Eventhandler binden
    $$('pre').forEach(function (element) {
      element.addEventListener('click', copy);
    });

  });

})(window, document);