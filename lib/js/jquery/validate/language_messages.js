
function messageLang () {

      var languageFile;
      var userLang;
      if ($('html').attr('lang')) {
        userLang = $('html').attr('lang')
      } else {
        userLang = navigator.language || navigator.userLanguage;
      }
  
      switch (userLang.toLowerCase()) {
        case 'en':
        case 'en-en':
        case 'en-au':
        case 'en-bz':
        case 'en-ca':
        case 'en-ie':
        case 'en-jm':
        case 'en-nz':
        case 'en-ph':
        case 'en-za':
        case 'en-tt':
        case 'en-gb':
        case 'en-zw':
          /* Englisch ist Standard, keine Einbindung erforderlich. */
          break;
        case 'de':
        case 'de-de': // 'de-De'
        case 'de-at':
        case 'de-li':
        case 'de-lu':
        case 'de-ch':
          languageFile = 'messages_de.min.js';
          break;
        case 'ar':
        case 'ar-dz':
        case 'ar-bh':
        case 'ar-eg':
        case 'ar-iq':
        case 'ar-jo':
        case 'ar-kw':
        case 'ar-lb':
        case 'ar-ly':
        case 'ar-ma':
        case 'ar-om':
        case 'ar-sa':
        case 'ar-sy':
        case 'ar-tn':
        case 'ar-ae':
        case 'ar-ye':
          languageFile = 'messages_ar.min.js';
          break;
      }
      /* Englisch: languageFile ist undefined kein externe Datei einbinden. */
      if (languageFile) {
        // document.write('<script src="localization/' + languageFile + '"><\/script>');
        document.write('<script src="../../../lib/js/jquery/validate/localization/' + languageFile + '"><\/script>');
        
      }
}
