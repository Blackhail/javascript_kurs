(function (window, document) {
    'use strict';
  
    function changeImage(e) {
      var img = document
        .getElementById('mainimage')
        .getElementsByTagName('img')[0];
      var imgUnderMouse = this;
      img.src = imgUnderMouse.src;
    }
  
    function markLinkAsActive(clickedButton) {
      var active = document.getElementsByClassName('active')[0];
      if (active) {
        active.classList.remove('active');
      }
      clickedButton.classList.add('active');
    }
  
    function generateHTML(ssd) {
      var info = document.getElementById('info');
      while (info.lastChild) {
        info.lastChild.remove();
      }
  
      var h2 = document.createElement('h2');
      var txt = document.createTextNode(ssd.headline);
      h2.appendChild(txt);
  
      info.appendChild(h2);
  
      var ul = document.createElement('ul');
      var li;
      var value;
      for (var key in ssd) {
        value = ssd[key];
        if (key !== 'headline') {
          li = document.createElement('li');
          txt = document.createTextNode(key + ': ' + value);
          li.appendChild(txt);
          ul.appendChild(li);
        }
      }
  
      info.appendChild(ul);
      var array = Object.entries(ssd);
      var key, value, len = array.length;
      for (var i = 1; i < len; i++) {
        key = array[i][0];
        value = array[i][1];
        console.log(key, value);
      }
    }
  
    function findSSD(clickedButton, article) {
      var buttons = document
        .getElementById('switcher')
        .getElementsByTagName('button');
      var index = Array.from(buttons).indexOf(clickedButton);
      return article[index];
    }
  
  
    function requestAjax(thisPointer) {
      var xhr = new XMLHttpRequest(); // Request in Variable speichern
      console.log(xhr);
      xhr.open('GET', '../lib/data/data.json', true); // setzt request-Methode, url, true=asynchron)
      xhr.onreadystatechange = function () { // was passiert, wenn sich Status der Anfrage ändert
        if (xhr.readyState === 4 && xhr.status === 200) { //  readyState gibt den Status zurück, in dem sich ein XMLHttpRequest-Client befindet
          console.log('AJAX readyState 4:', (new Date()).getMilliseconds());
          var array = JSON.parse(xhr.responseText);
          var ssd = findSSD(thisPointer, array);
          generateHTML(ssd);
        }
      };
      xhr.send(); // leitet Request ein
    }
  
  
    function handleClick(e) {
      if (!this.classList.contains('active')) {
        markLinkAsActive(this);
        // var ssd = findSSD(this);
        // generateHTML(ssd);
        requestAjax(this);
        console.log('AJAX Methode send() aufgerufen: ', (new Date()).getMilliseconds());
  
      }
    }
  
    function bindClickHandler() {
      var buttons = document
        .getElementById('switcher')
        .getElementsByTagName('button');
      var button, len = buttons.length;
      for (var i = 0; i < len; i++) {
        button = buttons[i];
        button.addEventListener('click', handleClick);
      }
    }
  
  
    function bindMouseHandler() {
      var images = document
        .getElementById('vorschau')
        .getElementsByTagName('img');
      var img, len = images.length;
      for (var i = 0; i < len; i++) {
        img = images[i];
        img.addEventListener('mouseover', changeImage);
      }
    }
  
    window.addEventListener('load', function () {
      bindMouseHandler();
      bindClickHandler();
    });
  })(window, document);
  
  
  
  ///////////////////////////////////////////////////////////////////////////////////////
  /*
    xhr.readyState
    https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState
    0	UNSENT	Client has been created. open() not called yet.
    1	OPENED	open() has been called.
    2	HEADERS_RECEIVED	send() has been called, and headers and status are available.
    3	LOADING	Downloading; responseText holds partial data.
    4	DONE	The operation is complete.
  
    xhr.status:
    https://de.wikipedia.org/wiki/HTTP-Statuscode
  
    xhr.onreadystatechange = function(){}
    Eventhandler (DOM-Level 1) wird gebunden.
    Der Aufruf erfolgt im Browser immer wenn sich der Status ändert.
    Aufruf erfolgt intern im Browser vom user interface thread.
  
    xhr.responseText 
    Die Antwort vom Server: Inhalt der Datei als Zeichenkette.
    
    xhr.responseXML
    Die Antwort vom Server: Inhalt der Datei als XML-Document-Object, wenn eine XML-Datei angefordert wurde. 
  
    xhr.response
    Die Antwort vom Server: im korrekten Format wenn der requestType vorher gesetzt wurde. 
  
    xhr.timeout = 2000; //=> Zeit in Millisekunden
  
    xhr.ontimeout = funciton(){
      // Mache etwas wenn timeout überschritten und Anfrage beendet
    }
  
    Parameter senden
    Methode GET über query-string in URL
    xhr.open('POST', '../../lib/data/beispiel.txt?term=value&term2=value2', true);
  
  
    Methode POST:
    send('term=value&term2=value2')
    */
  ///////////////////////////////////////////////////////////////////////////////////////
  
  
  
  /* 
  1. Code wird Zeile für Zeile abgearbeitet
  2. Code wird wiederholt (Schleifen)
  3. Code in Funktionen (Definition und Aufruf von Funktionen)
    Der Code läuft zu einem späteren Zeitpunkt nach Aufruf auf
  4. Eventhandler/EventListener 
    Code wird ggf. gar nicht oder erst nach einem Ereignis ausgeführt
  5. Code wird asynchron ausgeführt. 
    Mehrere Prozesse werden parallel ausgeführt.
  
  
  
    var xhr = new XMLHttpRequest();
    xhr.open();
    xhr.onreadystatechange = function () {
  
    };
    xhr.send();
  
  
    var xhr = new XMLHttpRequest();
    xhr.open(method, file, async);
    xhr.onreadystatechange = function () {
  
    };
    xhr.send();
  */