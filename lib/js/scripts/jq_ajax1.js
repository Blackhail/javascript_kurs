;
(function (window, document, $) {
  'use strict';
  /*  Load data from the server and place the returned HTML into the matched elements. 
  Laden von Daten die direkt ohne weitere Verarbeitung im HTML ausgegeben werden können. 
  Ganze HTML-Fragmente (HTML Seite ohne doctype usw.), HTML-Seiten.
  
  
  Beispiel Siehe events.html im Ordner lib > data*/
  function handleClickList1( /* e */ ) {
    var $ausgabe = $('#ausgabe1');
    var filename = this.textContent.trim().split(': ')[1];
    var path = this.href;
    // var path = '../lib/data/' + filename;
    // console.log(path);
    switch (filename) {
      case 'pathfinder_fragment.html':
        $ausgabe.load(path);
        break;
      case 'pathfinder.html':
        /* Mit Leerzeichen vom Pfad getrennt kann hier jeder beliebige Selektor übergeben werden.*/
        $ausgabe.load(path + ' #main');
        break;
      case 'pathfinder.php':
        /* Diese PHP-Datei kann direkt angefordert werden, da innerhalb der PHP-Datei ermittelt wird ob ein XHR-Request vorliegt. Bei einem normalen Request ohne XHR wird der Doctype durch PHP erzeugt und bei XHR-Request wird nur der Inhalt erzeugt.
        
        Ohne die Abfrage innerhalb der PHP-Datei kann diese Datei wie eine HTML-Datei behandelt werden. Auch hier kann mit einem Selektor nache dem Pfad nur ein Teil der Datei ausgewählt werden.*/
        $ausgabe.load(path);
        break;
      case 'data.json':
        /* Der String aus responseText wird direkt in unser Dokument geschrieben ohne JSON.parse */
        $ausgabe.load(path);
        break;
    }
    console.log(path);



    return false;
  }

  function handleJSON(data) {
    var html = '';
    data.forEach(function (element) {
      html += '<li>' + element.headline.substring(0, 46) + '</li>';
    });
    return '<ul>' + html + '</ul>';
  }


  function handleClickList2( /* e */ ) {
    var $ausgabe = $('#ausgabe2');
    var filename = this.textContent.trim().split(': ')[1];
    // var path = this.href;
    var path = '../lib/data/' + filename;
    switch (filename) {
      case 'pathfinder.html':
        /* Hier wäre load mit Selektor deutlich einfacher */
        $.get(path, function (html) {
          /* Das komplette HTML-Dokument mit Doctype usw. würde ausgegeben */
          // $ausgabe.empty().append(html);
          /* Die Zeichenkette in html, der responseText der XHR-Anfrage, wird durch die Factory-FN in eine Sammlung DOM-Knoten umgewandelt. filter zieht den container #main aus dieser Sammlung und wir können Ihn in unser Dokument einfügen */
          var $element = $(html).filter('#main');
          $ausgabe.empty().append($element);
        }, 'html'); //=> Default: Intelligent Guess (xml, json, script, text, html)
        break;
      case 'data.json':
        var x; /* handleJSON könnte hier direkt als FN-Referenz an $.get als 2 Argument übergeben werden. In unserem Beispiel ist die anonyme FN/FN-Ausdruck nur zur Darstellung */
        $.get(path, function (data) {
          /* Inhalt der FN wird später ausgeführt als das LOG in Zeile 75 */
          $ausgabe.empty().append(handleJSON(data));
          console.log(data);
          x = data;
        }, 'json'); //=> Default: Intelligent Guess (xml, json, script, text, html)
        console.log(x); //=> undefined
        break;
      case 'kontakt.xml':
        $.get(path, function (xml) {
          /* xml ist ein natives Document-Objekt erzeugt aus der XML-Datei */
          console.log(xml, xml.getElementsByTagName('name')[0], xml.getElementsByTagName('name')[0].textContent);
          //////////////////////////////////////////////////////////////////////////////////////////////////////////
          /* Durch die Factory FN werden als DOM-Knoten der XML-Datei über jQuery verfügbar gemacht.
          AM jQuery-XML-Objekt können wir alle jQuery-Methoden, wie z.B find, verwenden. */
          var $xml = $(xml);
          console.log($xml, $xml.find('name').eq(0), $xml.find('name').eq(0).text());
          var html = '';
          html += '<ul>';
          var $children = $(xml).find('kontakt').children();
          var $child;
          for (var i = 0; i < $children.length; i++) {
            $child = $children.eq(i);
            html += '<li>' + ucfirst($child.prop('tagName')) + ': ' + $child.text()
            '</li>';
          }
          html += '</ul>';
          $ausgabe.empty().append(html);
        }, 'xml');
        break;
      case 'request.php':
        /* query als String ohne ? am Anfang! */
        // var query = 'term=value&schluessel=wert&foo=bar';
        /* query als Objekt ist deutlich übersichtlicher */
        var query = {
          term: 'value',
          schluessel: 'wert',
          foo: 'bar'
        }
        $.get(path, query, function (html) {
          $ausgabe.empty().append(html);
        }, 'html');
        break;
      case 'readjson.php':
        $.get(path, function (html) {
          $ausgabe.empty().append(html);
        }, 'html');
        break;
      case 'datenbankauslesen.php':
        break;
    }
    return false;
  }

  function handleClickList3( /* e */ ) {
    var $ausgabe = $('#ausgabe3');
    var filename = this.textContent.trim().split(': ')[1];
    var path = '../lib/data/' + filename;
    switch (filename) {
      case 'pathfinder.html':
        $.post(path, function (html) {
          var $element = $(html).filter('#main');
          $ausgabe.empty().append($element);
        }, 'html');
        break;
      case 'data.json':
        $.post(path, function (data) {
          $ausgabe.empty().append(handleJSON(data));

        }, 'json');
        break;
      case 'kontakt.xml':
        $.post(path, function (xml) {
          var html = '';
          html += '<ul>';
          var $children = $(xml).find('kontakt').children();
          var $child;
          for (var i = 0; i < $children.length; i++) {
            $child = $children.eq(i);
            html += '<li>' + ucfirst($child.prop('tagName')) + ': ' + $child.text()
            '</li>';
          }
          html += '</ul>';
          $ausgabe.empty().append(html);
        }, 'xml');
        break;
      case 'request.php':
        var query = {
          term: 'value',
          schluessel: 'wert',
          foo: 'bar'
        }
        $.post(path, query, function (html) {
          $ausgabe.empty().append(html);
        }, 'html');
        break;
      case ' readjson.php':
        break;
      case 'datenbankauslesen.php':
        break;
    }
    return false;
  }

  function handleClickListEvents( /* e */ ) {
    var file = this.href + ' #' + this.id.split('_')[1];
    $('#ausgabeEvents').load(file);
    return false;
  }

  function handleSubmit1( /* e */ ) {

  }

  $(function () {
    $('#list1').find('a').on('click', handleClickList1);
    $('#listEvents').find('a').on('click', handleClickListEvents);
    $('#list2').find('a').on('click', handleClickList2);
    $('#list3').find('a').on('click', handleClickList3);
    $('#form1').on('submit', handleSubmit1);
  });
})(window, document, jQuery);