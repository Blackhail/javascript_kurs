'use strict';
/* https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Strict_mode */

var firstname = 'Maximilian';
document.write(firstname);

/* Uncaught ReferenceError: assignment to undeclared variable lastname */
// lastname = 'Reindfeld';

document.write('<h2>Eine Überschrift aus der externen Datei</h2>');
document.write('<p>Das ist ein Fließtext</p>');

/* Deklartion benötigt Schlüsselwort var */
var output = '';
console.log(output);

output = output + 'Hallo Welt';
console.log(output);

output = output + '<br>';
console.log(output);

output = output + 'Weiterer Inhalt auf einer neuen Zeile';
console.log(output);

document.write(output);
/* Hier kein Schlüsselwort var nötig */
output = '';
console.log(output);
