'use strict';
function init() {
  var html = '<h2>Datum und Uhrzeit mit JavaScript (Client-Zeit)</h2>';
  /* Datumsobjekt mit der aktuellen Clientzeit */
  var d = new Date();
  console.log(d);

  html += '<p>';
  /* Datumsobjekt in String verkettet wird automatisch in eine Zeichenkette umgewandelt. Format nicht optimal für Ausgabe außerhalb der englischen Sprachräume */
  html += 'var d = new Date(); //=> ' + d + '<br>';
  /* toLocaleString erzeugt ein in unserem Sprachraum lesbares Format */
  html += 'd.toLocaleString() //=> ' + d.toLocaleString() + '<br>';
  /* Millisekunden vergangen seit dem 01.01.1970 um 00:00:00.000 UTC.
Unixzeit: https://de.wikipedia.org/wiki/Unixzeit */
  html += 'Date.now() ' + Date.now() + '<br>';

  //Tag der Woche, beginnt mit 0 am Sonntag (0-6)
  html += 'd.getDay(); //=> <b>' + d.getDay() + '</b><br>';
  //Tag des Monats (1-31)
  html += 'd.getDate(); //=> <b>' + d.getDate() + '</b><br>';
  //Monat beginnt mit 0 Januar bis 11 Dezember(0-11)
  html += 'd.getMonth(); //=> <b>' + d.getMonth() + '</b><br>';
  html += 'd.getMonth()+1; //=> <b>' + (d.getMonth() + 1) + '</b><br>';
  //Aktuelles Jahr
  html += 'd.getFullYear(); //=> <b>' + d.getFullYear() + '</b><br>';
  //Jahre seit dem Jahr 1900
  html += 'd.getYear(); //=> <b>' + d.getYear() + '</b><br>';
  //Stunden
  html += 'd.getHours(); //=> <b>' + d.getHours() + '</b><br>';
  //Minutes
  html += 'd.getMinutes(); //=> <b>' + d.getMinutes() + '</b><br>';
  //Sekunden
  html += 'd.getSeconds(); //=> <b>' + d.getSeconds() + '</b><br>';
  //Millisekunden
  html += 'd.getMilliseconds(); //=> <b>' + d.getMilliseconds() + '</b><br>';
  html += '</p>';

  html += '<h2>Datumsobjekt mit vordefiniertem Datum</h2>';
  /* Datumsobjekt ohne Übergabe von Argumenten, erzeugt die aktuelle Clientzeit für JavaScript als Objekt. */
  var date1 = new Date();
  /* Datumsobjekt mit 1 Argument (Timestamp) */
  var date2 = new Date(421538400000);
  /* Datumsobjekt mit 1 Argument (Formatstring) */
  var date3 = new Date('02,08,2015,09:01:11');

  /* Datumsobjekt Beliebige Anzahl an Argumenten */
  var date4 = new Date(
    1982, // Jahr
    5, // Monat (beginnt bei 0)
    14, // Tag
    17, // Stunden
    10, // Minuten
    0, // Sekunden
    0 // Millisekunden
  );

  html += '<p>';
  html += 'var date1 = new Date(); //=> ' + date1 + '<br>';
  html += 'var date2 = new Date(421538400000); //=> ' + date2 + '<br>';
  html += "var date3 = new Date('02,08,2015,09:01:11'); //=> " + date3 + '<br>';
  html += 'new Date(1982,5,14,17,10,0,0) <b> ' + date4 + '</b><br>';
  html += '<pre>';
  html +=
    'var date4 = new Date(\n\t1982, // Jahr\n\t5, // Monat (beginnt bei 0)\n\t14, // Tag\n\t17, // Stunden\n\t10, // Minuten\n\t0, // Sekunden\n\t0 // Millisekunden\n);';
  html += '</pre>';
  html += '</p>';

  html += '<h2>Mit Datumangaben rechnen</h2>';
  html += '<p>';
  // var a = new Date(1982,5,14);
  /* Datumsobjekt Geburtstag */
  var a = new Date(1982, 5, 14);
  /* Datumsobjekt Heute */
  var b = new Date();
  /* Von beiden Datumsobjekten das Jahr ermitteln. Vom heutigen Datum das Geburtsdatum abziehen.*/
  var dif = b.getFullYear() - a.getFullYear();
  console.log(dif);
  /* Hier wird ermittelt ob der Geburtstag in diesem Jahr bereits hinter dem aktuellen Datum (Monat/Tag) liegt oder noch erfolgen vor uns liegt. Hatten wir noch keinen Geburtstag dieses Jahr muss in der Variable dif, die das Alter enthält, 1 abgezogen werden.*/
  if (
    b.getMonth() < a.getMonth() ||
    (b.getMonth() === a.getMonth() && b.getDate() < a.getDate())
  ) {
    dif = dif - 1;
  }
  html += 'Berechnunge Siehe Quellcode Zeile 38-44. Alter: ' + dif + '<br>';
  html += '</p>';
  console.log('JavaScript Date to timestamp');
  console.log(new Date('1982,06,14').getTime() / 1000);
  console.log(new Date().getTime() / 1000);
  /////////////////////////////////////////////////////////////
  html += '</p>';

  // Globale alle Moment Datumsobjekt auf Deutsch setzen
  moment.locale('de');

  var mom = moment();
  console.log(mom);
  console.log(mom._d);
  /* Dieses Moment-Datums-Objekt auf Deutsch einstellen. */
  mom.locale('de');
  html += '<h2>MOMENT.JS</h2>';
  html += '<p>';
  html += '<a href="https://momentjs.com/docs/">Dokumentation (ENG)</a><br>';
  html +=
    '<a href="https://momentjs.com/docs/#/displaying/">Ausgabe formatieren</a><br>';
  html += 'var mom = moment(); //=> ' + mom + '<br>';
  html += "mom.format('LLLL') //=> " + mom.format('LLLL') + '<br>';
  html += "mom.format('L') //=> " + mom.format('L') + '<br>';
  html += "mom.format('LL') //=> " + mom.format('LL') + '<br>';
  html += "mom.format('LLL') //=> " + mom.format('LLL') + '<br>';
  html += "mom.format('LLLL') //=> " + mom.format('LLLL') + '<br>';
  html += "mom.format('HH:mm:ss') //=> " + mom.format('HH:mm:ss') + '<br>';
  html += "mom.format('M') //=> " + mom.format('M') + '<br>';
  html += "mom.format('MM') //=> " + mom.format('MM') + '<br>';
  html += "mom.format('MMM') //=> " + mom.format('MMM') + '<br>';
  html += "mom.format('MMMM') //=> " + mom.format('MMMM') + '<br>';

  html += '<h2>MOMENT.JS Alter Berechnen</h2>';
  html +=
    "mom.diff('1982-06-14', 'year') //=> " +
    mom.diff('1982-06-14', 'year') +
    '<br>';
  html +=
    "mom.diff('1982-06-14', 'hour') //=> " +
    mom.diff('1982-06-14', 'hour') +
    '<br>';
  html +=
    "mom.diff('1982-06-14', 'minutes') //=> " +
    mom.diff('1982-06-14', 'minutes') +
    '<br>';

  html += '</p>';

  /* set kann ein vordefiniertes Datum für unser Datumsobjekt setzen */
  var date = moment().set({
    year: 2029,
    month: 3,
    date: 30,
  });

  document.querySelector('#ausgabe').innerHTML = html;

  function gibtZeitAus() {
    document.querySelector('#uhrzeit').innerHTML =
      '<b>' + moment().locale('de').format('HH:mm:ss') + '</b><hr>';
  }
  gibtZeitAus();

  /*  Die FN gibtZeitAus wird hier als Referenz an setInterval übergeben. Das 2 Argument sagt rufe die Funktion alle 1000ms (jede Sekunde) auf. */
  window.setInterval(gibtZeitAus, 1000);

  /* setIntveral(arg1,arg2)
  arg1 -> eine Funktionsreferenz die in einem Interval aufgerufen wird
  arg2 -> Zeit für das Interval in Millisekunden
  setInterval gibt eine Zahl zurück die als Referenz auf den Timer dient. */

  /* Der Zähler soll nur den Ablauf darstellen. Hier könnte mit Klickevent z.B. ein Timer gestoppt werden. */
  var i = 1;
  function doStuff() {
    console.log(i);
    i++;
    if (i > 10) {
      window.clearInterval(intRef);
    }
  }
  var intRef = window.setInterval(doStuff, 1000);

  function doThings() {
    console.log('************************************');
    console.log('Es sind genau 5 Sekunden vergangen!');
    console.log('************************************');
  }

  window.setTimeout(doThings, 5000);
  console.log('************************************');
  console.log('Start!');
  console.log('************************************');
  //////////////////////////////////////////////////////////////
  /* progress
  Hier könnte ein Datei geladen werden deren Größe wir kennen.
  Wurde die Datei zu 100% geladen ist der Balken auf seiner vollen
  Größe, vorher stellen wir den Ladestatus mit dem Balken dar.
  Alterantiv könnte man hier z.B. auch SKILLS in einer Bewerbung darstellen die von bis im Bereich 0 - 100% über einen Timer aufgebaut werden.
  */

  function changeWidth() {
    width = width + 5;
    progress.style.width = width + 'px';
    if (width >= parentWidth) {
      window.clearInterval(progressIntRef);
      /* Endlos */
      // progress.style.width = '0px';
      // width = 0;
    }
  }

  var progress = document.querySelector('#progress');
  /* Beim laden wird das Element durch Breite 0 ausgeblendet. Dadurch haben wir ein style-Element und können den Wert direkt auslesen.  */
  progress.style.width = 0;
  var width = progress.offsetWidth; //=> 0
  var parentWidth = progress.parentElement.offsetWidth; //=> 734

  /* changeWidth wird referenziert und soll alle 50 Millisekunden aufgerufen werden */
  var progressIntRef = window.setInterval(changeWidth, 50);
}
window.onload = init;
