;(function(window, document, $){
    'use strict';

    function handleSuccess (data, status, xhr) {
        // bereits mit JSON.parse in JS-Objekt umgewandelt
        console.log(data); 
        console.log(status); // XHR status message
        console.log(xhr);  // XHR Objekt

        /// Beispiel:
        var html = '';
        for (var key in data) {
          html += '<b>Personalnummer: ' + key + '</b><br>';
        }
        $('.column').eq(0).append(html);
        /// Beispiel Ende
    }

    function handleError(xhr, status, error) {
        console.log(arguments); // Argumente anzeigen lassen
        console.log('error: ', error);
        console.log('status: ', status); //XHR-Status-Message 
        console.log('xhr: ', xhr); //XHR-Objekt
      }

      function handleComplete(xhr, status) {
        console.log(arguments); // Argumente anzeigen lassen
        console.log('xhr: ', xhr); // XHR-Objekt
        console.log('status: ', status); //XHR-Status-Message 
      }
    
    
    $(function() {

        $.ajax({     // request kann genauer verarbeitet werden
            url: '../lib/data/personen.json',
            typ: 'GET',
            cache: false, // funktioniert nicht in allen browsern
            contentType: 'application/x-form-urlencoded; charset=UTF-8',
            dataType: 'json', // wird automatisch ermittelt
            data: {
                term: Date.now() // verhindert browser caching

            },
            success: handleSuccess, // Übertragung nicht erfolgreich
            error: handleError, // Übertragung erfolgreich
            complete: handleComplete // Übertragung beendet, Status egal
           });
  






    });



  })(window, document, jQuery);