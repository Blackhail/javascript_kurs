'use strict';

var number = 12;
var a = 1,
  b = 2,
  c = 3;

var r, //=> undefined
  t, //=> undefined
  z, //=> undefined
  u = 0; //=> 0

/* Funktioniert nur ohne 'use strict' */
// var d = e = f = 0;

var d = 0,
  e = 0,
  f = 0;

var g,
  h,
  j = 10;

let x = 31;
if (true) {
  let x = 71; // andere variable
  console.log(x); // 71
}

// const MWST = 19;
const PI = 3.141592653589793;
var empty;
console.log(empty); //=> undefined

/* ######################### */
/* Typprüfung */
/* ######################### */
console.log(isNaN(12)); //=> false
console.log(isNaN('12')); //=> false
console.log(isNaN('abc')); //=> true

/* ######################### */
/* Typumwandlung */
/* ######################### */

var input = '12.29';
var n = Number(input); //=> 12.29
var n = parseFloat(input); //=> 12.29
var n = parseInt(input); //=> 12
var n = +input; //=> 12.29
var n = input * 1; //=> 12.29

input = 'abc';
var n = Number(input); //=> NaN
var n = parseFloat(input); //=> NaN
var n = parseInt(input); //=> NaN
var n = +input; //=> NaN

var input = 2.99;
var s = String(input); //=> '2.99'
var s = input.toString(); //=> '2.99'
var s = '' + input; //=> '2.99'

//1 Funktionen
function doStuff() {}

//2 Variablen
// var firstname, lastname, birthday, zipcode;

//3 Eventhandler

//Vorbereitung EV
var firstname,
  lastname,
  str = '';

// E
firstname = prompt('Bitte Vorname eingeben');

//V
str = 'Vorname: ' + firstname;

//A
console.log(str);

var html = '';
document.write(html);

var defaultValue = 'things';

const MWST = 19;
let someValue = 'hello world';

/* let bedeutet Blockscope, ist nur innerhalb der Runden/Geschweiften Klammern verfügbar in denen die Variable deklariert wurde. */
if (1) {
  let foo = 'bar';
  console.log(foo);
}
/* Uncaught ReferenceError: foo is not defined */
// console.log(foo);

let bar = 'foofoo';
if (1) {
  let bar = 'barbar';
  console.log(bar);
}

var foobar = 'foofoo';
if (1) {
  var foobar = 'barbar';
  console.log(foobar);
}

if (1) {
  let x = 1;
  console.log(x);
  if (x) {
    let x = 2;
    console.log(x); //=> 2
    if (x) {
      x = 3;
      console.log(x); //=> 3
    }
    console.log(x); //=> 3
  }
}
console.log(x); // Aus Zeile 24

for (var i = 1; i < 10; i++) {
  console.log(i);
}
console.log(i); //=> 10

for (let j = 1; j < 10; j++) {
  console.log(j);
}
// Kein j

var bububa = 'foobar';
function doThings() {
  // var bububa = ''; // LOKAL
  /* Ohne use strict */
  // bububa = ''; // GLOBALE VARIABLE wird überschrieben
  let bububa;
}
doThings();
const NAMES = ['Jon', 'Hans'];
console.log(NAMES);

function doThingsAndStuff(array) {
  console.log(array);
  array.push('BUUUHHH');
}
doThingsAndStuff(NAMES);
console.log(NAMES);

const TEST = 'ABC';

function doThingsAndStuff2(s) {
  s = '';
  console.log(s);
}
console.log(TEST);
doThingsAndStuff2(TEST);
console.log(TEST);
/* Elementare Datentypen: string, number, boolean  
callByValue: Kopie des Wertes landet im Argument 

Objektdatentypen: Object, Array, Function
callByReference: Eine Referenz, Pointer auf den Speicher übergeben
*/

const OBJ = {
  x1: 12,
  x2: 30,
};

console.log(OBJ);

// OBJ = 'lalalalalaala';
OBJ.x1 = 200;
console.log(OBJ);

const p = document.querySelectorAll('p');
console.log(p);

function xyz() {
  var myVarInFN = '123';
  console.log(myVarInFN);
}

xyz();
/* Hier gibt es keine Variablk */
// console.log(myVarInFN);

function kaesekuchen(x) {
  console.log(x);
}

// for (var y = 0; y < 4; y++) {
//   kaesekuchen(y);
// }

var a = document.querySelectorAll('#list a');
for (let y = 0; y < a.length; y++) {
  a[y].onclick = function (e) {
    e.preventDefault();
    kaesekuchen(y);
  };
}
