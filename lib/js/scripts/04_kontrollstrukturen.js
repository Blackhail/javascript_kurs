var a = 12;
// if-Anweisung:
if (a > 0) {
  document.write('größer 0<br>');
} else {
  document.write('nicht größer 0<br>');
}

if (a > 0) {
  document.write('größer 0 <br>');
} else if (a < 0) {
  document.write('kleiner 0<br>');
} else {
  document.write('0<br>');
}

var city = 'Algier';
// switch-Anweisung:
switch (city) {
  case 'Kairo':
    document.write('Ägypten 0 <br>');
    break;
  case 'Algier':
    document.write('Algerien 0 <br>');
    break;
  //...
  case 'Washington':
  case 'Washington, D.C.':
    document.write('Vereinigte Staaten 0 <br>');
    break;
  default:
    document.write('Kein Land gefunden 0 <br>');
    break;
}

var age = 26;
// Ternär-Operator
var canDrinkAlcohol = age >= 18 ? 'True, 18 oder älter' : 'False, unter 18';
console.log(canDrinkAlcohol); // 'True, 18 oder älter'

// Länger als if-Anweisung
var canDrinkAlcohol;
if (age >= 18) {
  canDrinkAlcohol = 'True, 18 oder älter';
} else {
  canDrinkAlcohol = 'False, unter 18';
}
