;
(function (window, document, $) {
  'use strict';

  $(function () {
    var $list = $('#list');
    console.log('$(\'#list\'): ', $list);
    /*https://api.jquery.com/attr/
     Get the value of an attribute for the first element in the set of matched elements or set one or more attributes for every matched element. */
    console.log('$list.attr(\'id\'): ', $list.attr('id'));
    /* https://api.jquery.com/prop/#prop-propertyName 
    Get the value of a property
    for the first element in the set of matched elements or set one or more properties for every matched element.*/
    console.log('$list.prop(\'id\'): ', $list.prop('id'));
    console.log('$list.attr(\'nodeName\'): ', $list.attr('nodeName'));
    console.log('$list.prop(\'nodeName\'): ', $list.prop('nodeName'));

    console.log('$list[0].getAttribute(\'id\'): ', $list[0].getAttribute('id'));
    console.log('$list[0].getAttribute(\'nodeName\'): ', $list[0].getAttribute('nodeName'));

    console.log('$list[0].id: ', $list[0].id);
    console.log('$list[0].nodeName: ', $list[0].nodeName);
    /**
     * attr für Attribute die am HTML-Element gesetzt wurden (id, title, alt ...)
     * prop für Eigenschaften des DOM-Knotens (tagName, nodeType ...)
     * https: //api.jquery.com/prop/#prop-propertyName Vergleich Attributes vs. Properties
     */

  });
})(window, document, jQuery);