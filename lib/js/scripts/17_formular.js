(function (window, document) {
    'use strict';
    ////////////////////////////////////////////////////////////////////////////////////////////
    /* Allgemeine FN:
    Kommen später in die helpers.js bzw. ist schon dort angelegt. Dient hier nur der besseren Übersicht */
    ////////////////////////////////////////////////////////////////////////////////////////////
    function isBlank(s) {
      return s.trim().length === 0 ? true : false;
    }
  
    function createEl(tag, txt) {
      tag = document.createElement(tag);
      if (txt) {
        txt = document.createTextNode(txt);
        tag.appendChild(txt);
      }
      return tag;
    }
  
    function insertAfter(newNode, refNode) {
      return refNode.parentNode.insertBefore(newNode, refNode.nextSibling);
    }
  
    function ucfirst(s) {
      return s.slice(0, 1).toUpperCase() + s.slice(1);
    }
  
    function isChecked(collection) {
      var len = collection.length;
      for (var i = 0; i < len; i++) {
        if (collection[i].checked) {
          return true;
        }
      }
      return false;
    }
  
  
    ////////////////////////////////////////////////////////////////////////////////////////////
    /* FN für die Formularvalidierung*/
    ////////////////////////////////////////////////////////////////////////////////////////////
    function setError(field) {
      var label = createEl('label', ucfirst(field.name) + ' darf nicht leer sein');
      label.classList.add('error_required'); // Klasse im CSS vordefiniert
      /* field.type === 'radio' || field.type === 'checkbox' */
      if (field.name === 'gender') {
        field.parentElement.parentElement.appendChild(label);
      } else {
        // insertAfter(label, field);
        field.parentElement.appendChild(label);
      }
    }
  
    function removeError() {
      var errorRequired = document.getElementsByClassName('error_required');
      while (errorRequired.length) {
        errorRequired[0].remove();
      }
    }
  
    function countErrors() {
      return document.getElementsByClassName('error_required').length;
    }
  
    /* Hier sind this und e.target document.forms[0], weil der EventListener daran gebunden wurde.  */
    function checkForm(e) {
      /* Form-Objekt. Alle Formularfelder liegen auf numerischen und assoziativen Indizes. */
      var form = e.target; // this
      console.log(form.elements[0], form.elements.vorname);
  
      var vorname = form.elements.vorname; //=> $_GET['vorname'] | $_REQUEST['vorname']
      var nachname = form.elements.nachname; //=> $_GET['nachname'] | $_REQUEST['nachname']
      var email = form.elements.email; //=> $_GET['email'] | $_REQUEST['email']
      var gender = form.elements.gender; //=> $_GET['gender'] | $_REQUEST['gender']
      var subject = form.elements.subject; //=> $_GET['subject'] | $_REQUEST['subject']
      var message = form.elements.message; //=> $_GET['message'] | $_REQUEST['message']
  
      /* Vor jeder neuen Prüfung alle Fehlermeldungen entfernen. */
      removeError();
  
      if (isBlank(vorname.value)) {
        setError(vorname);
      }
  
      if (isBlank(nachname.value)) {
        setError(nachname);
      }
  
      if (isBlank(email.value)) {
        setError(email);
      }
  
      if (!isChecked(gender)) {
        setError(gender[0]);
      }
  
      if (isBlank(subject.value)) {
        setError(subject);
      }
  
      if (isBlank(message.value)) {
        setError(message);
      }
  
      /* WENN: Felder nicht oder falsch ausgefüllt wurden... */
      if (countErrors() /* > 0 */ ) {
        /* ... submit verhindern. */
        e.preventDefault();
      }
    }
  
    function formValidate() {
      document.forms[0].addEventListener('submit', checkForm);
    }
  
    /**
     * FEATURE PRÜFUNG element.checkValidity :
     * Browser die dieses Feature anbieten, geben den Inhalt der Methode zurück.
     * Browser die das Feature nicht unterstützen geben undefined zurück.
     * Prüfen ob der Browser HTML - Validierung kann.
     * VORSICHT: Dieser Code funktioniert nur in alten Browsern die addEventlistener 
     * kennen. In alten IEs müssen die Handler mit attachEvent gebunden werden.
     * 
     * Wird an die FN checkHTML5Validation true übergeben, aktivieren wir den debug Modus
     * Im Debug Modus wird die HTML5 Validierung deaktivert und IMMER mit JS validiert.
     */
    function checkHTML5Validation(debug) {
      if (debug === true) {
        /* Zum Testen deaktivieren wir die HTML5 Validierung und rufen die formValidate hier direkt auf. Dieser Code wird später entfernt wenn die Seite Online geht. */
        document.forms[0].noValidate = true;
        formValidate();
      } else {
        /* Prüfen ob der Browser HTML5 Validierung kann.
        WENN der Browser keine HTML5 Validierung kennt...*/
        if (!document.getElementsByTagName('input')[0].checkValidity) {
          /* ...JS Validierung starten */
          formValidate();
        }
      }
    }
  
    window.addEventListener('load', function () {
      // Der Code der auf das DOM zugreift hier
      // checkHTML5Validation(); // Live
      checkHTML5Validation(true); // debug testens
  
  
    });
  })(window, document);
  
  
  /* 
  Normale Reihenfolge der Validierung:
  HTML5 danach PHP
  
  
  Ideale Reihenfolge der Validierung:
  HTML5 wenn möglich
  JavaScript wenn HTML5 nicht möglich 
  PHP wenn keine der vorherigen funktioniert.
  */