'use strict';
// = Zuweisungs-Operator
var firstname = 'Jon Snow';

// Operation und Zuweisungs
var sum = 0;
sum += 5; //=> 5;   // sum = sum + 5
sum *= 5; //=> 25;  // sum = sum + 5

// + als arithmetischer Operator
var a = 12 + 2;

// + als Stringverkettungsoperator
var str = 'Hello ' + 'World';

// + Typumwandlungsoperator
var number = +'12';

/* Der Operator + ist in JavaScript überladen.*/

// Bitwise NOT (~)
var x = 21;
console.log(~x); //=> -22
console.log(~x + 1); //=> -21

// && Logischer Operatorer UND
var bool1 = true;
var bool2 = false;
var bool3 = true;
console.log(bool1 && bool2); //=> false
console.log(bool1 && bool3); //=> true

var isLoggedIn = true;
var isAdmin = false;
var isLoggedInAdmin = isLoggedIn && isAdmin; //=> false

// || Logischer Operatorer ODER
var bool1 = true;
var bool2 = false;
var bool3 = false;
console.log(bool1 || bool2); //=> true
console.log(bool2 || bool3); //=> false

var isLoggedIn = true;
var isAdmin = false;
var isLoggedInAdmin = isLoggedIn || isAdmin; //=> true
var loginCheck = true;

// Bedingter (ternärer) Operator
var isLoggedIn = loginCheck ? true : false;

// Komma Operator
var a = 1,
  b = 2,
  c = 3;
console.log('hello', 'world');
var input = prompt('Bitte etwas eingeben', 'Platzhalter');

//Unäre Operatoren
var n = 12;
var s = 'foobar';
console.log(typeof n); //=> 'number';
console.log(typeof s); //=> 'string';

var x = 1;
x++; //=> 2
++x; //=> 3

var number = +'12'; //=> 12

// Vergleichsoperator (einfach)
var x = 100;
var y = 20;
var z = '200';

console.log(x == y); //=> false
console.log(x != y); //=> true
console.log(y == z); //=> true

// Vergleichsoperator (strikt)
console.log(x === y); //=> false
console.log(x !== y); //=> true
console.log(y === z); //=> false

// Vergleichsoperator Größer/Kleiner
console.log(x > y); //=> true
console.log(x < y); //=> false
/* 100 <= '200' */
console.log(x <= z); //=> true
