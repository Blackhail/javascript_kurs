;
(function (window, document, $) {
  'use strict';

  function handleSuccess(data, status, xhr) {
    console.groupCollapsed('__________ handleSuccess __________');
    /* Der bereits mit JSON.parse in ein JS-Objekt umgewandelte responseText */
    console.log('data: ', data);
    console.log('status: ', status); //XHR-Status-Message 
    console.log('xhr: ', xhr); //XHR-Objekt

    var html = '';
    for (var key in data) {
      html += '<b>Personalnummer: ' + key + '</b><br>';
    }
    $('.column').eq(1).append(html);
    console.groupEnd('_____ handleSuccess _____');
  }

  function handleError(xhr, status, error) {
    console.groupCollapsed('__________ handleError __________');
    console.log(arguments);
    console.log('xhr: ', xhr); //XHR-Objekt
    console.log('error: ', error);
    console.log('status: ', status); //XHR-Status-Message 
    console.groupEnd('_____ handleError _____');
  }

  function handleComplete(xhr, status) {
    console.groupCollapsed('__________ handleComplete __________');
    console.log('xhr: ', xhr); //XHR-Objekt
    console.log('status: ', status); //XHR-Status-Message 
    console.groupEnd('_____ handleComplete _____');
  }

  /* Hier drin kann der xhr-Request auch manipuliert werden. Ist die Anfrage von einer vorherigen Anfrage oder Bedingung abhängig kann hier entschieden werden ob die neue Anfrage überhaupt gestartet werden soll. return false in dieser FN verhindert die ajax-Anfrage zu der beforeSend gehört.*/
  function handleBeforeSend(xhr, settings) {
    console.groupCollapsed('__________ handleBeforeSend __________');
    console.log('xhr: ', xhr); //XHR-Objekt
    console.log('settings: ', settings); //XHR-Status-Message 
    console.groupEnd('_____ handleBeforeSend _____');
  }

  /* https://de.wikipedia.org/wiki/HTTP-Statuscode
  Für jeden der Codes kann eine eigene FN geschrieben werden. */
  var statusCodes = {
    200: function (data, status, xhr) {

    },
    404: function (xhr, status, error) {

    }
  }

  /* https://api.jquery.com/jquery.ajax/ */
  $(function () {
    $.ajax({
      url: '../lib/data/personen.json',
      type: 'GET',
      /* Alternativ zu cache wird hier in data gezeigt */
      cache: false, // Funktioniert nur bei GET und HEAD - Requests
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      dataType: 'json', //Intelligent Guess
      /* Query der an Server gesendet wird. */
      data: {
        term: Date.now() // Verhindert Caching!
      },
      beforeSend: handleBeforeSend, // Aktion bevor der Request gestartet wird
      success: handleSuccess, // Übertragung war erfolgreich
      error: handleError, // Übertragung war nicht erfolgreich 
      complete: handleComplete, // Übertragung beendet. Status egal
      statusCode: statusCodes
    });



    /* Synchrone XMLHttpRequests am Haupt-Thread sollte nicht mehr verwendet werden, weil es nachteilige Effekte für das Erlebnis der Endbenutzer hat. Für weitere Hilfe siehe http://xhr.spec.whatwg.org/ */
    var response;
    $.ajax({
      url: '../lib/data/personen.json',
      async: false,
      success: function (data) {
        response = data;
      }
    });

    console.log(response);
    //////////////////////////////////////////////////////////////////////////////////
    /* Asychnroner Request und Daten speichern. Macht nur Sinn wenn die Daten sich nicht permanent ändern. 
    Im Idealfall kombiniert man das mit einem Cookie oder Timer. */
    var responseData = null;

    function handleData(res) {
      console.log(res);
    }

    function handleClick( /* e */ ) {
      if (responseData === null) {
        $.ajax({
          url: '../lib/data/personen.json',
          success: function (data) {
            responseData = data;
            handleData(data);
          }
        });
      } else {
        handleData(responseData);
      }
      return false;
    }

    $('a:contains("Click me")').on('click', handleClick);

  });
})(window, document, jQuery);

/* 
data: {
  term: Date.now() // Verhindert Caching!
}

// Caching
'../lib/data/personen.json'
'../lib/data/personen.json'
'../lib/data/personen.json'

//Kein Caching
'../lib/data/personen.jsonterm=8484646'
'../lib/data/personen.jsonterm=8484651'
'../lib/data/personen.jsonterm=8484741' 
*/