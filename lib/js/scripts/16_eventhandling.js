(function (window, document) {
  'use strict';
  /* *1* Für das Eventobjekt das automatisch übergeben wird benötigen wir nur einen Parameter. e | ev | event sind typische Bezeichner. Hier sind this und e.target identisch, beide Pointer zeigen auf das selbe Element.  */
  function doStuff(e){
    e.preventDefault(); //=> Verhindert das Standardverhalten an dieser Position sofort!
    console.log('Event-Objekt //=> ', e);
    console.log('this-Objekt //=> ', this);
    console.log('Event-Objekt Eigenschaft target e.target //=> ', e.target);
    console.log('this === e.target ', this === e.target);
    console.log('____________________________________________________');
  }
  /* this ist das Element an dem der EventListener/Eventhandler gebunden wurde, e.target das Element an dem das Ereignis/Event ausgelöst wurde. */
  function delegiertesEvent(e){
    e.preventDefault(); //=> Verhindert das Standardverhalten an dieser Position sofort!
    console.log('Event-Objekt //=> ', e);
    console.log('this-Objekt //=> ', this);
    console.log('Event-Objekt Eigenschaft target e.target //=> ', e.target);
    console.log('this === e.target ', this === e.target);
    console.log('____________________________________________________');
    //////////////////////////////////////////////////////////////////////////
    if(e.target.nodeName === 'A') {
      /* Hier kommt der CODE hin der ausgeführt werden soll! */
      console.log('AKTION wenn A angeklickt wird ausführen.');
    }
    //////////////////////////////////////////////////////////////////////////
  }


  function doOtherStuff(){

  }

  window.addEventListener('load', function () {
    // Der Code der auf das DOM zugreift hier
    var list1A = document.getElementById('list1').getElementsByTagName('a');
    console.log(list1A);
    for(var i = 0; i < list1A.length; i++) {
      /* Sobald der Browser den click registriert, wird in der FN referenzt das angeklickte Element in this gespeichert und die FN wird mit dem Eventobjekt als Argument aufgerufen. *1*
      Hier wird der EventListener direkt am Element das angeklickten werden kann gebunden. Im Inspektor ist (event) hinter jedem der Elemente zu sehen. */
      list1A[i].addEventListener('click', doStuff);
    }

    var list2A = document.getElementById('list2');
    list2A.addEventListener('click', delegiertesEvent);
    
    /* Arrayartiges Objekt, mit Inidzes und length-Eigenschaft ABER kein echtes Array! */
    var htmlcol = document.getElementsByClassName('box');
    /* Moderne Technik Array.from und each */
    Array
    .from(htmlcol)
    .forEach(function (el) {
      el.addEventListener('click', doOtherStuff);
    });


    /* HTMLCollection und Array haben als Inhalt die selben Pointer. Nur das umgebende Konstrukt ist anders. An der HTMLCollection kann man keine Arraymethoden, wie z.B. forEach, aufrufen.   */
    var pointers1 = document.getElementsByClassName('box');
    var pointers2 = Array.from(htmlcol);
    console.log(pointers1);
    console.log(pointers2);
    for(var i = 0; i < pointers1.length; i++) {
      if(pointers1[i] === pointers2[i]) {
        console.log(pointers1[i] , pointers2[i]);
      }
    }
    
  });
})(window, document);



