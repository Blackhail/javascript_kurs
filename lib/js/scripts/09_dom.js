'use strict';
/* 
https://www.mediaevent.de/javascript/DOM.html
https://www.mediaevent.de/javascript/queryselector.html
https://www.mediaevent.de/javascript/queryselectorall.html
https://www.mediaevent.de/javascript/innerhtml.html
https://www.mediaevent.de/javascript/style.html
https://www.mediaevent.de/javascript/event-handler.html
*/

document.querySelector('#bild').style =
  'width:20%; height: auto; border: 5px solid pink;';

console.log('Hello World oder Debugging');
document.write('<p id="test">document.write schreibt beim laden der Seite</p>');

document.querySelector('#test').innerHTML +=
  ' <br><b>innerHTML kann den Text in der bereits vollständig geladenen Seite verändern</b><br>';

console.log(document);
console.log(document.URL);
console.log(document.title);
console.log(document.body);

/* querySelector findet das erste Elemente passend zum Selektor  */
console.log(document.querySelector('p'));
/* querySelector findet alle Elemente passend zum Selektor.  */
console.log(document.querySelectorAll('p'));
console.log(document.querySelectorAll('p')[0]);
console.log(document.querySelectorAll('p')[1]);

console.log(document.querySelectorAll('body')); //=> [ body ]
console.log(document.querySelectorAll('body')[0]); //=> body

console.log(document.querySelectorAll('*'));
console.log(document.querySelectorAll('.postit'));

//////////////////////////////////////////////////////////////////////////////////
function ziehungSimulieren() {
  var ziehung = [];
  var random;
  while (ziehung.length < 6) {
    random = Math.floor(Math.random() * 49) + 1;
    if (!ziehung.includes(random)) {
      ziehung.push(random);
    }
  }
  document.querySelector('#ausgabe').innerHTML = ziehung.join(', ');
}

// document['querySelector']('input[value="Lottozahlen Ziehung starten"]')[
//   'onclick'
// ] = ziehungSimulieren;

document.querySelector(
  'input[value="Lottozahlen Ziehung starten"]'
).onclick = ziehungSimulieren;
/* ziehungSimulieren ist eine Referenz. Der Aufruf erfolgt nicht hier in Zeile 43-45 sondern später durch den Browser wenn er den Klick auf das mit dem querySelector angewählte Element registriert.  */

/* 
. in PHP Stringverkettung in JS +
. in JS Objekt-Zugriff auf Eigenschaft/Methode in PHP Stringverkettung
-> in PHP Objekt-Zugriff auf Eigenschaft/Methode in JS FEHLERMELDUNG
=> in PHP Zuweisung  auf Assioativen Index in JS Teil der Arrow-Funktion
*/

var element = document.querySelector('#bild');
console.log(element);
console.log(element.src);
console.log(element.id);
element.title = 'Das ist der Title';

console.log(document.querySelector('div').className);
console.log(document.querySelector('.postit'));

console.log(document.querySelector('ul > li a:first-child'));
console.log(document.querySelectorAll('div:not(.postit)'));

console.log(document.querySelector('div')); //=> Erste als JS-Objekt
console.log(document.querySelectorAll('div')); //=> Alle als Sammlung von JS-Objekten, in einem arrayartigen Objekt

console.log('*********************************');
/* nth-of-type beginnt bei 1 zu zählen */
console.log(document.querySelector('#list li:nth-of-type(3)'));
/* All #list li gibt eine Nodelist zurück, diese verhält sich wie ein Array.#list li:nth-of-type(3) liegt in der NodeList auf Position 2, also [2].
Die NodeList beginnt bei 0 zu zählen nicht wie nth-of-type bei 1. */
console.log(document.querySelectorAll('#list li')[2]);

console.log(document.querySelector('#list li:nth-of-type(1)'));
console.log(document.querySelectorAll('#list li')[0]);
