/**
 * Buch: Kapitel 4.5
 * https: //overapi.com/jquery (TRAVERSING)
 * https: //api.jquery.com/category/traversing/
 */

;
(function (window, document, $) {
  'use strict';

  function task1() {
    console.groupCollapsed('Native DOM-Knoten (NODES)');
    var list = document.getElementById('list');
    console.log('Element/Node: ', list, list.toString());
    console.log('Attribute/Eigenschaften "id"', list.id);
    console.log('Attribute/Eigenschaften "id"', list.getAttribute('id'));

    console.log('outerHTML: ', list.outerHTML);
    console.log('innerHTML: ', list.innerHTML);
    console.log('innerText: ', list.innerText);
    console.log('textContent: ', list.textContent);

    console.log('Element Kindknoten: ', list.children);
    console.log('Alle Kindknoten: ', list.childNodes);

    console.log('Erster Kindknoten: ', list.firstChild);
    console.log('Letzter Kindknoten: ', list.lastChild);
    console.log('Kindknoten aus Index 2 : ', list.childNodes[2]);
    console.log('Erster Kindknoten Typ Element: ', list.firstElementChild);
    console.log('Letzter Kindknoten Typ Element: ', list.lastElementChild);
    /* Geschwister */
    console.log(
      'Vorheriges Geschwisterelement vom lastChild aus: ',
      list.lastChild.previousSibling
    );
    console.log(
      'Nächstes Geschwisterelement vom firstChild aus: ',
      list.firstChild.nextSibling
    );
    console.log(
      'Vorheriges Geschwisterelement Typ Element vom lastChild aus: ',
      list.lastChild.previousElementSibling
    );
    console.log(
      'Nächstes Geschwisterelement Typ Element vom firstChild aus: ',
      list.firstChild.nextElementSibling
    );
    /* Das direkte Elternelement */
    console.log('Elternelement: ', list.parentElement);
    console.log('Elternelement: ', list.parentNode);
    console.log('Elternelement: ', list.lastChild.parentElement);
    console.groupEnd('Native DOM-Knoten (NODES)');
  }

  function task2() {
    ///////////////////////////////////////////////////////////////////
    /* DOM - Traversing (Bewegen durch den DOM-Baum - jQuery) */
    ///////////////////////////////////////////////////////////////////
    console.group('jQuery DOM-Knoten (Nodes)');
    var $list = $('#list');
    console.log('jQuery-Collection/Objekt: ', $list, $list.toString());
    console.log('Element/Node: ', $list[0], $list[0].toString());

    /* Attribute werden mit attr ausgelesen.
    prop liest DOM-Eigenschaften aus.  */
    console.log('Attribute/Eigenschaften', $list.attr('id'));
    console.log('Attribute/Eigenschaften', $list.prop('id'));
    console.log('Attribute/Eigenschaften', $list[0].id);
    console.log('outerHTML: ', 'in jQuery nicht vordefiniert');
    console.log('innerHTML / html(): ', $list.html());
    console.log('innerText / text(): ', $list.text());
    console.log('textContent: ', $list[0].textContent);

    console.log('Element Kindknoten: ', $list.children());
    console.log('Alle Kindknoten: ', $list.contents());
    console.log('Erster Kindknoten: ', $list.contents().first());
    console.log('Letzter Kindknoten: ', $list.contents().last());
    console.log('Erster Kindknoten Typ Element: ', $list.children().first());
    console.log('Letzter Kindknoten Typ Element: ', $list.children().last());
    console.groupEnd('jQuery DOM-Knoten (Nodes)');
  }

  function examples() {
    console.group('jQuery DOM-Knoten (examples)');
    var $box = $('.box');
    $box.text(function (i) {
      return i;
    });

    console.log('$(\'.box \'): ', $box);
    console.log('$(\'.box \').eq(3): ', $box.eq(3));
    console.log('$(\'.box \').first(): ', $box.first());
    console.log('$(\'.box \').last(): ', $box.last());

    console.log('$(\'.box \').eq(2).nextAll(): ', $box.eq(2).nextAll());
    console.log('$(\'.box \').eq(2).prevAll(): ', $box.eq(2).prevAll());

    console.log('$(\'.box \').eq(3).parent(): ', $box.eq(3).parent());
    console.log('$(\'.box \').eq(3).parentsUntil(\'html\'): ', $box.eq(3).parentsUntil('html'));

    console.groupEnd('jQuery DOM-Knoten (examples)');
  }

  $(function () {
    task1();
    task2();
    examples();
  });
})(window, document, jQuery);