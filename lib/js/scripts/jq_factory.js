/* JS Dateien werden oftmals, beim laden,  zusammengeführt oder hintereinander eingebunden.
Da JS kein Semikolon am einer Zeile benötigt, schließt man bei jedem neuen Dokument zur Sicherheit einen vorher geschrieben Befehl mit einem Semikolon ab. */
;
(function (window, document, $) {
  'use strict';
  /* Natives JS */
  window.onload = function () {};
  window.addEventListener('load', function () {});
  window.addEventListener('DOMContentLoaded', function () {});

  /* jQuery */
  $(document).ready(function () {});

  /*
   * jQuery Shorthand: 
   * Query( callback ) 
   * https: //api.jquery.com/jQuery/#jQuery-callback 
   */
  $(function () {
    ////////////////////////////////////////////////////////////////////////////
    /* 
     * Factory FN und Ihre Argumente 
     * API Documentation: Category: Core
     * https: //api.jquery.com/jQuery/    
     */
    ////////////////////////////////////////////////////////////////////////////
    console.log('____ jQuery( selection ) ____');
    /*jQuery( selection ) */
    console.log($('#list a'));
    console.log($('.boxes div'));

    console.log('____ jQuery( selector [, context ] ) ____');
    /* jQuery( selector [, context ] ) */
    console.log($('a', '#list'));
    console.log($('div', '.boxes'));

    console.log('____ jQuery( element ) ____');
    /* jQuery( element ) */
    var list = document.getElementById('list');
    console.log($(list));
    /* Hier macht es keinen Sinn, weil folgende Selektion einfacher ist: */
    console.log($('#list a'));

    /* Praktisches Anwendungsbeispiel:
     Übergabe eines JS-Objekts Typ-Element (DOM-Element-Knoten) */
    console.log($(document));
    console.log($(document.body));
    console.log($(document.forms[0]));

    console.log('____ jQuery( elementArray ) ____');
    /* jQuery( elementArray ) 
    elementArray ist Array oder HTMLCollection/Nodelist mit JavaScript Objekten vom Typ Element. */
    console.log($(document.forms));
    console.log($(document.images));
    console.log($(document.links));
    console.log($(document.anchors));
    /* Schneller als die folgende Zeile */
    console.log($(document.getElementsByTagName('p')));
    /* Einfacher als die vorherige Zeile */
    console.log($('p'));

    console.log('____ jQuery( html ) ____');
    /* jQuery( html ) */
    console.log($('<p>Hello <b>World</b></p>'));

    console.log('____ jQuery( html, attributes ) ____');
    /* jQuery( html, attributes ) */
    console.log($('<p></p>', {
      id: 'a1',
      text: 'Hello World',
      class: 'blue',
      title: 'Das ist der Titel',
      click: function () {}
    }));
  });
})(window, document, jQuery);