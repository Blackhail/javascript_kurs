(function (window, document) {
    'use strict';
    
    var submitted = false; // Hilfsvariable, wird bei Formulareingabe auf true gesetzt

    ////////////////////////////////////////////////////////////////////////////////////////////
    /* Allgemeine FN:
    Kommen später in die helpers.js bzw. ist schon dort angelegt. Dient hier nur der besseren Übersicht */
    ////////////////////////////////////////////////////////////////////////////////////////////
    function isBlank(s) {
      return s.trim().length === 0 ? true : false;
    }
  
    function createEl(tag, txt) {
      tag = document.createElement(tag);
      if (txt) {
        txt = document.createTextNode(txt);
        tag.appendChild(txt);
      }
      return tag;
    }

    function insertAfter(newNode, refNode) {
      return refNode.parentNode.insertBefore(newNode, refNode.nextSibling);
    }
  
    function ucfirst(s) {
      return s.slice(0, 1).toUpperCase() + s.slice(1);
    }
  
    function isChecked(collection) {
      var len = collection.length;
      for (var i = 0; i < len; i++) {
        if (collection[i].checked) {
          return true;
        }
      }
      return false;
    }
  
  
    ////////////////////////////////////////////////////////////////////////////////////////////
    /* FN für die Formularvalidierung*/
    ////////////////////////////////////////////////////////////////////////////////////////////
    function setError(field) {
      var label = createEl('label', ucfirst(field.name) + ' darf nicht leer sein'); // createEL erwartet zwei Parameter
      label.classList.add('error_required'); // fügt class hinzu (in css definiert)
      if (field.name === 'gender') {
        field.parentElement.parentElement.appendChild(label); // ans Ende der radiobutton-Liste einfügen 
      } else {
        field.parentElement.appendChild(label); // hinter label einfügen
      }
    }
  
    function removeError() {
      var errorRequired = document.getElementsByClassName('error_required'); // leert die var errorRequired (Collection)
      while (errorRequired.length) { 
        errorRequired[0].remove(); // remove, solange es eine Länge hat (bis nichts mehr drin steht)
      }
    }
  
    function countErrors() { // wird später benötigt, um Absenden (Standardverhalten) zu unterdrücken, wenn Fehler vorhanden sind
      return document.getElementsByClassName('error_required').length; // gibt Zahl aus (Anzahl Indizes)
    }
     
    function checkForm(e) { // Errorhandling für Formulareingaben
      submitted = true;
      var form = e.target; // this
      console.log(form.elements[0], form.elements.vorname);
  
      var vorname = form.elements.vorname; // holt sich Inhalt aus ID-Attribut oder name-Attribut im html-tag
      var nachname = form.elements.nachname;
      var email = form.elements.email;
      var gender = form.elements.gender;
      var subject = form.elements.subject;
      var message = form.elements.message;

      removeError(); // leert vorher die var errorRequired (Collection)
  
      if (isBlank(vorname.value)) { // Inhalt von input-Feld mit id/name="vorname" leer?
        setError(vorname);
      }
  
      if (isBlank(nachname.value)) {
        setError(nachname);
      }
  
      if (isBlank(email.value)) {
        setError(email);
      }
  
      if (!isChecked(gender)) { // ist einer der Radio-Buttons mit name="gender" NICHT ausgewählt?
        setError(gender[0]);
      }
  
      if (isBlank(subject.value)) {
        setError(subject);
      }
  
      if (isBlank(message.value)) {
        setError(message);
      }
  
      if (countErrors() /* > 0 */ ) { // wenn Fehler vorhanden, Abschicken (Standardverhalten) verhindern!
        e.preventDefault();
      }
    }
  
    function formValidate() {
      document.forms[0].addEventListener('submit', checkForm); // Funktion 'checkForm' (darin ist Fehlerhandling) wird bei clicken von submit aktiviert
    }
  
    function checkHTML5Validation(debug) {
      if (debug === true) {
        document.forms[0].noValidate = true;
        formValidate();
      } else {
        if (!document.getElementsByTagName('input')[0].checkValidity) {
          /* ...JS Validierung starten */
          formValidate();
        }
      }
    }
  
    function checkInput(e){
      if(submitted) {
        var field = e.target; //this
        var label = field             
                    .parentElement
                    .getElementsByClassName('error_required')[0];
        console.log(field, label);
        if (isBlank(field.value)) {
          if (!label) {
            setError(field);
          }
        } else {
          if (label) {
            label.remove();
          }
        }
      }
    }
  
    function checkRadio(e) {
      if (submitted) {
        var field = e.target; //this
        var name = field.name;
        var radios = document.getElementsByName(name);
        
        var label = field
          .parentElement
          .parentElement
          .getElementsByClassName('error_required')[0];

          if (label) {
            label.remove();
          }
  
          /* Bei checkboxen */
          // if(!isChecked(radios)){
          //   if (!label) {
          //     setError(radios[0]);
          //   }
          // } else {
          //   if (label) {
          //     label.remove();
          //   }
          // }
      }
    }
  
    function bindHandler(f) {
      for(var i = 0; i < f.elements.length; i++){
        var field = f.elements[i];
        if (field.type === 'text' || field.type === 'textarea') {
          field.addEventListener('keyup', checkInput);
          field.addEventListener('focus', checkInput);
          field.addEventListener('blur', checkInput);
        } else if (field.type === 'select-one') {
          field.addEventListener('change', checkInput);
          field.addEventListener('focus', checkInput);
          field.addEventListener('blur', checkInput);
        } else if (field.type === 'radio') {
          field.addEventListener('click', checkRadio);
          field.addEventListener('change', checkRadio);
          field.addEventListener('blur', checkRadio);
        }
      }
    }
  
    window.addEventListener('load', function () {
      checkHTML5Validation(true); // debug testens
      bindHandler(document.forms[0]);
  
    });
  })(window, document);
  
  
////// Normale Reihenfolge der Validierung: //////////////////////////////////////////////////////////////////////////////
  
//   HTML5 danach PHP

//   Ideale Reihenfolge der Validierung:
//   HTML5 wenn möglich
//   JavaScript wenn HTML5 nicht möglich 
//   PHP wenn keine der vorherigen funktioniert.
