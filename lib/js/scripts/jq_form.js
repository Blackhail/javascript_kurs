;
(function (window, document, $) {
  'use strict';
  
  $.validator.addMethod("letterswithbasicpunc", function (value, element) {
        return this.optional(element) || /^[a-zäöüß\-.,()'"\s]+$/i.test(value);
  }, 'Bitte nur Buchstaben und Interpunktion');


  var settings = {
   
    errorElement: 'label',
    errorClass: 'error_required',
    validClass: 'valid',
 
    normalizer: function (value) {
        return $.trim(value);
    },
    submitHandler: function (form) {
     
        var query = $(form).serialize();
        console.log(query);
        $.get('../lib/data/request.php', query, function (html) {
            $('#ausgabe').empty().append(html);
        });
    },
    errorPlacement: function ($errorElement, $element) {

      console.log($element, $errorElement);
        if ($element.prop('type') === 'radio') {
            $element.parent().parent().append($errorElement);
        } 
        else if ($element.prop('type') === 'checkbox') {
            $element.parent().append($errorElement);
        } 
        else {
            $element.after($errorElement); 
        }
        },
   
    highlight: function (element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass); 
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).addClass(validClass).removeClass(errorClass);
    },

    rules: {
        vorname: {       
            pattern: /^[a-zäöüß\-.,()'"\s]+$/i,
            minlength: 2,
            maxlength: 5
        },
        nachname: {
            letterswithbasicpunc: true
        }
    },

    messages: {
        nachname: {
            letterswithbasicpunc: 'Bitte nur Buchstaben und Interpunktion AUS messages'
        }
    }
  };

  $(function () {
        $('form').eq(0).validate(settings);
  });
  
})(window, document, jQuery);