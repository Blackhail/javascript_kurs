(function (window, document) {
    'use strict';
  
    function changeImage(e) {
      var img = document
        .getElementById('mainimage')
        .getElementsByTagName('img')[0];
      var imgUnderMouse = this;
      img.src = imgUnderMouse.src;
    }
  
  
    function changeContent(e) {
      var clickedButton = this;
      if (!clickedButton.classList.contains('active')) {
        var active = document.getElementsByClassName('active')[0];
        if (active) {
          active.classList.remove('active');
        }
        clickedButton.classList.add('active');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '../lib/data/data.json', true);
        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            var data = JSON.parse(xhr.responseText);
            var buttons = document
              .getElementById('switcher')
              .getElementsByTagName('button');
            var index = Array.from(buttons).indexOf(clickedButton);
            var ssd = data[index];
            var info = document.getElementById('info');
            while (info.lastChild) {
              info.lastChild.remove();
            }
  
            var h2 = document.createElement('h2');
            var txt = document.createTextNode(ssd.headline);
            h2.appendChild(txt);
  
            info.appendChild(h2);
  
            var ul = document.createElement('ul');
            var li;
            var value;
            for (var key in ssd) {
              value = ssd[key];
              if (key !== 'headline') {
                li = document.createElement('li');
                txt = document.createTextNode(key + ': ' + value);
                li.appendChild(txt);
                ul.appendChild(li);
              }
            }
  
            info.appendChild(ul);
            var array = Object.entries(ssd);
            var key, value, len = array.length;
            for (var i = 1; i < len; i++) {
              key = array[i][0];
              value = array[i][1];
            }
          }
        };
        xhr.send();
  
      }
    }
  
  
    window.addEventListener('load', function () {
      var images = document
        .getElementById('vorschau')
        .getElementsByTagName('img');
      var img, len = images.length;
      for (var i = 0; i < len; i++) {
        img = images[i];
        img.addEventListener('mouseover', changeImage);
      }
      var buttons = document
        .getElementById('switcher')
        .getElementsByTagName('button');
      var button, len = buttons.length;
      for (var i = 0; i < len; i++) {
        button = buttons[i];
        button.addEventListener('click', changeContent);
      }
    });
  })(window, document);