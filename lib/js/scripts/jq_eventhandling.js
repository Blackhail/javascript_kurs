;
(function (window, document, $) {
  'use strict';

  function handleClick(e) {
    e.preventDefault();
    console.log(e.target, this);
  }

  function handleSubmit(e) {
    e.preventDefault();
    console.log(e.target, this);

    var value = e.target.input1.value;
    if (value) {
      $('#list, #list2')
        .append('<li><a href="#">' + value + '</a></li>');
      e.target.input1.value = '';
    }
  }

  $(function () {
    /* Eventhandler über find direkt an den A-Elementen binden */
    $('#list').find('a').on('click', handleClick);

    /* Eventhandler vom Elternelement #list2 an A-Elemente delegieren.
    Vorteil bei jQuery ist das this und e.target beide auf das Element in this zeigen. */
    $('#list2').on('click', 'a', handleClick);

    // $(document.forms[0])
    $('form')
      .eq(0)
      .on('submit', handleSubmit);

  });
})(window, document, jQuery);


// function handleClickBind(e) {
//   e.preventDefault();
//   console.log(e.target, this);
// }


// function handleClick(e) {
//   e.preventDefault();
//   var x = handleClickBind.bind(e.target);
//   x(e);
// }

// document.getElementById('list2').addEventListener('click', handleClick);