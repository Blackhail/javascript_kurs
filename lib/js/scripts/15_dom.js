(function (window, document) {
  'use strict';
  // Funktionen und Variablen deklarieren
  function markH1(e) {
    // if (this.className === 'marker') {
    //   this.className = '';
    // } else {
    //   this.className = 'marker';
    // }

    this.classList.toggle('marker');
    // e.target.classList.toggle('marker');
  }

  window.addEventListener('load', function () {
    // Der Code der auf das DOM zugreift hier
    document //=> Document Objekt
      .querySelector('h1') //=> Erstes h1 Element
      .addEventListener(
        //=> Eventlistener binden
        'click', //=> Event ohne on als Zeichenkettte
        markH1 //=> FN-Referenz
      );

    // var button = document.getElementsByClassName('button');
    // console.log(button);
    // /* [1] ist absolut ausreichend wenn wir Wissen das hier kein weiterer input.button hinzugefügt wird. */
    // console.log(button[1]);
    // /* Alterantiv kann man hier auch nach dem value suchen.  */
    // var target = null;
    // for (var i = 0; i < button.length; i++) {
    //   if (button[i].value === 'Entferne Klasse box') {
    //     target = button[i];
    //   }
    // }
    // console.log(target);

    /* Hier kann man den querySelector verwenden oder wenn man das HTML manipulieren kann direkt eine ID vergeben. */
    var button = document.querySelector("input[value='Entferne Klasse box']");
    /* Wir wissen das kein weiters input.button hinzugefügt wird */
    var button = document.getElementsByClassName('button')[1];
    ///////////////////////////////////////////////////////////
    /**
     * getElementById //=> JS-Objekt-Typ Element
       Einzige Methode die nur ein Element einliest

     * getElementsByTagName  //=> HTMLCollection
        Alle Element passend zum übergebenen Tagnamen
        Ein Tagname wird ohne <> geschrieben
      
    * getElementsByName       //=> HTMLCollection
        Alle Element passend zum übergebenen Nameattributes

    * getElementsByClassName  //=> HTMLCollection
        Alle Element passend zum übergebenen Klassennamen

      HTMLCollections sind Sammlungen von JS-Objekten-Typ Element. Sie verhalten sich wie Nodelists, unterscheiden sich von diesen aber durch ein LIVE-UPDATE. 

      NODELIST und HTMLCollection sind beides arrayartige Objekte. Sie haben numerische Indizes und eine Eigenschaft length wie Array, ABER man kann an Ihnen keine Arraymethoden nutzen. 

      Beispiel:
      var elements = document.getElementsByClassName('box');
      //=> HTMLCollection { 0: div.box, 1: div.box, 2: div.box, 3: div.box, length: 4 }

      elements[0].classList.remove('box')
      HTMLCollection { 0: div.box, 1: div.box, 2: div.box, length: 3 }

      -----------------------------------------------------
      var elements = document.querySelectorAll('.box');
      NodeList(4) [ div.box, div.box, div.box, div.box ]

      elements[0].classList.remove('box')
      NodeList(4) [ div, div.box, div.box, div.box ]
     */
    ///////////////////////////////////////////////////////////
    var element = document.querySelector('#a1');
    console.log(element);
    /* Hier wird keine Selektor im String übergeben, wir suchen hier nach der ID a1 */
    element = document.getElementById('a1');
    console.log(element);

    var elements = document.querySelectorAll('#list a');
    console.log(elements);

    /* CSS: #list a */
    elements = document //=> HTML Dokument
      .getElementById('list') //=> #list in HTML Dokument
      .getElementsByTagName('a'); //=> a in #list
    console.log(elements);

    console.group('Native DOM-Knoten (NODES)');
    var list = document.getElementById('list');
    console.log('Element/Node: ', list, list.toString());
    console.log('Attribute/Eigenschaften "id"', list.id);
    console.log('Attribute/Eigenschaften "id"', list.getAttribute('id'));

    console.log('outerHTML: ', list.outerHTML);
    console.log('innerHTML: ', list.innerHTML);
    console.log('innerText: ', list.innerText);
    console.log('textContent: ', list.textContent);

    console.log('Element Kindknoten: ', list.children);
    console.log('Alle Kindknoten: ', list.childNodes);
    console.log('Erster Kindknoten: ', list.firstChild);
    console.log('Letzter Kindknoten: ', list.lastChild);
    console.log('Kindknoten aus Index 2 : ', list.childNodes[2]);
    console.log('Erster Kindknoten Typ Element: ', list.firstElementChild);
    console.log('Letzter Kindknoten Typ Element: ', list.lastElementChild);
    /* Geschwister */
    console.log(
      'Vorheriges Geschwisterelement vom lastChild aus: ',
      list.lastChild.previousSibling
    );
    console.log(
      'Nächstes Geschwisterelement vom firstChild aus: ',
      list.firstChild.nextSibling
    );
    console.log(
      'Vorheriges Geschwisterelement Typ Element vom lastChild aus: ',
      list.lastChild.previousElementSibling
    );
    console.log(
      'Nächstes Geschwisterelement Typ Element vom firstChild aus: ',
      list.firstChild.nextElementSibling
    );
    /* Das direkte Elternelement */
    console.log('Elternelement: ', list.parentElement);
    console.log('Elternelement: ', list.parentNode);
    console.log('Elternelement: ', list.lastChild.parentElement);

    ///////////////////////////////////////////////////////////////////
    /* DOM - Manipulation (Elemente hinzfügen/entfernen - natives JS)*/
    ///////////////////////////////////////////////////////////////////
    var parent = document.getElementById('boxes');
    var box = document.createElement('div');
    box.classList.add('box');
    box.appendChild(document.createTextNode('0'));
    parent.appendChild(box);

    box = document.createElement('div');
    box.classList.add('box');
    box.appendChild(document.createTextNode('1'));
    parent.insertBefore(box, parent.children[2]);

    console.groupEnd('Native DOM-Knoten (NODES)');
  });
})(window, document);

/* 
https://www.w3schools.com/jsref/dom_obj_event.asp 
https://www.w3.org/TR/uievents/

classList
  contains  Prüft ob eine Klasse gesetzt ist
  add       Fügt eine Klasse hinzu
  remove    Remove entfernt eine oder alle Klassen
  toggle    Schaltet eine Klasse um
*/

function addClass(element, classToAdd) {
  if (element.classList) {
    element.classList.add(classToAdd);
  } else {
    element.className += ' ' + classToAdd;
  }
}

function removeClass(element, classToRemove) {
  if (element.classList) {
    element.classList.remove(classToRemove);
  } else {
    var list = element.classList.split(' ');
    var indexPostion = list.indexOf(classToRemove);
    list.splice(1, indexPostion);
    //////////////////////////////////////////
    // var str = element.classList;
    // var indexPostion = str.indexOf(classToRemove);
    // var newStr =
    //   str.substring(0, indexPostion) +
    //   ' ' +
    //   str.substring(indexPostion + classToRemove.length);
  }
}
