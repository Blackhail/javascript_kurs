;
(function (window, document, $) {
  'use strict';
  /* Einzelne Methoden aus der Datei additional-methods.js kopieren und für unsere Zwecke anpassen. 
  Dann kann die Einbinundg im HTML entfallen. 
  VORSICHT: Switch mit Wechsel der Sprachdatei sollte auch hier berücksichtigt werden.*/
  $.validator.addMethod("letterswithbasicpunc", function (value, element) {
    return this.optional(element) || /^[a-zäöüß\-.,()'"\s]+$/i.test(value);
  }, 'Bitte nur Buchstaben und Interpunktion');

  /* default in settings bedeutet wenn wir den Defaultwert nutzen wollen müssen wir die Eigenschaft nicht angeben */
  var settings = {
    // debug: true,
    errorElement: 'label', //=> default: 'label'
    errorClass: 'error_required',
    validClass: 'valid',
    /* Felder werden für Validierung/Prüfung getrimt. */
    normalizer: function (value) {
      return $.trim(value);
    },
    submitHandler: function (form) {
      // form.submit(); // default: submit wenn alle Felder valide sind
      /* https://api.jquery.com/serialize/ 
      Alle Formularfelder werden als name-value-Paare in eine String geschrieben.
      */
      var query = $(form).serialize();
      console.log(query);
      $.get('../lib/data/request.php', query, function (html) {
        $('#ausgabe').empty().append(html);
      });
    },
    errorPlacement: function ($errorElement, $element) {
      /* $element.attr('name') === 'gender' */
      console.log($element, $errorElement);
      if ($element.prop('type') === 'radio') {
        $element.parent().parent().append($errorElement);
      } /* $element.attr('name') === 'gender' */
      else if ($element.prop('type') === 'checkbox') {
        $element.parent().append($errorElement);
      } /* default */
      else {
        $element.after($errorElement); // default
      }
    },
    /* Positionierung der errorClass und validClass am Feld ändern.  */
    highlight: function (element, errorClass, validClass) {
      $(element).addClass(errorClass).removeClass(validClass); //=> default
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).addClass(validClass).removeClass(errorClass); //=> default
    },
    /*  Festlegen welche Felder Pflichtfelder sind und wie diese validiert werden sollen. required ist hier durch die Angabe im HTML nicht nötig! 
    rules entfällt weil die Felder im HTML bereits das Attribut required haben */
    rules: {
      vorname: {
        // required: true // durch HTML required überflüssig
        pattern: /^[a-zäöüß\-.,()'"\s]+$/i,
        minlength: 2,
        maxlength: 5
      },
      nachname: {
        letterswithbasicpunc: true
      }
    },
    messages: {
      nachname: {
        letterswithbasicpunc: 'Bitte nur Buchstaben und Interpunktion AUS messages'
      }
    }
  };

  $(function () {
    $('form').eq(0).validate(settings);
    /* Vorraussetzungen für die Validierung:
    Formular benötigt name-Attribute an den Feldern, diese sind für PHP bzw. das Senden zum Server sowieso nötig. 
    Wird im HTML des Formulars vorab bereits das Attribut required für die HTML5 Validierung gesetzt kann dieser Teil bei den rules im settings Objekt entfallen.
    Ohne requried im HTML-Dokument muss innerhalb der rules für jedes Feld festgelegt werden ob es ein required Feld sein soll:

    rules : {
        vorname : {
          required : true
        }
      }

    Die Eigenschaften im settings-Objekt sind vordefiniert und steuern die Valdierung des Formulars. Mögliche Eigenschaften und Ihre Funktion finden Sie in der Dokumentation. Alle Eigenschaften die mit Ihrem default-Wert belegt sind können auch einfach weggelassen werden. z.B.  validClass : 'valid' muss nicht extra notiert werden, da der Defaultwert eingestellt ist. */
  });
})(window, document, jQuery);

/* 
###############################################################################
Einbindung: 
jquery-x.x.x.min.js
jquery.validate.js
additional-methods.js
messages_de.js

Am Formular werden automatisch folgende Handler registriert:
click, focusin, focusout, keyup und submit.
Nach dem ersten submit werden alle anderen Handler ebenfalls ausgelöst.

settings-Objekt:

https://jqueryvalidation.org/validate/#debug
debug: true | false
Zum erstellen der Settings auf true setzen, in der Produktivseite entfernen oder auf false setzten.

https://jqueryvalidation.org/validate/#submithandler
submitHandler : function(form){}
Funktion die aufgerufen wird wenn alle Felder valide sind.
Der submitHandler funktioniert nur wenn debug: false gesetzt wird.
submitHandler default: Versand an Formular action
form: Das Formular als natives JS-Objekt

onclick: Boolean or Function()
onfocusin: Boolean or Function()
onfocusout: Boolean or Function()
onkeyup: Boolean or Function()
onsubmit: Boolean or Function()

Beispiel onclick deaktiveren
onclick: false
onclick : function(element, event) {return false}

https://jqueryvalidation.org/validate/#errorelement
errorElement : 'Tagname'
Elementtype für Fehlermeldungen
Default: label

https://jqueryvalidation.org/normalizer/
normalizer: function(value) {

}
Eine Methode die auf alle values vor der Validierung angewendet wird.
Wir trimmen in unserem Beispiel die Feldvalues vor der Prüfung.
Wichtig: der Wert im value wird im Browser nicht geändert die Leerzeichen sind noch zu sehen.

https://jqueryvalidation.org/validate/#highlight
https://jqueryvalidation.org/validate/#unhighlight

highlight: function(element, errorClass,validClass) {}
Position der Klasse für Fehler ändern.
element ist hier ein natives JS-Objekt

Example: Adds the error class to both the invalid element and its label

highlight: function(element, errorClass, validClass) {
$(element).addClass(errorClass).removeClass(validClass);
$(element.form).find("label[for=" + element.id + "]")
    .addClass(errorClass);
},
unhighlight: function(element, errorClass, validClass) {
$(element).removeClass(errorClass).addClass(validClass);
$(element.form).find("label[for=" + element.id + "]")
    .removeClass(errorClass);
}




https://jqueryvalidation.org/validate/#errorplacement
errorPlacement : function(error, element) {}
Benutzerdefinierte Positionierung der Fehlermeldungen.
Beide Paramter sind jQuery-Objekte
error bei uns $errorElement: Erzeugtes errorElement mit Fehlermeldungstext
element bei uns $element: Formularelement an dem der Fehler aufgetretten ist

Default:
errorPlacement : function($errorElement, $element) {
    $element.after($errorElement); //=> Default
}

https://jqueryvalidation.org/validate/#errorclass
errorClass : 'klassenname'
Default: error
Klasse festlegen die an den Element mit Fehlern gesetzt wird.
Zusätzlich wird diese Klasse für die erzeugten LABEL-Element gesetzt.

https://jqueryvalidation.org/validate/#validclass
validClass : 'klassenname'
Default: valid
Klasse festlegen die an den validen Element gesetzt wird.

https://jqueryvalidation.org/validate/#rules
rules : {}
Schlüssel-Wert Paare die Regeln für die Validierung festlegen.

rules : {
    vorname : {
        required : true
    }
}
Sind die Felder bereits im HTML-Dokument mit dem Attribut required versehen muss dies in rules nicht mehr angegeben werden.


Das Feld input[name=vorname] ist für die Valdierung als Pflichtfeld gekennzeichnet, die Methode validate erkennt das Feld und validert es.
Voreinstellung für required: Feld darf nicht leer sein.
Bei einem leeren Feld wird eine Standardprozedur durchgeführt:
1. Das input wird mit der Klasse error ausgezeichnet
2. Es wird ein LABEL-Element erzeugt in dem die Fehlermeldung "This field is required." steht.
3. Das LABEL aus Punkt 2 wird im Formular hinter dem input-Element mit DOM-Methoden eingefügt.
4. Am Formular werden mehrere Eventhandler aktiviert. onchance, onblur, onfocus, onclick ... Diese Handler starten eine erneute Validierung des Feldes an dem Sie gefeuert wurde.


A set of standard validation methods is provided:

required – Makes the element required.
remote – Requests a resource to check the element for validity.
minlength – Makes the element require a given minimum length.
maxlength – Makes the element require a given maximum length.
rangelength – Makes the element require a given value range.
min – Makes the element require a given minimum.
max – Makes the element require a given maximum.
range – Makes the element require a given value range.
step – Makes the element require a given step.
email – Makes the element require a valid email
url – Makes the element require a valid url
date – Makes the element require a date.
dateISO – Makes the element require an ISO date.
number – Makes the element require a decimal number.
digits – Makes the element require digits only.
equalTo – Requires the element to be the same as another one

Some more methods are provided as add-ons, and are currently included in additional-methods.js in the download package. Not all of them are documented here:

accept – Makes a file upload accept only specified mime-types.
creditcard – Makes the element require a credit card number.
extension – Makes the element require a certain file extension.
phoneUS – Validate for valid US phone number.
require_from_group – Ensures a given number of fields in a group are complete.


Weitere Validierungsmethoden können über additional-methods.js eingebunden werden.
Für diese Methoden gibt es in messages_de.js keine Übersetzung.
VORSICHT: Die Prüfungen innerhalb der Zusatzmethoden ist für den englischen Sprachraum definiert. 

Beispiel: 

Methode letterswithbasicpunc aus additional-methods.js

$.validator.addMethod( "letterswithbasicpunc", function( value, element ) {
	return this.optional( element ) || /^[a-z\-.,()'"\s]+$/i.test( value );
}, "Letters or punctuation only please" );


Für den deutschen Sprachraum fehlen die Umlaute und Eszett (ß).


$.validator.addMethod( "letterswithbasicpunc", function( value, element ) {
	return this.optional( element ) || /^[a-zäöüß\-.,()'"\s]+$/i.test( value );
}, "Letters or punctuation only please" );



/^[a-z\-.,()'"\s]+$/ muss zu /^[a-zäöüß\-.,()'"\s]+$/ angepasst werden.

###############################################################################
Ändern Sie den Regulären Ausdruck in der externen Datei additional-methods.js
müssen Sie bei einem Versionswechsel daran denken die Methode jedesmal zu ändern.
Alterantiv Lösungen:
1. $.validator.addMethod für eine eigene Methode nutzen
2. Methode pattern mit $.validator.addMethod hinzufügen ohne additional-methods.js einzubinden. Dann kann in rules pattern mit einem Regulären Ausdruck verwendet werden.




$.validator.addMethod( "pattern", function( value, element, param ) {
	if ( this.optional( element ) ) {
		return true;
	}
	if ( typeof param === "string" ) {
		param = new RegExp( "^(?:" + param + ")$" );
	}
	return param.test( value );
}, "Invalid format." );


rules {
    vorname : {
        required : true,
        pattern : '/^[a-zäöüß\-.,()'"\s]+$/i'
    }
}
###############################################################################
*/