/**
 * Buch: Kapitel 4.9 und 4.10
 * https: //overapi.com/jquery Manipulation
 * https: //api.jquery.com/category/manipulation/
 */
;
(function (window, document, $) {
  'use strict';

  $(function () {
    ///////////////////////////////////////////////////////////////////
    /* DOM - Manipulation (Elemente hinzfügen/entfernen - natives JS)*/
    ///////////////////////////////////////////////////////////////////
    var parent = document.getElementsByClassName('boxes')[0];
    var box = document.createElement('div');
    box.classList.add('box');
    box.appendChild(document.createTextNode('X'));
    parent.appendChild(box);

    box = document.createElement('div');
    box.classList.add('box');
    box.appendChild(document.createTextNode('X'));
    parent.insertBefore(box, parent.children[2]);
    /**
     * Ein DOM-Knoten kann immer nur an einer Position vorhanden sein. Jeder Knoten ist einzigartig. Beim verschieben ist kein vorheriges entfernen nötig.
     */
    var boxes = document.getElementsByClassName('box');
    parent.insertBefore(boxes[boxes.length - 1], parent.firstChild);

    /* cloneNode erzeugt eine Kopie des Knotens */
    var newBox = boxes[0].cloneNode(true);
    parent.appendChild(newBox);

    /* Knoten löschen/entfernen */
    console.log(boxes[1].remove());
    /* Knoten löschen/entfernen, hier wird der gelöschte Knoten zurückgeben und kann gespeichert werden. */
    console.log(parent.removeChild(boxes[1]));


    ///////////////////////////////////////////////////////////////////
    /* DOM - Manipulation (Elemente hinzfügen/entfernen - jQuery)    */
    ///////////////////////////////////////////////////////////////////
    var $box = $('.box');
    /* Die Methode text verändert textContent/innerText. Die übergeben callback-FN wird für jedes Element der jQuery-Sammlung aufgerufen. */
    $box.text(function (i) {
      /* Das return schreibt den Rückgabewert in den textContent/innerText des aktuellen Elements. */
      return i;
    });

    /* append gibt das Element zurück in das wir eingefügt haben. */
    console.log('Rückgabe von append: ');
    console.log(
      $('.boxes')
      .append('<div class="box">X</div>')
      .css({
        border: '5px solid lime'
      })
    );

    console.log('Rückgabe von appendTo: ');
    /* appendTo gibt das Element zurück das eingefügt wurde. */
    console.log(
      $('<div class="box">X</div>')
      .appendTo('.boxes')
      .css({
        border: '5px solid lime'
      })
    );



    var $newBox = $box.eq(0).clone();
    $newBox.text('Y');
    $('.boxes').prepend($newBox);

    $newBox.css({
      backgroundColor: 'hotpink'
    });

    var $elements = $('.box').detach();
    console.log($elements);

    $elements = $elements.toArray().sort(function () {
      return Math.random() - 0.5;
    });

    $('.boxes').append($elements);

    $('.column')
      .eq(1)
      .append('<h3>Das ist neuer Inhalt aus jQuery</h3><p>Dieser Inhalt wird als Zeichenkette an append übergeben, die Methode macht daraus DOM-Knoten und fügt diese im DOM ein</p>');


    var $newNode = $('<p/>', {
      text: 'Das ist ein Test!',
      id: 'myId1',
      class: 'marker',
      click: function () {
        alert();
      }
    });


    // $newNode.appendTo('.column:eq(1)');

    $('.column')
      .eq(1)
      .append($newNode);

  });
})(window, document, jQuery);