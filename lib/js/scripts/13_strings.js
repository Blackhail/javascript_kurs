(function (window, document) {
    'use strict';
    // Funktionen und Variablen deklarieren
  
    window.addEventListener('load', function () {
      // Der Code der auf das DOM zugreift hier
  
      //                            0
      //Index             01234567891
      var simpleString = 'Hallo Welt!'; // Literale Schreibweise
      var objectString = new String('Hallo Welt!'); // String-Konstruktor
      console.log(
        'simpleString Inhalt: ',
        simpleString,
        ' / Datentyp: ',
        typeof simpleString
      );
      console.log(
        'objectString Inhalt: ',
        objectString,
        ' / Datentyp: ',
        typeof objectString
      );
  
      var html = '<h2>Zeichenkette/String</h2>';
      html += "new String('Hallo Welt!') //=> " + objectString + '<br>';
      html += 'simpleString[0] //=> ' + simpleString[0] + '<br>';
      html += 'objectString[0] //=> ' + objectString[0] + '<br>';
  
      html += 'simpleString.length //=> ' + simpleString.length + '<br>';
      html += 'objectString.length //=> ' + objectString.length + '<br>';
  
      html +=
        '<p><b>Ein Stringobjekt erzeugen ist meistens überflüssig. Ein elementarer String wird bei Aufruf einer Methode automatisch in einen Objektwrapper verpackt und temporär wie ein Stringobjekt behandelt.</b></p>';
  
      html +=
        'simpleString.toUpperCase() //=> ' + simpleString.toUpperCase() + '<br>';
      html += 'simpleString.charAt(1) //=> ' + simpleString.charAt(1) + '<br>';
      html += 'objectString.charAt(1) //=> ' + objectString.charAt(1) + '<br>';
  
      html +=
        '<p><b>String Methoden im String-Prototypen - Siehe Konsole!</b></p>';
      console.log('String //=> ', String);
  
      html += '<h2>for-Schleife für String (simpleString)</h2>';
      for (var i = 0; i < simpleString.length; i++) {
        html +=
          'simpleString.charAt(' +
          i +
          ') //=> ' +
          simpleString.charAt(i) +
          '<br>';
      }
  
      html += '<h2>for-Schleife für String (objectString)</h2>';
      for (var i = 0; i < objectString.length; i++) {
        html +=
          'objectString.charAt(' +
          i +
          ') //=> ' +
          objectString.charAt(i) +
          '<br>';
      }
  
      document.querySelector('#ausgabe').innerHTML = html;
    });
  })(window, document);
  
  
  ////////////////////////////////////////////////////
  /* Lernskript Strings / Zeichenketten  */
  ////////////////////////////////////////////////////


  /* 1 */
  /* Wie Array habena uch alle Zeichenketten eine Eigenschaft (lenght), die über die DOT-Notation abgefragt werden kann. */
  
  /* 2 */
  /* Die Methoden toUpperCase und toLowerCase. 
  'das ist ein test'.toUpperCase() //=> 'DAS IST EIN TEST' 
  'DAS IST EIN TEST'.toLowerCase() //=> 'das ist ein test' 
  */
  
  /* 3 */
  /* inlcudes:
  Die includes()-Methode gibt an, ob ein String innerhalb eines anderen Strings gefunden wird und gibt dabei true oder false wieder. 
  https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/String/includes*/
  
  console.log(string.includes('Muster')); //=> true
  /* Indexof gibt die Startposition zurück wenn die Suchzeichenkette gefunden wurde oder -1 wenn nichts gefunden wurde. */
  console.log(string.indexOf('Muster')); //=> 4
  /* FALLBACK: Includes mit indexOf nachbilden für IE.
  Die Position der Suchzeichenkette spielt keine Rolle, Sie muss nur vorkommen. 
  Die Prüfung > -1 bedeutet: kommt in der Zeichenkette an einer einer beliebigen Position vor. */
  console.log(string.indexOf('Muster') > -1); //=> true
  
  /*startsWith:
  Die startsWith()-Methode stellt fest, ob ein String mit den Zeichen eines anderen Strings beginnt, falls dies so ist, wird true, sonst wird false zurückgegeben. 
  https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith*/
  console.log(string.startsWith('Max')); //=> true
  console.log(string.startsWith('Alfred')); //=> false
  /* FALLBACK: startsWith  mit indexOf nachbilden. 
  Die Suchzeichenkette muss genau am Anfang der Zeichenkette vorkommen. 
  === 0 bedeutet die Suchzeichenkette kommt vor und beginnt direkt auf Index 0, also am Anfang der Zeichenekten.*/
  console.log(string.indexOf('Max') === 0); //=> true
  console.log(string.indexOf('Alfred') === 0); //=> false
  
  /* endsWith:
  Die Methode endsWith() bestimmt, ob ein String das Ende eines anderen Strings ist, und liefert entsprechend true oder false zurück. 
  https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith*/
  console.log(string.endsWith('Mustermann')); //=> true
  console.log(string.endsWith('Musterfrau')); //=> false
  
  /* FALLBACK: endsWith  mit lastIndexOf nachbilden. */
  var search = 'Mustermann';
  
  console.log(string.lastIndexOf(search) === string.length - search.length); //=> true
  
  search = 'Musterfrau';
  console.log(string.lastIndexOf(search) === string.length - search.length); //=> false
  
  search = 'Muster';
  console.log(string.lastIndexOf(search) === string.length - search.length); //=> false
  
  search = 'MustermannXxx';
  console.log(string.lastIndexOf(search) === string.length - search.length); //=> false
  
  /* 4 */
  /* Bei beiden kann ein Index über den Computed-Member-Access, [], ausgelesen werden. Sie haben eine Eigenschaft (length) und einen Objekt-Prototypen von dem Sie Ihre Methoden erben.  */
  console.log(String); //=> Prototype (Bauplan)
  console.log(String.prototype); //=> Methodenliste
  
  /* 5 */
  /* Elementare Datentypen sind immutable. Der Stringindex kann nicht durch eine Zuweisung geändert werden. */
  
  /* 6 */
  /* Die Methoden funktionieren zum aktuellen Zeitpunkt (15.07.2020) nicht in allen Browser! Fallbacks bzw. 
  Alternativen mit indexOf wurden im Code bereis aufgezeigt. */
  
  /* 7 */
  /* Siehe 3, wurde bereis besprochen */
  
  /* 8 */
  /* Computed-Member-Access und die Methode charAt() */
  
  /* 9 */
  /* 
  substring(startindex, endindex(exklusive))
  substr(startindex, anzahl)  */
  console.log(string.substring(2, 4));
  console.log(string.substr(2, 4));
  
  /* 10 */
  /* indexOf */
  
  /* 11 */
  /* lastIndexOf */
  
  /* 12 */
  /* Buch 6.2.3 Suche und Teilzeichenketten Seite 170/171 */
  var csv = 'Hans;Meiser;Max;Mustermann';
  var pos = csv.indexOf(';');
  while (pos !== -1) {
    console.log(pos);
    pos = csv.indexOf(';', pos + 1);
  }
  
  /* match mit einer Zeichenkette ähnelt indexOf, gibt aber ein Array mit dem Treffer zurück, 
  das zusätzlich noch die Position und den Originalinput enthält.
  Um das volle Potenzial von match zu nutzen muss man mit Regulären Ausdrücken arbeiten. */
  var found = csv.match(';');
  console.log(found);
  
  /* Hier erhalten wir ein Array mit allen Treffern aber ohne Indexpositionen. */
  found = csv.match(/;/g);
  console.log(found);
  
  var regEx = /;/g;
  var found;
  while ((found = regEx.exec(csv))) {
    console.log(found, found.index);
  }
  
  var template = 'AxBxCxDxExF';
  /* 13 */
  /* RegEx-Methode replace */
  console.log(template.replace('x', ' '));
  /* Flags:
  g - globale (alle Vorkommen)
  i - case insensitive (Groß - und Kleinschreibung ignorieren) */
  console.log(template.replace(/x/gi, ' '));
  
  /* RegEx-Objekt in JavaScript */
  var regEx = new RegExp('x', 'gi');
  console.log(template.replace(regEx, ' '));
  
  /* join und split als Beispiel für Frage 14 und 15*/
  console.log(template.split('x').join(' '));
  
  /* WICHTIG: Das Original bleibt unverändert.  */
  console.log(template);
  
  /* 14 */
  /* Methode split */
  
  /* 15 */
  /* Methode join */
  