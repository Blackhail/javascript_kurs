'use strict';
function mehrereWerte() {
  return {
    x1: 2,
    x2: 4,
    x3: 10,
  };
}

/* Funktionsdeklaration */
function add(zahl1, zahl2) {
  /* erg ist hier lokal. Eine Variable der FN add */
  var erg = zahl1 + zahl2;
  return erg; // Hier wird der Inhalt/Wert aus Variable erg zurückgegeben
}

/* Funktionsaufruf: Die Argumente werden kommasepariert in die Funktion übergeben und landen dort in arrayartgigen Objekt arguments und/oder in den vorher definierten Parametern. Der Rcükgabewert ersetzt den Funktionsaufruf. Zurückgegeben wird ein Wert, nicht die Variable (hier erg.)  */
add(22, 11); //=> 33
console.log(add(12, 2));
/* Globale Variable erg  */
var erg = add(100, 24); // var erg = 124;
document.write(erg + '<br>');

console.log(mehrereWerte());

function defaullWerte(a, b, c) {
  //Default Wert in alten Browser
  var c;
  if (arguments.length > 2 && arguments[2] !== undefined) {
    c = arguments[2];
  } else {
    c = 12;
  }

  if (typeof a !== 'string') {
    console.error('Wert a muss string sein');
  }
}

var array = ['a', 'b', 'c', 'd', 'e'];
// array.forEach(function () {
//   console.log(arguments);
// });

array.forEach(print); // print als Referenz kein Aufruf

console.log('***************************************');

function print(value, index, array) {
  // console.log(arguments);
  console.log(value, index, array);
}

function myForEach(array, callbackFN) {
  for (var i = 0; i < array.length; i++) {
    callbackFN(array[i], i, array);
  }
}

myForEach(array, print);
////////////////////////////////////////////////////////////////
function nurGerade(zahl) {}
function nurUngerade(zahl) {}

function nurVokale(wort) {}

var a = [53, 684, 8658, 89, 478, 55];
var b = ['a', 'b', 'c', 'd', 'e'];

sortiere(a, nurGerade);
sortiere(a, nurUngerade);

sortiere(b, nurVokale);

function sortiere(array, sortierfunktion) {
  // Schleife läuft über array und nutzt sortierfunktion um
  // neues Array zu erzeugen
}

////////////////////////////////////////////////////////////////
var months = ['March', 'Jan', 'Feb', 'Dec'];
months.sort();
console.log(months);

var zahl = [53, 684, 8658, 89, 478, 55];
zahl.sort(function (a, b) {
  return a > b;
});
console.log(zahl);

////////////// Standard return //////////////////////////////////////////////////
function name(a, b) {
   var x = a + b;
    return x;
  }

  var a = 2;
  var b = 4;
  var c;
  c = name(a, b);
  console.log(c);