'use strict';
function init() {
  /* Elementare Datentypen (Primitive Datentypen) */
  var string = 'Hello World';
  var IntNumber = 12;
  var floatNumber = 1.245;
  var boolean = true;

  /* Objektdatentypen */
  function fn() {
    return 'my Return';
  }

  var fnExp = function () {};

  var array = [12, 56, 3, 150];
  var object = { x1: 12, x2: 30 };

  /* DOT-Notation */
  console.log(object.x1);
  /* Arrayartige-Notation */
  console.log(object['x1']);

  var person = {
    vorname: 'John',
  };

  console.log(person.vorname);
  /* Nachträglich ein neues SChlüsselwert-Paar einfügen */
  person.nachname = 'Wick';
  console.log(person.nachname);
  console.log('**********************************');

  /* Objekt als Datenspeicher (Literalschreibweise)*/
  var book = {
    name: 'Professionell entwickeln mit JavaScript',
    author: 'Philip Ackermann',
    publisher: 'Rheinwerk Computing; Auflage: 2 (22. Juni 2018)',
    price: 49.9,
    isbn10: '9783836256872',
    isbn13: '978-3836256872',
  };

  var html = '';

  html += '<h2>for-in-Schleife für Objekte</h2>';
  html +=
    '<p><b>Für Syntax: Quellcode im Editor oder Debugger anschauen</b></p>';
  for (var key in book) {
    var value = book[key];
    /* Computed-Member-Access liest den Inhalt der Variable key als Zeichenkette aus. */
    console.log(key, ' ', book[key]);
    /* DOT-Notation funktioniert nicht! Es gibt keine Eigenschaft namens key. */
    // console.log(key, ' ', book.key);
    html +=
      'key: ' +
      key +
      " <b>|</b> book[key] -> book['" +
      key +
      "'] //=> " +
      book[key] +
      '<br>';
  }

  html += '<h2>for-Schleife/join mit Object.keys und Object.values</h2>';
  var keys = Object.keys(book);
  var values = Object.values(book);

  for (var i = 0; i < keys.length; i++) {
    html += 'keys[i] -> keys[' + i + '] //=> ' + keys[i] + '<br>';
    html += 'book[keys[i]] //=> ' + book[keys[i]] + '<br>';
    html += 'values[i] -> values[' + i + '] //=> ' + values[i] + '<hr>';
  }

  html +=
    '<b>Object.keys(book)</b>' +
    '<ul><li>' +
    keys.join('</li><li>') +
    '</li></ul>';
  html +=
    '<b>Object.values(book)</b> ' +
    '<ul><li>' +
    values.join('</li><li>') +
    '</li></ul>';

  /* Array als Datenspeicher hat den Nachteil das die Indizes numerisch sind. 
Das Objekt hat assoziative Indizes. Arrays speichern im idealfall viele gleichartige und zusammengehörige Werte. */
  var bookArray = [
    'Professionell entwickeln mit JavaScript',
    'Philip Ackermann',
    'Rheinwerk Computing; Auflage: 2 (22. Juni 2018)',
    49.9,
    '9783836256872',
    '978-3836256872',
  ];
  /* 
  Ein Array in JavaScript ist ein Objekt mit automatisch vergebenen 
  numerischen Indizes und einer ebenfalls automtisch ermittelten Eigenschaft lenght. Zusätzlich erbt jedes Array vom Array-Prototypen seine Methoden wie z.B. push, pop, slice ...

  {
    '0' : 'Professionell entwickeln mit JavaScript',
    '1' :'Philip Ackermann',
    '2' :'Rheinwerk Computing; Auflage: 2 (22. Juni 2018)',
    '3' :49.90,
    '4' :'9783836256872',
    '5' :'978-3836256872',
    length: '6',
    <prototype>: Array []
  }
  */

  document.querySelector('#ausgabe').innerHTML = html;
}
window.onload = init;
