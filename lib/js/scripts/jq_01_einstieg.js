'use strict';
/* Die Factory FN kann über $ oder jQuery zeigen beide auf die FN. 
 Viele Bibliotheken nutzen das $, somit kann jQuery als Fallback für den 
 Zugriff genutzt werden. */
console.log('window //=> ', window);
console.log('window.$ //=> ', window.$);
console.log('window.jQuery //=> ', window.jQuery);

/* Beispiel: Hier wird der $ global überschrieben und enthält dann nicht mehr die jQuery Referenz.  */
var $ = document.querySelector.bind(document);

/* $ durch Zeile 10 überschrieben */
console.log('$ //=> ', $);
console.log('jQuery //=> ', jQuery);

/* IIFE für eigenen Gültigkeitsbereich, hier wird jQuery in die Parameter $ der IIFE übergeben. Das wird unser Basis Snippet für alle jQuery-Dateien */
(function (window, document, $) {
  /* Factory als Referenz */
  console.log('$ //=> ', $);
  /* Factory-FN wird aufgerufen und gibt ein Objekt zurück */
  console.log('$() //=> ', $()); //=> Object {  }
  /* 
  {}​ 
  < prototype >: Object {
    jquery: "3.5.1",
    constructor: jQuery(selector, context),
    length: 0,
    …
  }
  */
  window.addEventListener('load', function () {
    console.log('$(\'h1\') //=> ', $('h1'));
    /* Object { 0: h1, length: 1, prevObject: {…} } 
    {…}​
    0: < h1 > ​
      length: 1​
    prevObject: Object {
        0: HTMLDocument http: //localhost/kurs/javascript/pages/jq_einstieg.html, length: 1 }
          ​
          <
          prototype >: Object {
            jquery: "3.5.1",
            constructor: jQuery(selector, context),
            length: 0,
            …
          }
    
    jQuery-Sammlung:
    Enthält alle Elemente passend zum übergeben Selektor $('h1')
    In numerischen Indizes finden wir die nativen JS-Objekte (DOM-Knoten.)
    Das Objekt hat eine Eigenschaft length, enthält Anzahl der Elemente
    Im prototype befinden sich alle Eigenschaften und Methoden die an einem jQuery-Objekt vordefiniert aufgerufen können.
    */

    /* Beispiel Methode .html() */
    console.log('$(\'h1\').html() //=> ', $('h1').html());
    console.log('$(\'h1\').css({ color : \'red\'}) //=> ', $('h1').css({
      color: 'red'
    }));

    /* html is not a function. Das native JS-Objekte hat keine Methode html */
    // console.log(document.getElementsByTagName('h1')[0].html());
    console.log(document.getElementsByTagName('h1')[0].innerHTML);

    /* Das jQuery-Objekt hat keine Eigenschaft innerHTML. Wir erhalten den Wert undefined */
    console.log('$(\'h1\').innerHTML //=> ', $('h1').innerHTML); //=> undefined
    /* RIESIGE FEHLERQUELLE:
    Im DOM passiert nichts. Wir sehen keine Änderung im Brower.
    Die DOT-Notation erzeugt eine Eigenschaft innerHTML, diese hat aber keine Auswirkung auf das innerHTML des DOM-Elements.  */
    var $el = $('h1');
    $el.innerHTML = 'TEST';
    console.log($el);
    /* {…}
      ​0: <h1 style="color: red;">
      ​innerHTML: "TEST"
      ​length: 1
      ​prevObject: Object { 0: HTMLDocument http://localhost/kurs/javascript/pages/jq_einstieg.html, length: 1 } 
    */

    /* Methode html am jQuery Objekt */
    $el.html('Einstieg in jQuery, erweiteret mit jQuery');
    /* Natives JS-Objekt mit innerHTML */
    $el[0].innerHTML += '! durch natives JS hinzugefügt.'

    // console.log('$(\'p\') //=> ', $('p'));
    /* Object { 0: p.lead, 1: p, 2: p.blue, length: 3, prevObject: {…} } */


  });

})(window, document, jQuery);


/* Funktion Objekt zurück!? */
/* Funktionsreferenz */
/* jQuery-Objekt vs Objekt? */
/* Was ist ein prototype? */