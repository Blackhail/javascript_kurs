/* JS Dateien werden oftmals, beim laden,  zusammengeführt oder hintereinander eingebunden.
Da JS kein Semikolon am einer Zeile benötigt, schließt man bei jedem neuen Dokument zur Sicherheit einen vorher geschrieben Befehl mit einem Semikolon ab. */
;
(function (window, document, $) {
  'use strict';
  $(function () {
    ////////////////////////////////////////////////////////////////////////////
    /* 
     * SELEKTION: Sammlungen erzeugen und bearbeiten    
     */
    ////////////////////////////////////////////////////////////////////////////  
    console.log('*************************************************************');
    console.log('______ CSS Selektoren ______');
    console.log('$(\'#list\') //=> ', $('#list'));
    console.log('$(\'.blue\') //=> ', $('.blue'));
    console.log('$(\'div\') //=> ', $('div'));
    console.log('$(\'[name=shorty]\') //=> ', $('[name=shorty]'));
    console.log('$(\'.box:nth-child(2)\') //=> ', $('.box:nth-child(2)'));

    console.log('______ jQ Pseudoselektoren/Filter Selektoren ______');
    console.log('$(\'.box:empty()\') //=> ', $('.box:empty()'));
    console.log('$(\'.box:contains(2)\') //=> ', $('.box:contains(2)'));
    console.log('$(\'a:contains(Amazon)\') //=> ', $('a:contains(Amazon)'));
    console.log('$(\'p:contains(exercitationem)\') //=> ', $('p:contains(exercitationem)'));

    /* $ ist nur eine Kennzeichnung, nicht nötig! Zeigt das hier ein jQuery-Objekt gespeichert wird, ist aber OPTIONAL! */
    var el = $('p:contains(exercitationem)');
    var $el = $('p:contains(exercitationem)');
    var txt = $el.html();
    txt = txt.replace('exercitationem', '<b>JAVASCRIPT</b>');
    $el.html(txt);

    /* 
    Content Filters
    :contains()
    :empty
    :has()
    :parent
    Basic Filters
    :animated
    :eq()
    :even
    :first
    :gt()
    :header
    :last
    :lt()
    :not()
    :odd
    */
    /* eq entspricht equal. Zahl die übergeben wird ist die Indexposition beginnend bei 0  */
    console.log('$(\'.box\') //=> ', $('.box'));
    /*  
    0: div.box​
    1: <div class="box">​
    2: <div class="box">​
    3: <div class="box">​
    4: <div class="box">​
    5: <div class="box">
    ​
    length: 6
    ​
    prevObject: Object { 0: HTMLDocument  
    */
    console.log('$(\'.box:eq(0)\') //=> ', $('.box:eq(0)'));
    console.log('$(\'.box:first\') //=> ', $('.box:first'));
    console.log('$(\'.box:gt(2)\') //=> ', $('.box:gt(2)')); //=> Ab Index 3
    console.log('$(\'.box:lt(2)\') //=> ', $('.box:lt(2)')); //=> Index 0 und 1
    console.log('$(\'.box:last\') //=> ', $('.box:last'));
    /* Alle Boxen auf geraden (even) 0,2,4,6 ... Indizes */
    console.log('$(\'.box:even\') //=> ', $('.box:even'));
    /* Alle Boxen auf ungeraden (odd) 1,3,5,7 ... Indizes */
    console.log('$(\'.box:odd\') //=> ', $('.box:odd'));

    var box = document.getElementsByClassName('box');
    var even = [];
    var odd = [];
    for (var i = 0; i < box.length; i++) {
      if (i % 2 === 0) {
        even.push(box[i]);
      } else {
        odd.push(box[i]);
      }
    }

    $('#list li:odd').css({
      backgroundColor: 'pink'
    });

    $('#list li:even').css({
      backgroundColor: 'lime'
    });

    console.log('******************************************');
    console.log('______ Pseudoselektoren als Methode ______');
    var $boxes = $('.box');
    console.log('var $boxes = $(\'.box\') //=> ', $('.box'));
    console.log('$boxes.eq(0) //=> ', $boxes.eq(0));
    console.log('$boxes.first() //=> ', $boxes.first());
    console.log('$(\'.box\').eq(0) //=> ', $('.box').eq(0));

    console.log('______ Selektion verfeinern ______');
    var $list = $('#list');
    console.log('var $list = $(\'#list\') //=> ', $('#list'));
    /* find sucht alle Nachfahren des angegeben Selektors in allen Elementen der Sammlung. */
    console.log('var $list.find(\'a\') //=> ', $list.find('a'));

    /*  
    0: div.box​
    1: <div class="box">​
    2: <div class="box">​
    3: <div class="box">​
    4: <div class="box">​
    5: <div class="box">
    ​
    length: 6
    ​
    prevObject: Object { 0: HTMLDocument  
    */


    //////////////////////////////////////////////////////////////////////////
    // /* Funktioniert nicht. */
    // var $elelements = $('p:contains(Lorem ipsum)');
    // /* Nur der Text aus dem ersten Absatz */
    // var txt = $elelements.html();
    // txt = txt.replace('Lorem ipsum', '<b>JAVASCRIPT</b>');
    // /* Der Text aus Absatz 1 wird in jedem Absatz eingefügt. */
    // $elelements.html(txt);
    //////////////////////////////////////////////////////////////////////////
    var $elements = $('p:contains(Lorem ipsum)');
    for (var i = 0; i < $elements.length; i++) {
      var $el = $elements.eq(i);
      var txt = $el.html();
      txt = txt.replace('Lorem ipsum', '<b>jQuery</b>');
      $el.html(txt);
    }

    /* Shorthand von jQuery:  */
    // $('p:contains(Lorem ipsum)').html(function (i, txt) {
    //   return txt.replace('Lorem ipsum', '<b>jQuery</b>');
    // });


    console.log('*********************************************');
    console.log('______ Pseudoselektoren als Methode ______');
    console.log($('#list a'));
    console.log($('#list a:eq(1)'));
    console.log($('#list a:even()'));

    /* Lieber die Methode als einen Pseudoselektor verwenden */
    // #list a:eq(1)
    console.log(
      $('#list')
      .find('a')
      .eq(1)
    );

    console.log(
      $('#list')
      .find('a')
      .even()
    );


    console.log('*********************************************');
    console.log('______ Pseudoselektor empty vs Methode empty() ______');
    /* Die Methode empty() entfernt alle Kindknoten innerhalb aller Elemente einer Sammlung. Der Pseudoselektor :empty wählt alle leeren Element an. */
    // $('.box').empty();

    console.log($('.box:empty'));

    var box = document.getElementsByClassName('box');
    var elements = [];
    for (var i = 0; i < box.length; i++) {
      if (box[i].innerHTML === '') {
        elements.push(box[i]);
      }
    }
    console.log(elements);


  });
})(window, document, jQuery);