;
(function (window, document, $) {
  'use strict';

  function exampleData() {
    var key = 'jQuery351' + Date.now();
    // var key = 'cheesecake';
    console.log(key);
    document.body[key] = 'Daten die gespeichert werden';
    console.log(document.body);
  }

  function task1() {
    var $list = $('#list');
    console.log('$(\'#list\'): ', $list);
    /*https://api.jquery.com/attr/
     Get the value of an attribute for the first element in the set of matched elements or set one or more attributes for every matched element. */
    console.log('$list.attr(\'id\'): ', $list.attr('id'));
    /* https://api.jquery.com/prop/#prop-propertyName 
    Get the value of a property
    for the first element in the set of matched elements or set one or more properties for every matched element.*/
    console.log('$list.prop(\'id\'): ', $list.prop('id'));
    console.log('$list.attr(\'nodeName\'): ', $list.attr('nodeName'));
    console.log('$list.prop(\'nodeName\'): ', $list.prop('nodeName'));

    /* Am nativen JS Objekt kann getAttribute, im HTML gesetzte Attribute auslesen. DOM-Eigenschaften wie nodeName können nicht gelesen werden. */
    console.log('$list[0].getAttribute(\'id\'): ', $list[0].getAttribute('id'));
    console.log('$list[0].getAttribute(\'nodeName\'): ', $list[0].getAttribute('nodeName'));

    /* DOT-Notation kann Attribute und Eigenschaften auslesen. */
    console.log('$list[0].id: ', $list[0].id);
    console.log('$list[0].nodeName: ', $list[0].nodeName);
    /**
     * attr für Attribute die am HTML-Element gesetzt wurden (id, title, alt ...)
     * prop für Eigenschaften des DOM-Knotens (tagName, nodeType ...)
     * https: //api.jquery.com/prop/#prop-propertyName Vergleich Attributes vs. Properties
     */
  }

  function handleSubmit(e) {
    e.preventDefault(); // Debug#
    var myform = $('body')[0];
    console.log($('body').data('clicked'));
    console.log($.hasData(myform));
    if ($.hasData(myform)) {
      var value = $('#input').val().trim();
      if (value !== '') {
        /**
         * https: //api.jquery.com/jQuery.hasData/
         * Prüft am nativen JS-Objekt ob jQuery über die Methoden data
         * eine Data-Eigenschaft gesetzt hat.
         * 
         * Alternativ kann $('body').data('clicked') ausgelesen werden:
         * Kein Wert gespeichert undefined
         * Wert gespeichert, der Wert ist in der Eigenschaft
         * if($('body').data('clicked') !== undefined)
         */

        /*  $('body').data('clicked') !== undefined */


        $('body').data('clicked').text(value);
        $('#input').val('');
        $('body').removeData('clicked');

      }
    }

    return false;
  }

  function handleClick() {
    $('#input').val($(this).text());
    $('body').data('clicked', $(this));
    /* Die Methode data legt am nativen DOM-Knoten-Objekt eine Eigenschaft an in der unser Wert gespeichert wird. Eigenschaftsname: jQuery351XXXXXXXXXXXXX. */
    // console.log($('#myform'));
    return false;
  }

  $(function () {
    // exampleData();
    // task1();
    $('#list').find('a').on('click', handleClick);
    $('#myform').on('submit', handleSubmit);
    //////////////////////////////////////////////////////////////////////////
    // Sobald ein Element einen Eventhandler hat, gibt $.hasData(element)   //
    // true zurück!                                                         //
    //////////////////////////////////////////////////////////////////////////

  });
})(window, document, jQuery);