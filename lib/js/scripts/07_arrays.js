'use strict';
/*  */
console.log(Array);
/* Literalschreibweise */
var names1 = [];
/* Konstruktorschreibweise */
var names2 = new Array();

// Index                0        1
var anotherArray1 = ['Hallo', 'Welt'];
var anotherArray2 = [];

console.log(anotherArray1[0]); //=> 'Hallo'
console.log(anotherArray1[1]); //=> 'Welt'

console.log(new Array(1, 2, 3, 4));
console.log([1, 2, 3, 4]);
/* VORSICHT:  */
console.log(new Array(4)); //=> [undefined,undefined,undefined,undefined]
console.log([4]); //=> [4]

var leeresArray1 = new Array(); //=> []
var leeresArray2 = []; //=> []

console.log('Anzahl Element im Array (leeresArray1): ', leeresArray1.length);
console.log('Anzahl Element im Array (leeresArray2): ', leeresArray2.length);

leeresArray1 = new Array(3); //=> [undefined,undefined,undefined]
leeresArray2 = [3]; //=> [3]
console.log('Anzahl Element im Array (leeresArray1): ', leeresArray1.length); //=> 3
console.log('Anzahl Element im Array (leeresArray2): ', leeresArray2.length); //=> 1
console.log(leeresArray1[0]); //=> undefined
console.log(leeresArray2[0]); //=> 3

console.log(leeresArray1[1]); //=> undefined
console.log(leeresArray2[1]); //=> undefined

/* Vergleichen führt  */
console.log(leeresArray1 === leeresArray2);
console.log([] === []);
console.log([1, 2] === [1, 2]);
console.log('**********************************************************');

var stuff = [];
while (stuff.length < 10) {
  stuff.push(1);
}

console.log(stuff);

/* Anzahl Element auf 0 setzten leert das Array */
stuff.length = 0;
/* Array hat 10 leere Plätze */
stuff.length = 10;

for (var i = 0; i < stuff.length; i++) {
  stuff[i] = 1;
}
console.log(stuff, stuff.length);

/* Lückenhaftes Array Indizes 10-99 sind mit undefined vorbelegt/reserviert. 
Vorbelegt undefined ist nicht gleich dem Rückgabewert undefined für nicht belegte Indizes. Wird ein Index aufgerufen den es nicht gibt erhalten wir den Wert undefined. */
stuff[100] = 'X';
console.log(stuff, stuff.length);

console.log('**********************************************************');

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
// Ein Array erstellen
var fruits = ['Apple', 'Banana'];
console.log(fruits.length);
// 2

// Zugriff auf ein Arrayelement (mit Index)
var first = fruits[0];
// Apple

var last = fruits[fruits.length - 1];
// Banana

// Über ein Array Iterieren (forEach)
fruits.forEach(function (item, index, array) {
  console.log(item, index);
});
// Apple 0
// Banana 1

// Über ein Array Iterieren (for)
for (var index = 0; index < fruits.length; index++) {
  var item = fruits[index];
  console.log(index, item);
}

// Ein Element am Ende des Arrays einfügen
var newLength = fruits.push('Orange');
// ["Apple", "Banana", "Orange"]

// Ein Element am Ende des Arrays löschen
var last = fruits.pop(); // remove Orange (from the end)
// ["Apple", "Banana"];

// Ein Element am Anfang des Arrays löschen
var first = fruits.shift(); // remove Apple from the front
// ["Banana"];

// Ein Element am Anfang des Arrays einfügen
var newLength = fruits.unshift('Strawberry'); // add to the front
// ["Strawberry", "Banana"];

// Den Index eines Elements im Array ermitteln
fruits.push('Mango');
// ["Strawberry", "Banana", "Mango"]

var pos = fruits.indexOf('Banana');
// 1

// Ein Element mithilfe eines Index aus dem Array löschen
var removedItem = fruits.splice(pos, 1); // this is how to remove an item
// ["Strawberry", "Mango"]

// Elemente von einer Indexposition aus löschen
var vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
console.log(vegetables);
// ["Cabbage", "Turnip", "Radish", "Carrot"]

var pos = 1,
  n = 2;

var removedItems = vegetables.splice(pos, n);
// this is how to remove items, n defines the number of items to be removed,
// from that position(pos) onward to the end of array.

console.log(vegetables);
// ["Cabbage", "Carrot"] (the original array is changed)

console.log(removedItems);
// ["Turnip", "Radish"]

// Ein Array kopieren
var shallowCopy = fruits.slice(); // this is how to make a copy
// ["Strawberry", "Mango"]

console.log('***************************************************');
var a = [];
console.log(a.length, a);
a.push('a'); //=> 1
console.log(a.length, a);
console.log(a.push('b')); //=> 2
console.log(a.length, a);
/* push legt das neue Elemente bzw. die neuen Elemente im Array auf den/die freien Indizes am Ende und gibt dann den neuen Wert der Eigenschaft .length zurück. 

Der letzte belegte Index ist immer um 1 kleiner als der Wert aus Eigenschaft .length. Die length Eigenschaft ist immer um 1 größer als der letzte belegte Index.

*/
console.log('***************************************************');
console.log('Lückenhaftes Array:');
var a1 = ['a', 'b'];
console.log(a1.length, a1);
a1[12] = 'l';
console.log(a1.length, a1);
console.warn(
  'Um lückenhafte Array zu vermeiden, sollten neue Werte im Array mit push eingefügt werden!'
);

console.log('***************************************************');
console.log('Frankensteins Monster:');
var frankensteinsMonster = ['a', 'b'];
console.log(frankensteinsMonster.length, frankensteinsMonster);
frankensteinsMonster['Erzeuger'] = 'Dr. Frankenstein';
console.log(frankensteinsMonster.length, frankensteinsMonster);
console.warn(
  'Wir erhalten eine Kombination aus Objekt und Array. Wir haben numerische Inidzes und eine Eigenschaft .length. Die Eigenschaft erfasst aber nur die Anzahl der numerischen Inidzes. Assoziative Inidzes sind nicht über eine numerischen Index verfügbar.'
);

var len = frankensteinsMonster.length;
for (var i = 0; i < len; i++) {
  console.log(frankensteinsMonster[i]);
}
console.log('frankensteinsMonster[3] ', frankensteinsMonster[3]);
console.log(
  "frankensteinsMonster['Erzeuger'] ",
  frankensteinsMonster['Erzeuger']
);
console.log('***************************************************');
/* JS kennt keine assoziativen Arrays aber Objekte mir assoziativen Indizes. */
var objekt = {
  name: 'Heath Wave',
  author: 'Richard Castle',
  isbn: '9783864250071',
  preis: 10.45,
};

console.log('objekt.length ', objekt.length); //=> undefined
console.warn(
  'Ein Objekt hat keine numerischen Inidizes und auch keine Eigenschaft .length. ABER es gibt eine Schleife für Objekte. for-in'
);

for (var key in objekt) {
  var value = objekt[key];
  console.log(key, ' ', value);
}
console.log('objekt.preis ', objekt.preis);
/* Member-Access mit . */
objekt.preis = 9.99;
/* Computed-Member-Access mit [], Schlüssel muss als Zeichenkette/String angegeben werden. */
objekt.preis = 9.99;
// objekt['preis'] = 9.99;
console.log('objekt.preis ', objekt.preis);

/* LEGAL aber keine gute Progammierung: */
var o = {
  'Haus mit Hund vor der Tür': 'WTF?',
  '123hello': 'WTF!',
};

console.log(o['Haus mit Hund vor der Tür']);
console.warn(
  'Für Eigenschaftsnamen gelten die gleichen Regeln wie Variablenbezeichner. Am besten in Englisch ohne Sonderzeichen oder deutsche Umlaute. Mit Kleinbuchstaben und nicht mit Zahlen beginnen ...'
);

var buch = ['Heath Wave', 'Richard Castle', '9783864250071', 10.45];
var buch = {
  name: 'Heath Wave',
  author: 'Richard Castle',
  isbn: '9783864250071',
  preis: 10.45,
};

/* Objektdatentypen werden referenziert. */
var a = { x1: 2, x2: 4 };
var b = a;
var copyA = Object.assign({}, a); // VORSICHT: Flache Kopie

a.x1 = 'ABC';
console.log(a);
console.log(b);
console.log(copyA);

/* Elementardatentypen werden nicht referenziert, hier wird der Inhalt kopiert. */
var c = 12;
var d = c;

c = 24;
console.log(c);
console.log(d);

var z = { a: 1, b: 2, c: 'hello' };
console.log(JSON.stringify(z)); //=> ' {"a":1,"b":2,"c":"hello"}'
console.log(JSON.parse('{"a":1,"b":2}')); //=> { a: 1, b: 2, c: "hello" }

var copy = JSON.parse(JSON.stringify(z));

// var array = [1, 2, 3];
// var newArray = array.slice();
// console.log(array);
// console.log(newArray);

var array = [1, 2, ['a', 'b'], 3];
var newArray = array.slice();
var realCopy = JSON.parse(JSON.stringify(array));
console.log(array);
console.log(newArray);

newArray[2][0] = 'X';

console.log(array);
console.log(newArray);
console.log(realCopy);

/////////////////////////////////////////////////
var a = { x1: 2, x2: { a: 1, b: 2 } };

var aCopy = Object.assign({}, a);
a.x2.a = 'XXXX';
console.log(a);
console.log(aCopy);
console.warn(
  'Object.assign und Array.slice erzeugen eine flache Kopie. 
  Hat das Objekt oder Array mehrere Dimensionen, 
  also sind weitere Objekte oder Arrays verschachtelt, 
  werden diese nur referenziert. Die beste Methode für eine  
  Deep Copy ist JSON.stringify in Kombination mit JSON.parse.'
);
