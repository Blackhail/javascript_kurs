'use strict';
var html = '';

var sum = 0;

html += 'sum //=> ' + sum + '<br>';
html += '<b>if(sum &lt; b 4) {...}</b><br>';
/* WENN: Einmalig den Anweisungsblock {} ausführen wenn die Bedingung erfüllt ist */
if (sum < 4) {
  sum += Math.random();
}
html += 'sum //=> ' + sum + '<hr>';

sum = 0;
html += 'sum //=> ' + sum + '<br>';
html += '<b>while(sum &lt; b 4) {...}</b><br>';
/* SOLANGE: Anweisungblock {} wird ausgeführt solange die Bedingung erfüllt ist. */
while (sum < 4) {
  sum += Math.random();
}
html += 'sum //=> ' + sum + '<hr>';

var i = 1;
while (i < 11) {
  html += i + ' ';
  i++;
}
html += '<hr>';

var save = 0;
i = 1;
while (i < 10) {
  console.log(i);
  html += i + ' ';
  /* Hier wurde i++ vergessen */

  /* Sicherung! */
  if (save++ > 1000) {
    console.log('ENDLOSSCHLEIFE verlassen');
    break;
  }
}
/* save++ bedeutet: save = save + 1; */
html += '<hr>';
var i = 1;
for (; i < 11; ) {
  html += i + ' ';
  i++;
}
html += '<hr>';

for (var i = 1; i < 11; i++) {
  html += i + ' ';
}
/* Mit var ist der Schleifeniterator, hier i, nach der Schleife um 1 größer als im letzten Schleifendurchlauf. Das könnte durch die Nutzung von let verhindert werden. */
console.log(i); //=> 11
html += '<hr>';
html += '<b>do{...}while(i &lt; 4);</b><br>';
i = 4;
do {
  html += i += ' ';
  i++;
} while (i < 4);

html += '<hr>';
html += '<b>while(i &lt; 4){...}</b><br>';
i = 4;
while (i < 4) {
  html += i += ' ';
  i++;
}

document.write(html);
