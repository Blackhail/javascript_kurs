<?php
/* Prüfung: Wurde die Seite mit Query-String angesprochen? */
if ( isset( $_GET['pnr'] ) ) {
    /* Sicherheitsprüfung: Ist die übergebene PNR überhaupt gültig. Bei größeren DBS liest man die PNR hier aus der DB aus und prüft, im Beispiel nur hardkodiert. */
    $pnr = $_GET['pnr'];
    $possibile_pnrs = ['2223', '2356', '5687', '2323', '2563'];
    if ( in_array( $pnr, $possibile_pnrs ) ) {
        //////////////////////////////////////////////////////////////////////
        $mysqli = new mysqli( 'localhost', 'root', '', 'kunde' );

        if ( $mysqli->connect_error ) {
            echo 'Fehler bei der Verbindung: ' . $mysqli->connect_error . '<br>';
            die();
        }

        if ( !$mysqli->set_charset( 'utf8' ) ) {
            echo 'Fehler beim Laden von UTF8 ' . $mysqli->error;
        }

        $response = $mysqli->query( "SELECT * FROM personal WHERE personalnummer='$pnr'" );
        // $data = $response->fetch_array();
        $data = $response->fetch_assoc();
        echo '<ul>';
        foreach ( $data as $key => $value ) {
            echo '<li>'. ucfirst( $key ) . ': ' . $value .'</li>';
        }
        echo '</ul>';
        $mysqli->close();
        //////////////////////////////////////////////////////////////////////
    } else {
        echo '<b>PNR nicht in Datenbank enthalten</b>';
    }
}

?>