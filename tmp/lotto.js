/*
 * Funktion welche 6 Lottozahlen generiert und diese in einer Liste anzeigt
 * @TODO: Zusatzzahl, diese ist bisher nicht implementiert (wenn notwendig)
 */
function generateLottery() {
  var lottery = new Array();
  /*
   * die Schleife wird solange durchlaufen, 
   * bis 6 Zahlen im Array gespeichert sind
   */
  while (lottery.length < 6) {
    /*
     * Erzeugen einer Zufallszahl mit Math.random()
     * Zufallszahl erzeugen: Math.random() * (MAX - MIN) + MIN --> erzeugt eine Zahl zwischen MIN und MAX
     * floor() --> Abrunden auf nächste Ganzzahl. Mit Math.round() keine gleichmäßige Verteilung
     * Gute Erklärung siehe: https://wiki.selfhtml.org/wiki/JavaScript/Tutorials/Zufallszahlen
     */
    var number = Math.floor(Math.random() * (45 - 1)) + 1;
    /*
     * Wenn die Zahl noch nicht im Array ist (!lottery.includes(number)) wird diese mit push()
     * in das Array der Lottozahlen gespeichert
     */
    if (!lottery.includes(number)) {
      lottery.push(number);
    }
  }
  
  /*
   * Ab hier nur noch Anzeigen der Lottozahlen und generieren der Liste
   * Die Zahlen sind bereits fix fertig im Array
   */
  
  // Löschen der Liste, damit bei jedem Button-Klick eine neue Liste begonnen wird
  document.getElementById('lottery').innerHTML = '';
  /*
   * Sortieren der Lottozahlen (aufsteigend)
   * Array.sort() ist eine Standardfunktion eines Arrays in JS. Wird die sort()-Funktion
   * nicht überladen (also aufruf nur lotter.sort()), dann wird das Array alphabetisch
   * sortiert (also z.B. 1, 13, 3, etc.).
   * daher eine anonyme Funktion wie unten dargestellt, damit es nach Zahlen aufsteigend
   * sortiert wird
   */
  lottery.sort(function(s1, s2) {
    return s1 - s2;
  });
  /*
   * Lottozahlen mit forEach durchgehen und für jedes ein Listenelement (LI) in 
   * der Liste (UL - id: lottery) erzeugen
   */
  lottery.forEach(function(e) {
    document.getElementById('lottery').insertAdjacentHTML('beforeend', '<li>' + e + '</li>');
  });
}