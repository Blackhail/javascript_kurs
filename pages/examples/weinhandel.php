<!DOCTYPE html>
<html lang="de">

<head>
  <meta charset="UTF-8">
  <title>AJAX Weinhandel</title>
  <link rel="stylesheet" href="../lib/css/basis.css">
  <link rel="stylesheet" href="../lib/css/form.css">
  <script src="../lib/js/copycode.js"></script>
  <script src="../lib/js/repositories/helpers.js"></script>
  <script src="../lib/js/scripts/weinhandel.js"></script>
  <style>
  #info img {
    width: 10%;
  }
  </style>
</head>

<body>
  <input class="button" type="button" value="HOME" onclick="window.location = '../../index.html'">
  <!-- Diesen div können Sie später gerne entfernen. Er ist nur eine Information das Sie die Ausgaben in der Browerkonsole finden. JavaScript inline (im HTML-Dokument) sollte vermieden werden. -->
  <div class="postit fix effect" onmouseover="this.style.display = 'none';"
    onmouseout="setTimeout(()=>{ this.style.display = '';},5000);">Bitte mit F12 die Browserkonsole öffnen
  </div>
  <div class="row">
    <div class="column">
      <!-- ########################  Linke Spalte  ######################## -->
      <h1>AJAX Weinhandel</h1>
      <ul>
        <li>Erzeugen Sie in der HTML-Datei im div#overview eine Liste mit den Weinsorten (statisch im HTML). Siehe
          Muster</li>
        <li>Binden Sie an den Hyperlinks aus der Liste die Sie vorher erzeugt haben einen Klickhandler. Wenn ein Link
          geklickt wird soll die JSON-Datei wein.json angefordert werden und die Information zum gewünschten Produkt im
          div#info ausgegeben werden.
          Ausgabe: h2-Überschrift mit dem Namen des Produkts, Bild des Weines gefolgt von einer ungeordneten Liste mit
          den Daten.</li>
        <li>Erweitern Sie die Aufgabe dahingehend das Sie die Liste in weinhandel.html aus der JSON-Datei dynamisch
          erzeugen. Beim laden der Seite mit JS oder alternativ gerne über PHP, dazu legen Sie exercise28.php an.</li>
      </ul>
      <h3>HTML div#overview</h3>
      <pre>
<code>
&lt;ul&gt;
  &lt;li&gt;&lt;a id="art-51359" href="#"&gt;TRADIOMANO GOVERNO ALL'USO TOSCANO&lt;/a&gt;&lt;/li&gt;
  &lt;li&gt;&lt;a id="art-223285" href="#"&gt;LE SELCI CHIANTI CLASSICO RISERVA&lt;/a&gt;&lt;/li&gt;
  &lt;li&gt;&lt;a id="art-269928" href="#"&gt;ZOLLA PRIMITIVO-MERLOT&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</code>
</pre>
      <h3>exercise28.json</h3>
      <pre><code>
{
  "51359": {
    "Name": "TRADIOMANO GOVERNO ALL'USO TOSCANO",
    "Jahrgang": "2015",
    "Weinstil": "samtig & üppig",
    "Anbauregion": "Toskana",
    "Herkunftsklassifikation": "IGP",
    "Rebsorte": ["10% Cabernet Sauvignon", "20% Merlot", "70% Sangiovese"],
    "Trinktemperatur": "18° C",
    "Alkoholgehalt": "13.5 % Vol.",
    "bild": "tradiomano-governo-all-uso-toscano.png"
  },
  "223285": {
    "Name": "LE SELCI CHIANTI CLASSICO RISERVA",
    "Jahrgang": "2008",
    "Weinstil": "kräftig & würzig",
    "Anbauregion": "Chianti",
    "Herkunftsklassifikation": "DOCG",
    "Rebsorte": ["100% Sangiovese"],
    "Trinktemperatur": "18° C",
    "Alkoholgehalt": "13.5 % Vol.",
    "bild": "le-selci-chianti-classico-riserva.png"
  },
  "269928": {
    "Name": "ZOLLA PRIMITIVO-MERLOT",
    "Jahrgang": "2016",
    "Weinstil": "fruchtig & weich",
    "Anbauregion": "Apulien",
    "Herkunftsklassifikation": "Puglia IGP",
    "Rebsorte": ["50% Merlot", "50% Primitivo"],
    "Trinktemperatur": "18° C",
    "Alkoholgehalt": "13.5 % Vol.",
    "bild": "zolla-primitivo-merlot.png"
  },
  "111122": {
    "Name": "MERLOT",
    "Jahrgang": "2016",
    "Weinstil": "fruchtig & weich",
    "Anbauregion": "Apulien",
    "Herkunftsklassifikation": "Puglia IGP",
    "Rebsorte": ["50% Merlot", "50% Primitivo"],
    "Trinktemperatur": "18° C",
    "Alkoholgehalt": "13.5 % Vol.",
    "bild": "zolla-primitivo-merlot.png"
  }
}
</code></pre>
      <!-- ########################  Linke Spalte  ######################## -->
    </div>
    <div class="column">
      <!-- ########################  Rechte Spalte  ######################## -->
      <h2>Weinhandel</h2>
      <div id="overview">
        <?php
          $filename = '../lib/data/weinhandel.json';
          if(file_exists($filename)) {
              $file = @file_get_contents($filename); 
              if($file) {
                  $data = @json_decode($file);   
                  if($data) {
                      $keys = [];
                      echo '<ul>';
                      foreach($data as $key => $obj) {
                          array_push($keys,$key);
                          echo '<li><a id="art-'.$key.'" href="'.$_SERVER['PHP_SELF'].'?artikelnummer='.$key.'">'.$obj->Name.'</a></li>';
                      }
                      echo '</ul>';
                  }else {
                      echo 'Daten konnten nicht verarbeitet werden!';
                  }
              } else {
                  echo 'Inhalt der Datei kann nicht geladen werden. Datei wurde nicht geöffnet!';
              }
          } else {
              echo 'Datei wurde nicht gefunden!';
          }
      ?>

        <div id="info">
          <?php
            if(isset($_GET['artikelnummer'])) {
              $key = htmlspecialchars($_GET['artikelnummer']);
              if($data && in_array($key, $keys)) {
                  $wein = $data->$key; // In JS data.key
                  echo '<h2>' . $wein->Name . '</h2>';
                  echo '<img src="../lib/img/wein/' . $wein->bild . '" alt="' . $wein->bild . '" title="' . $wein->bild . '">';
                  echo '<ul>';
                  echo '<li>Jahrgang: ' . $wein->Jahrgang. '</li>';
                  echo '<li>Weinstil: ' . $wein->Weinstil. '</li>';
                  echo '<li>Anbauregion: ' . $wein->Anbauregion. '</li>';
                  echo '<li>Herkunftsklassifikation: ' . $wein->Herkunftsklassifikation. '</li>';
                  echo '<li>Rebsorte: ' . implode(',',$wein->Rebsorte ). '</li>';
                  echo '<li>Trinktemperatur: ' . $wein->Trinktemperatur. '</li>';
                  echo '<li>Alkoholgehalt: ' . $wein->Alkoholgehalt. '</li>';
                  echo '</ul>';
              } else {
                  echo 'Daten konnten nicht verarbeitet werden!';
              }
          }
          ?>
        </div>

      </div>
      <!-- ########################  Rechte Spalte  ######################## -->
    </div>
  </div>


</body>

</html>