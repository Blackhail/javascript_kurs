; (function (window, document, $) {
    'use strict';

    function vinothek() {
        var responseData = null;

        // function createInfobox (article) {
        //
        // }

        function handleData(res, name) {
            $('#infobox').remove();
            var linkText = name;
            var wines = res;
            var len = Object.keys(wines).length; // Anzahl Weinsorten (objects) in json
            var index = Object.keys(wines); // array

            var infobox = '<div id="infobox"><h2></h2><ul></ul></div>';
            $('#overview').after(infobox);
            for (var i = 0; i < len; i++) {
                var wine = wines[index[i]];

                if (linkText == wine.Name) {  // wie komme ich an den Scheißnamen ran?????? Yeah! Geschafft!!
                    // createInfobox(article);
                    for (var key in wine) {
                        var li = '<li>' + key + ': ' + wine[key] + '</li>';
                        $('#infobox').find('ul').find('li').eq(0).css('font-weight', 'bold');
                        $('#infobox').find('ul').append(li);
                    }
                }
            }
        }

        function handleClick(e) {
            e.preventDefault();
            var vino = $(this)[0].innerText; // warum hier [0]? ...gibt doch nur eins...

            $.ajax({
                url: '../../../lib/data/weinhandel.json',
                success: function (data) {
                    responseData = data;
                    handleData(data, vino);
                }
            });
        }

        $('#overview').find('ul').on('click', 'a', handleClick)
    }

    $(function () {
        // $('#overview').append('<ul><li><a id="art-51359" href="#">TRADIOMANO GOVERNO ALLUSO TOSCANO</a></li><li><a id="art-223285" href="#">LE SELCI CHIANTI CLASSICO RISERVA</a></li><li><a id="art-269928" href="#">ZOLLA PRIMITIVO-MERLOT</a></li></ul>');
        vinothek();
    });

})(window, document, jQuery);