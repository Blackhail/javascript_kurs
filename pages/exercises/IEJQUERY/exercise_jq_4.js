;
(function (window, document, $) {
  'use strict';
  var $pointer;
  /* e (Eventobjekt) wird innerhalb der FN nicht verwendet uns muss dann nicht angegeben werden. this zeigt auf das Element das die FN gefeuert/ausgelöst hat. Standardverhalten wird durch return false unterbunden. */
  function handleClick( /* e */ ) {
    /* $box enthält den div.entry dessen a{UP|DOWN|EDIT} angeklickt wurde.
    div.entry ist ein Elternelement (parent) der A-Elemente */
    var $box = $(this).parent();
    /* Hier soll das Ziel vor oder hinter das verschoben wird im switch gespeichert werden. */
    var $target;
    switch (this.textContent) {
      case 'UP':
        /* Wenn der div.entry nach oben verschoben werden soll ist unser $target der vorangehende div.entry (prev) */
        $target = $box.prev();
        /* div.entry der verschoben werden soll vor (before) dem $target einfügen */
        $target.before($box);
        break;
      case 'DOWN':
        /* Wenn der div.entry nach oben verschoben werden soll ist unser $target der folgende div.entry (next) */
        $target = $box.next();
        /* div.entry der verschoben werden soll nach (after) dem $target einfügen */
        $target.after($box);
        break;
      case 'EDIT':
        /* $pointer speichert das angewählte Element das wir editieren.
        Die "globale Variable" ist in der FN >endEdit< mit der wir zurückschreiben verfügbar. */
        $pointer = $box;
        var value = $box.find('p').contents().eq(2).text();
        /* Alle Whitespaces (Tabs, Zeilenumbrüche, Leerzeichen(mehr als 1) gegen nur 1 Leerzeichen austauschen) */
        value = removeWhitespaces(value);
        /* Hier nicht zwingend erforderlich aber beim zurückschreiben Pflicht! */
        value = stripTags(value);
        /* focus setzt den Cursor in das textarea-Feld und scrollt ggf. zum Feld. */
        $('#neu').val(value).focus();
        /* button{Beenden} einblenden */
        $('#endEdit').show();
        /* button{Neuer Post} ausblenden */
        $('#endNeu').hide();
        break;
    }
    /* e.preventDefault() und e.stopPropagation() */
    return false;
  }

  function newEntry( /* e */ ) {
    var value = $('#neu').val().trim();
    if (value !== '') {
      var $entry = $('.entry').eq(0).clone();
      var boxNo = $('.entry').length + 1;
      /* Inhalt von P-Element  erst löschen dann neu schreiben */
      $entry
        .attr('id', 'box' + boxNo)
        .find('p')
        .empty()
        .append('Absatz ' + boxNo + '<br>' + value);

      /* An einem Textknoten aus contents kann nicht mit text der textContent überschrieben werden. Die Methode replaceWith tauscht den String im Textknoten aus. */
      // $entry.find('p').contents().eq(0).replaceWith('Absatz ' + boxNo);
      // $entry.find('p').contents().eq(2).replaceWith(value);
      // $entry.attr('id', 'box' + boxNo);

      $('#main').append($entry);
      $('#neu').val('');
    }
  }

  function endEdit( /* e */ ) {
    var value = $('#neu').val().trim();
    /* Alle Whitespaces (Tabs, Zeilenumbrüche, Leerzeichen(mehr als 1) gegen nur 1 Leerzeichen austauschen) */
    value = removeWhitespaces(value);
    /* Hier nicht zwingend erforderlich aber beim zurückschreiben Pflicht! */
    value = stripTags(value);
    if (value !== '') {
      /* $pointer enthält den div.entry der beim klicken auf EDIT gespeichert wurde. */
      $pointer.find('p').contents().eq(2).replaceWith(value);
      /* button{Beenden} ausblenden */
      $('#endEdit').hide();
      /* button{Neuer Post} einblenden */
      $('#endNeu').show();
      $('#neu').val('');
    }
  }

  function init() {
    /* button{Beenden} ausblenden wenn das DOM geladen wurde*/
    $('#endEdit').hide();
    /* Eventlistener binden */
    $('#main').on('click', 'a', handleClick);
    $('#endNeu').on('click', newEntry);
    $('#endEdit').on('click', endEdit);
    /* a{EDIT} in allen div.entry einfügen. */
    $('.entry')
      .append('<a class="link-button" href="#">EDIT</a>\n')
      /* Jedem div.entry eine zufällige backgroundColor geben.
      Wird einer der methoden FN-Referenz übergeben, wird diese für jedes Element der Sammlung aufgerufen. */
      .css({
        backgroundColor: randomRGB
      });
  }

  /* Factory-FN mit Callback-FN. FN wird aufgerufen wenn das DOM der Seite ready ist. */
  $(init);

})(window, document, jQuery);