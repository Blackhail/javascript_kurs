;
(function (window, document, $) {
  'use strict';



  var settings = {
    // debug: true,
    errorClass: 'error_required',
    normalizer: function (value) {
      return $.trim(value);
    },
    errorPlacement: function ($errorElement, $element) {
      if ($element.prop('type') === 'radio' || $element.prop('type') === 'checkbox') {
        $element.parent().append($errorElement);
      } else {
        $element.after($errorElement);
      }
    },
    rules: {
      email: {
        // required: true,
        email: true
      },
      /* Eigenschaftsnamen mit Sonderzeichen, Leerzeichen oder wie z.B. dem Minus, das in JS ein Operator ist, können in einfache oder doppelte Hochticks notiert werden. */
      'email-confirm': {
        // required: true,
        equalTo: '#email'
      }
    }
  }

  $(function () {
    if (!hasFormValidation()) {
      $('form').eq(0).validate(settings);
    }

    /* Wird das if-Statement einkommentiert haben wir eine HTML5 Valdidierung in allen Browsern in denen diese implentiert ist. Alle älteren Browser die durch die Bibliothek unterstützt werden haben eine jQuery validate Validierung.

    Current Active Support 3.5.1 ( 01.10.2020)
    Chrome: (Current - 1) and Current
    Edge: (Current - 1) and Current
    Firefox: (Current - 1) and Current, ESR
    Internet Explorer: 9 +
    Safari: (Current - 1) and Current
    Opera: Current     */
  });
})(window, document, jQuery);