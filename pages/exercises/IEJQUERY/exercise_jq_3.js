;
(function (window, document, $) {
  'use strict';

  function task1() {

    function handleClick() {
      var $box = $(this).parent();
      var $target;

      switch (this.textContent) {
        case 'UP':
          /* $box wird vor dem vorherigen Element eingefügt */
          $target = $box.prev();
          // $box.insertBefore($target);
          /* WENN: Das vorherige Element die Klasse .entry hat. Damit verhindern wir das Boxen vor die Überschrift geschoben werden. Besser wäre es die h3 im HTML-Dokument außerhalb von div#a3 zu schreiben. 
          
          Alternativ kann man auch die Position des zu verschiebenden Elements innerhalb der Sammlung .entry bestimmen. Ist die Position 0 darf nicht mehr verschoben werden.
          $('.entry').index($box)
          https: //api.jquery.com/index/#index
          */
          if ($target.hasClass('entry')) {
            $target.before($box);
          }
          break;
        case 'DOWN':
          /* $box wird nach dem folgenden Element eingefügt */
          $target = $box.next();
          // $box.insertAfter($target);
          $target.after($box);
          break;
      }


      return false;
    }
    /* Klickhandler direkt an A-Elementen binden. Die Elemente werden nicht in eine andere Liste verschoben und behalten den Klickhandler nachdem verschieben. */
    $('#a3').find('a').on('click', handleClick);
  }

  function task2() {
    function handleSubmit(e) {
      var value = $('#neu2').val().trim();
      if (value !== '') {
        /* Element über append als DOM-Knoten erzeugen */
        // $('#a4')
        //   .append('<div class="entry"><p class="content">' + value + '</p><a class="link-button" href="#">UP</a> <a class="link-button" href="#">DOWN</a></div>');

        /* Clonen des DOM-Knotens. Werden die Handler wie in Aufgabe 1 direkt gebunden kann hier mit clone(true) der Handler ebenfalls kopiert werden. Hier nicht nötig, da die Handler delegiert werden. */
        var $clone = $('#a4').find('.entry').eq(0).clone();
        $clone.find('.content').text(value);
        $('#a4').append($clone);
        $('#neu2').val('');
      }
      return false;
    }

    function handleClick(e) {
      var $box = $(this).parent();
      var $target;

      switch (this.textContent) {
        case 'UP':
          $target = $box.prev();
          if ($target.hasClass('entry')) {
            $target.before($box);
          }
          break;
        case 'DOWN':
          $target = $box.next();
          $target.after($box);
          break;
      }
      return false;
    }



    $('#a4').on('click', 'a', handleClick);
    $('#a4').next('form').on('submit', handleSubmit);
    // $('form').eq(0).on('submit', handleSubmit);
  }

  function task3() {

    var $pointer = null;

    function handleClick() {
      var $box = $(this).parent();
      var $target;

      switch (this.textContent) {
        case 'UP':
          $target = $box.prev();
          if ($target.hasClass('entry')) {
            $target.before($box);
          }
          break;
        case 'DOWN':
          $target = $box.next();
          $target.after($box);
          break;
        case 'EDIT':
          $pointer = $box;
          $('#edit2').val($box.find('.content').text());
          break;
      }
      return false;
    }

    function handleSubmit() {
      var value = $('#edit2').val().trim();
      if ($pointer !== null) {
        $pointer.find('.content').text(value);
        $pointer = null;
        $('#edit2').val('');
      }
      return false;
    }

    $('#a5').find('.entry').append('<a class="link-button" href="#">EDIT</a>');
    $('#a5').find('a').on('click', handleClick);
    $('#a5').next('form').on('submit', handleSubmit);
  }

  $(function () {
    task1();
    task2();
    task3();
  });
})(window, document, jQuery);