;
(function (window, document, $) {
  'use strict';

  function lernskript() {
    /* 
    //////////////////////////////////////////////////////////////////////////
    Lernskript Factory Funktion, jQuery - Objekt, Selektion und Accessoren
    //////////////////////////////////////////////////////////////////////////
    */
    console.log('_________________________ 1 __________________________');
    /* 1 */
    /* HTMLCollection ist eine native Sammlung */
    var links = document.getElementsByTagName('a'); //=> HTMLCollection 
    console.log('var links = document.getElementsByTagName(\'a\');');
    console.log('links //=> ', links);
    console.log('links[0] //=> ', links[0]);
    console.log('$(links) //=> ', $(links));
    /*  */
    console.log('Array.from(links) //=> ', Array.from(links));

    /* jQ-Collection/Sammlung ist ein JS-Objekt, mit numerischen Indizes. Die nativen DOM-Knoten werden im Objekt in numerischen Indizes gespeichert.  */
    var $links = $('a'); //=> jQ-Collection/Sammlung
    console.log('var $links = $(\'a\');');
    console.log('$links //=> ', $links);
    console.log('$links[0] //=> ', $links[0]); //=> Natives JS-Objekt Typ Element
    console.log('$links.eq(0) //=> ', $links.eq(0)); //=> jQ-Collection/Sammlung

    /* Für HTMLCollection und jQ-Collection gilt folgendes:
    Es handelt sich dabei auch um eine Sammlung wenn nur ein Element enthalten ist.  
    
    jQ - Collection
    {…}​
    0: <a id = "link-01" href = "https://www.google.de"> ​
    1: <a id = "link-02" href = "#"> ​
    2: <a id = "link-03" href = "#"> ​
    3: <a id = "link-04" href = "#"> ​
    length: 4​
    prevObject: Object {
      0: HTMLDocument http: //localhost/kurs/javascript/pages/exercises/IEJQUERY/exercise_jq_01.html, length: 1 }
    <prototype>: Object { jquery: "3.5.1", constructor: S(e, t), length: 0, … }
    */

    console.log('_________________________ 2 __________________________');
    /* 2 */
    /* Transparente jQuery-Methoden
    Diese Methoden ändern die DOM - Knoten innerhalb der Sammlung / Selektion aber die Sammlung / Selektion bleibt unverändert.Die Methode css verändert die DOM - Knoten, den Inhalt der Sammlung / Selektion, aber nicht die Sammlung / Selektion selbst.*/
    var $hauptsammlung = $('p');
    console.log(
      $hauptsammlung
      .css({
        color: 'pink'
      }) // => Transparente Methoden geben die ursprüngliche Sammlung zurück
    );

    /* Destruktive jQuery-Methoden
    Diese Methoden verändern die Sammlung / Selektion und gibt eine neue Sammlung / Selektion zurück.*/
    console.log(
      $hauptsammlung
      .eq(0) //=> Destruktive Methoden verändern die ursprüngliche Sammlung / Selektion und geben eine neue Sammlung / Selektion zurück
    );

    /* Terminierende jQuery-Methoden 
    Geben keine Sammlung / Selektion zurück. Die Methode html gibt eine Zeichenkette zurück. */
    console.log(
      $hauptsammlung.html() //=> Terminierende Methoden geben keine Sammlung / Selektion zurück
    );


    /* Die Art der Methode spielt beim Chaining/Verketten eine Rolle */
    console.log(
      $hauptsammlung
      .eq(0)
      .html()
      //=> Zeichenkette, kein weiterer jQuery-Methodenaufruf möglich. Hier kann mit nativen JS Stringmethoden gearbeitet werden.
      .toUpperCase()
    );

    console.log('_________________________ 3 __________________________');
    /* 3 */
    /* Der DOM-Knoten wird mit nativen JS eingelesen und an die Factory-FN übergeben.  */
    var p = document.getElementsByTagName('p')[0];
    var $p0 = $(p);
    var $body = $(document.body);
    console.log($body);

    var $newEl = $(createEl('p', 'Hello World'));
    console.log($newEl);




    console.log('_________________________ 4 __________________________');
    /* 4 */
    /* Alle CSS-Selektoren und zusätzlich die Filter/Pseudoselektoren von jQuery 
    https: //overapi.com/jquery im BEREICH Selectors zu sehen. */


    console.log('_________________________ 5 __________________________');
    /* 5 */
    /* Filter/Pseudoselektoren von jQuery die als zusatz im Selektorstring stehen können. */
    console.log('$(\'a:eq(3)\') //=> ', $('a:eq(3)'));
    console.log('$(\'a:even()\') //=> ', $('a:even()'));

    console.log('_________________________ 6 __________________________');
    /* 6 */
    /* Je genauer/spezifischer die Selektion desto besser die Performance.
    TIPP: Der beste Einstiegspunkt in das DOM ist immer eine ID. */
    console.log('$(\'a\').eq(0) //=> ', $('a').eq(0));
    console.log('$(\'a\').even() //=> ', $('a').even());

    console.log('_________________________ 7 __________________________');
    /* 7 */
    /* Die Factroy FN erzeugt einen DOM-Knoten und verpackt diesen in ein jQuery-Objekt. */
    var $newElement = $('<p><b>Das ist ein HTMLString</b></p>');

    console.log(
      'var $newElement = $(\'<p><b>Das ist ein HTMLString</b></p>\');\n',
      $newElement, '\n',
      '$newElement[0]',
      $newElement[0]
    );

    console.log('_________________________ 8 __________________________');
    /* 8 */
    /*  */
    var $newElement = $('<h2/>', {
      text: 'Das ist eine Überschrift',
      id: 'headline',
      title: 'Der Titel des Elements',
      click: (e) => alert(e)
    });

    console.log('var $newElement = $(\'<h2/>\',{\n\ttext: \'Das ist eine Überschrift\',\n\tid: \'headline\',\n\ttitle: \'Der Titel des Elements\',\n\tclick: (e) => alert(e);\n})', $newElement);

    $('.column').eq(1).append($newElement);

    console.log($('#headline'));
    console.log($('#headline').attr('id'));
    console.log($('#headline')[0].id);

    console.log($('#headline').attr('title'));
    $('#headline').attr('title', 'Neuer Titel')
    console.log($('#headline').attr('title'));


    console.log('_________________________ 9 __________________________');
    /* 9 */
    /* https://api.jquery.com/jquery/#jQuery-callback
    Die Funktion wird an den DOM-Ready Eventlistener gebunden und aufgerufen sobald das DOM geladen wurde. */

    console.log('_________________________ 10 __________________________');
    /* 10 */
    /* Die Sammlung hat numerische Indizes in denen die DOM-Knoten/JS-Objekte-Typ-Element liegen. */
    console.log('var $links = $(\'a\');');
    console.log('$links //=> ', $links);
    console.log('$links[0] //=> ', $links[0]);
    console.log('$links[1] //=> ', $links[1]);
    console.log('$links[2] //=> ', $links[2]);
    console.log('$links[3] //=> ', $links[3]);

    /* Array mit  DOM-Knoten/JS-Objekte-Typ-Element, keine HTMLCollection */
    console.log('$links.get() //=> ', $links.get());
    console.log('$links.toArray() //=> ', $links.toArray());

    /*  DOM-Knoten/JS-Objekte-Typ-Element  */
    console.log('$links.get(0) //=> ', $links.get(0));

    console.log('_________________________ 11 __________________________');
    /* 11 */
    /*  Kapitel 4.4 ab Seite 110  
    get, toArray, index, each, length */
    console.log('%c Techniken von langsam zu schnell. Ideal ist die for-Schleife', 'background: #000; color: #bada55');
    console.log('_____ .each() _____');
    $('a').each(function (index, element) {
      console.log(index, element, this);
    });

    console.log('_____ toArray und forEach _____');
    $('a').toArray().forEach(function (index, element) {
      console.log(index, element, this);
    });

    console.log('_____ for _____');
    var $a = $('a');
    for (var i = 0; i < $a.length; i++) {
      console.log(i, $a[i], this);
    }

    console.log('_________________________ 12 __________________________');
    /* 12 */
    /* Siehe Frage 11 */

    console.log('_________________________ 13 __________________________');
    /* 13 */
    /* NEIN, jede Methode hat automatisch eine Iteration implementiert.
    Methoden im schreibenden Zugriff, auf die Inhalte einer Sammlung, iterieren automatisch über alle Elemente der Sammlung.*/
    $('p').css({
      color: 'orange'
    });

    var $p = $('p');
    for (var i = 0; i < $p.length; i++) {
      $p
        .eq(i)
        .css({
          backgroundColor: 'blue'
        });

      /* Natives JS als Alternative: */
      // $p[i].style.backgroundColor = 'blue';
      // $p.get(i).style.backgroundColor = 'blue';
    }

    console.log('_________________________ 14 __________________________');
    /* 14 */
    /* get() oder .toArray()  */

    console.log('_________________________ 15 __________________________');
    /* 15 */
    /* Gibt true oder false zurück, je nachdem, ob mindestens ein Element der aktuellen Collection dem Selektor sel entspricht. */

    console.log($('#einstieg ul a').is('[href="https://www.google.de"]')); //=> true
    /* Der body hat nicht die Klasse .row. Und in der erzeugten Sammlung/Selektion befindet sich kein Element mit der Klasse .row */
    console.log($('body').is('.row')); //=> false
    /* Der body hat die Klasse .test. In der erzeugten Sammlung/Selektion befindet sich dadurch ein Element mit der Klasse .test */
    console.log($('body').is('.test')); //=> true
    /* Durch .children werden alle direkten Kinder des body-Elements als neue Sammlung/Selektion erstellt, div.row ist ein Kind innerhalb dieser Sammlung/Selektion. */
    console.log($('body').children().is('.row')); //=> true
    console.log($('body').hasClass('row')); //=> false
    /* Alle Nachfahren, bis zu untersten Ebene, des Bodys über Asterisk (*) */
    console.log($('body').find('*'));
    console.log($('body').find('*').is('.row')); //=> true


  }


  function markLink(clickedA) {
    $('#einstieg').find('.marker').removeClass('marker');
    $(clickedA).addClass('marker');
  }

  function generateHTML(clickedA) {
    /* Mit jQuery auslesen */
    // var id = $(clickedA).attr('id');
    // var nr = $(clickedA).attr('id').split('-')[1];
    // var text = $(clickedA).text();
    // var classN = $(clickedA).attr('class');

    /* Natives JS */
    var id = clickedA.id;
    var nr = clickedA.id.split('-')[1];
    var text = clickedA.textContent;
    var classN = clickedA.className;

    var html = 'Die id des geklickten links <b>' + id + '</b>.<br>Die Zahl<b> ' + nr + '</b><br>Der Textknoten:<b> ' + text + '</b><br> Er wurde durch JavaScript mit der Klasse<b> ' + classN + '</b> versehen.';

    $('#ausgabe')
      .empty()
      .append(html);
  }

  function task1(e) {
    e.preventDefault();
    markLink(this);
    generateHTML(this);
    return false; // e.preventDefault() und e.stopPropagation();
  }

  function bindHandlerTask1() {
    $('#einstieg')
      .find('ul')
      .eq(0)
      .find('a')
      .on('click', task1);
  }

  // function task2(e) {
  //   e.preventDefault();
  //   var $fehler = $('#fehler').empty();
  //   var $inputs = $(e.target).find('input:text');
  //   var errors = [];
  //   var values = [];
  //   var $input;
  //   var value;
  //   for (var i = 0; i < $inputs.length; i++) {
  //     $input = $inputs.eq(i);
  //     value = $.trim($input.val());
  //     if (value === '') {
  //       errors.push(ucfirst($input.attr('name')) + ' darf nicht leer sein.');
  //     } else {
  //       values.push(value);
  //     }
  //   }
  //   if (errors.length /* > 0 */ ) {
  //     $fehler.append(errors.join('<br>'));
  //   } else {
  //     $('#liste').append('<li>' + values.join(' ') + '</li>');
  //     $('input:text').val('');
  //   }
  //   return false;
  // }

  function task2(e) {
    e.preventDefault();
    var $fehler = $('#fehler').empty();
    var $inputs = $(e.target).find('input:text');
    var errors = [];
    var values = [];
    var input;
    var value;
    for (var i = 0; i < $inputs.length; i++) {
      input = $inputs[i];
      value = input.value.trim();
      if (value === '') {
        errors.push(ucfirst(input.name) + ' darf nicht leer sein.');
      } else {
        values.push(value);
      }
    }
    if (errors.length /* > 0 */ ) {
      $fehler.append(errors.join('<br>'));
    } else {
      $('#liste').append('<li>' + values.join(' ') + '</li>');
      $('input:text').val('');
    }
    return false;
  }

  function bindHandlerTask2() {
    $('#dom')
      .find('form')
      .eq(0)
      .on('submit', task2);
  }

  function changeImage() {
    var images = ['bild1.jpg', 'bild2.jpg', 'bild3.jpg'];
    var img = images[rand(0, images.length - 1)];
    $('#bilder')
      .find('img')
      .eq(0).attr('src', '../../../lib/img/nicht_lustig/' + img);
  }

  function bindHandlerTask3() {
    $('#bilder')
      .find('img')
      .eq(0)
      .on('mouseover', changeImage);
  }

  $(function () {
    // lernskript();
    $('#ausgabe').empty();
    bindHandlerTask1();
    bindHandlerTask2();
    bindHandlerTask3();
    changeImage();
  });
})(window, document, jQuery);


/* 
 console.log('___________________________________________________');
 console.log('links //=> ', links);
 console.log('links.item(0) //=> ', links.item(0));
 console.log('links.namedItem(\'link-01\') //=> ', links.namedItem('link-01'));
 console.log('links[0] //=> ', links[0]);
 console.log('Object.values(links) //=> ', Object.values(links));
//  Das folgende Beispiel unterscheidet zwischen direkten Kind-Attributen eines Objekts und den Attributen die durch die prototype - Verarbeitung entstehen. length ist vererbt und keine direkte Kindeigenschaft der HTMLCollection, darum false.
console.log('links.hasOwnProperty(\'length\') //=> ', links.hasOwnProperty('length'));
*/