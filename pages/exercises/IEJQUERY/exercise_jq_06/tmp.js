////////////////////////////////// Stand 1 ////////////////////////////////////////////////////

;(function(window,document,$){
    'use strict';
  

    function blog () {
        var $pointer = {};

        // Formular /////////////////////////////////////////////

        function handleSubmit ( form, currentCard) {
            // e.preventDefault();
            console.log(currentCard);
            console.log(form);
            // var clicked = currentCard;
            // console.log(clicked);

            // paste changed content:
            // $(clicked).find('h2').eq(0).val(removeWhitespaces(stripTags($($('form')).find('input').eq(0).val())));
            // console.log( $(clicked).find('h2').contents().eq(0).text(removeWhitespaces(stripTags($($('form')).find('input').eq(0).val()))));
            // console.log( $(clicked).find('h2').contents().eq(0));

            // console.log($('form'));
            // $('.leftcolumn').find('.card').show();
            // $('#editEntry').hide();
           
            // $(clicked).show();
            return false;
        }

        function showForm (form, card) {
          
            // clone div.card, id hinzufügen, leeren, form anhängen, sichtbar machen:
            var $form = $('.leftcolumn').find('.card').eq(0).clone(true).attr('id' , 'editEntry').empty().append(form).show();

            // value aus clicked card übernehmen:
            $($form).find('input').eq(0).val(removeWhitespaces(stripTags($(card).find('h2')[0].innerText))); // Überschrift
            $($form).find('input').eq(1).val(removeWhitespaces(stripTags($(card).find('h5')[0].innerText))); // Description
            $($form).find('textarea').eq(0).val(removeWhitespaces(stripTags($(card).find('p')[0].innerText))); // Blindtext
            // neuen div.card inkl. form als erstes Element einfügen:
            $('.leftcolumn').prepend($form);
            var $clickedCard = card;

            form.on('submit' , handleSubmit);
            // $('#editEntry').find('form').on('submit' , handleSubmit($form, $clickedCard));
           
            console.log($clickedCard);
            

        }
       

        function handleClick (e) {
            e.preventDefault();
               
            switch (this.textContent) {

                case 'Neuer Eintrag': 
                console.log(this.textContent);
                break;

                // EDIT /////////////////////////////////////////////

                case 'Edit': 
                $pointer = ($(this).parent());
                $('.leftcolumn').find('.card').hide();

                $.ajax({
                    url: 'form.html',
                    dataType: 'html',
                    success: function (data) {
                        var responseData = data;
                        var $form = $(data).find('form');
                       
                        console.log($form);
                        showForm($form, $pointer);
                        // form.on('submit' , handleSubmit);
                        // $($form).on('submit' , handleSubmit($form, $pointer));

                    
                      }
                })
    
                break;

                // DELETE ////////////////////////////////////////////

                case 'Delete': 
                console.log(this.textContent);
                break;

                case 'UP': 
                console.log(this.textContent);
                break;

                case 'DOWN': 
                console.log(this.textContent);
                break;
            }

        }





        $('.card').on('click' , 'a' , handleClick);
        // $('.leftcolumn').find('form').eq(0);
        // console.log($('.leftcolumn').find('form'));

    }




    $(function(){
        blog();
    });
  })(window,document,jQuery);




////////////////////////////////// Stand 2 ////////////////////////////////////////////////////

  ; (function (window, document, $) {
    'use strict';


    function blog() {
        var $pointer = {};
        $('.leftcolumn').find('.card').eq(0).addClass('newEntry');

        // Formular /////////////////////////////////////////////

        function handleSubmit(form) {
            // e.preventDefault();

            var h2 = $('#editEntry').find('input').eq(0).val();
            var h5 = $('#editEntry').find('input').eq(1).val();
            var text = $('#editEntry').find('textarea').eq(0).val();

            $pointer.find('h2').eq(0).html(removeWhitespaces(stripTags(h2)));
            $pointer.find('h5').eq(0).html(removeWhitespaces(stripTags(h5)));
            $pointer.find('p').eq(0).html(removeWhitespaces(stripTags(text)));
            console.log($pointer);

            $('.leftcolumn').find('.card').show();
            $('#editEntry').hide();

            return false;
        }

        function showForm(form) {

            // clone div.card, id hinzufügen, leeren, form anhängen, sichtbar machen:
            var $form = $('.leftcolumn').find('.card').eq(0).clone(true).attr('id', 'editEntry').empty().append(form).show();

            // value aus clicked card übernehmen:
            $($form).find('input').eq(0).val(removeWhitespaces(stripTags($pointer.find('h2')[0].innerText))); // Überschrift
            $($form).find('input').eq(1).val(removeWhitespaces(stripTags($pointer.find('h5')[0].innerText))); // Description
            $($form).find('textarea').eq(0).val(removeWhitespaces(stripTags($pointer.find('p')[0].innerText))); // Blindtext
            // neuen div.card inkl. form als erstes Element einfügen:
            $('.leftcolumn').prepend($form);

            // form.on('submit' , handleSubmit);
            $('#editEntry').on('submit', 'form', handleSubmit);

        }
        // Ende Formular /////////////////////////////////////////


        function handleClick(e) {
            e.preventDefault();

            switch (this.textContent) {

                case 'Neuer Eintrag':
                    console.log(this.textContent);
                    break;

                // EDIT /////////////////////////////////////////////

                case 'Edit':
                    $pointer = ($(this).parent());
                    $('.leftcolumn').find('.card').hide();

                    $.ajax({
                        url: 'form.html',
                        dataType: 'html',
                        success: function (data) {
                            var responseData = data;
                            var $form = $(data).find('form');

                            console.log($form);
                            showForm($form);
                        }
                    })

                    break;

                // DELETE ////////////////////////////////////////////

                case 'Delete':
                    $(this).parent().remove();
                    break;

                // MOVE ////////////////////////////////////////////
               
                case 'UP':

                    var $target = $(this).parent().prev();
                    if ( ! $target.hasClass('newEntry')) {          
                        $target.before($(this).parent());
                    }
                    break;

                case 'DOWN':
                    var $target = $(this).parent().next();
                    $target.after($(this).parent());
                    break;
            }

        }





        $('.card').on('click', 'a', handleClick);

    }

    $(function () {
        blog();
    });

})(window, document, jQuery);