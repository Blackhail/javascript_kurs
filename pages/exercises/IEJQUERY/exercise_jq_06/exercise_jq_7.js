(function (window, document, $) {
  'use strict';
  var $form;
  var $pointer = null;

  function editEntry(e) {
    e.preventDefault();
    if (!$pointer) {
      $pointer = $(e.target).parent();
    }
    $form.find('form').attr('id', 'editEntry');
    /* Formularfeld-values mit Werten aus div.card befüllen  */
    $form.find('#heading').val($pointer.find('h2').text());
    $form.find('#description').val($pointer.find('h5').text());
    $form.find('#image').val('fakeimg.jpg');
    $form.find('#runningtext').val(
      $pointer
        .find('p')
        .text()
        .replace(/\s{2,}/g, '')
    );
    /* div.card ausblenden */
    $('.leftcolumn').find('.card').hide();
    /* Browser an den Seitenanfang scrollen */
    $(window).scrollTop(0);
    /* Formular anzeigen und Fokus auf das erste Formularelement setzen */
    $form.show().find('input').eq(0).focus();
  }

  function endEditEntry(e) {
    e.preventDefault();
    var $inputs = $(e.target).find('input:text, textarea');
    var $error = $('.error').removeClass('error');
    var len = $inputs.length;
    for (var i = 0; i < len; i++) {
      if ($inputs.eq(i).val().trim() === '') {
        $inputs.eq(i).addClass('error');
      }
    }

    if ($('.error').length === 0) {
      $pointer.find('h2').text($form.find('#heading').val());
      $pointer.find('h5').text($form.find('#description').val());
      $pointer.find('.fakeimg').text($form.find('#image').val());
      $pointer.find('p').text($form.find('#runningtext').val());
      $('.leftcolumn').find('.card').show();
      $form.hide();
      $form.find('input:text, textarea').val('');
      $(window).scrollTop($pointer.offset().top);
      $pointer = null;
    }
  }

  function deleteEntry(e) {
    /* Bestätigung war nicht gefordert ist aber sinnvoll */
    if (confirm('Wirklich löschen?')) $(e.target).parent().remove();
  }

  function moveEntry(e) {
    e.preventDefault();
    var $clickedParent = $(e.target).parent();
    var $stop = $('.card').find('a:contains("Neuer Eintrag")').parent();
    switch (e.target.innerText) {
      case 'UP':
        var $parentBefore = $(e.target).parent().prev();
        if ($parentBefore[0] !== $stop[0])
          $clickedParent.insertBefore($parentBefore);
        break;
      case 'DOWN':
        var $parentAfter = $(e.target).parent().next();
        if ($parentAfter[0] !== $stop[0])
          $clickedParent.insertAfter($parentAfter);
        break;
    }
    $(window).scrollTop($clickedParent.offset().top);
  }

  function newEntry(e) {
    $form.find('form').attr('id', 'newEntry');
    /* div.card ausblenden */
    $('.leftcolumn').find('.card').hide();
    /* Browser an den Seitenanfang scrollen */
    $(window).scrollTop(0);
    /* Formular anzeigen und Fokus auf das erste Formularelement setzen */
    $form.show().find('input').eq(0).focus();
  }

  function endNewEntry(e) {
    e.preventDefault();
    var $inputs = $(e.target).find('input:text, textarea');
    var $error = $('.error').removeClass('error');
    var len = $inputs.length;
    for (var i = 0; i < len; i++) {
      if ($inputs.eq(i).val().trim() === '') {
        $inputs.eq(i).addClass('error');
      }
    }

    if ($('.error').length === 0) {
      var $newEntry = $('<div class="card"></div>')
        .append('<h2>' + $form.find('#heading').val() + '</h2>')
        .append('<h5>' + $form.find('#description').val() + '</h5>')
        .append('<div class="fakeimg" style="height:200px;">Image</div>')
        .append('<p>' + $form.find('#runningtext').val() + '</p>')
        .append(
          '<a class="edit" href="#">Edit</a><a class="delete" href="#">Delete</a><a class="move" href="#">UP</a><a class="move" href="#">DOWN</a>'
        );
      $('.leftcolumn').append($newEntry).find('.card').show();
      $form.hide();
      $form.find('input:text, textarea').val('');
      $(window).scrollTop($newEntry.offset().top);
    }
  }

  function bindHandler() {
    var $leftcolumn = $('.leftcolumn');
    $leftcolumn.on('click', 'a:contains("Edit")', editEntry);
    $leftcolumn.on('submit', 'form#editEntry', endEditEntry);
    $leftcolumn.on('click', 'a:contains("Delete")', deleteEntry);
    $leftcolumn.on('click', 'a:contains("UP"),a:contains("DOWN")', moveEntry);
    $('a:contains("Neuer Eintrag")').on('click', newEntry);
    $leftcolumn.on('submit', 'form#newEntry', endNewEntry);
  }

  function setup() {
    ///////////////////////////////////////////////////////////////////////
    /* form-Element mit Factory-FN erzeugen */
    // var $form = $('<div class="card"><form action="/action_page.php"><label for="heading">Überschrift</label><input type="text" id="heading" name="heading" placeholder="Musterüberschrift.."><label for="description">Beschreibung</label><input type="text" id="description" name="lastname" placeholder="Beschreibungstext.."><label for="image">Bildname</label><input type="text" id="image" name="pic" placeholder="Beschreibungstext.."><label for="runningtext">Fließtext</label><textarea id="runningtext" name="runningtext">Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto, ratione?</textarea><input type="submit" id="submitBtn" value="Submit"></form></div>').appendTo('.leftcolumn')
    // .hide();
    ///////////////////////////////////////////////////////////////////////
    /* Formular über AJAX laden */
    $form = $('<div class="card"></div>')
      .appendTo('.leftcolumn')
      .load('form.html form')
      .hide();
    bindHandler();
  }

  $(setup);
})(window, document, jQuery);
