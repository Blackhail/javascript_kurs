; (function (window, document, $) {
    'use strict';

    function blog() {

        var $pointer = {};
        $('.leftcolumn').find('.card').eq(0).addClass('newEntry');

        // Datum & Zeit ///// für description-Feld h5 ///////////

        function getToday() {
            var today = new Date();
            var day = today.getDate();
            var month = today.getMonth() + 1;
            var year = today.getFullYear();
            if (day < 10) {
                day = '0' + day;
            }
            if (month < 10) {
                month = '0' + month;
            }
            today = day + '.' + month + '.' + year;
            return today;
        }

        // Formular /////////////////////////////////////////////

        function handleSubmit(form) {
            // e.preventDefault();

            var h2 = $('#editEntry').find('input').eq(0).val();
            var h5 = $('#editEntry').find('input').eq(1).val();
            var text = $('#editEntry').find('textarea').eq(0).val();

            if ($pointer.hasClass('newEntry') === true) {
                $pointer = $('.leftcolumn').find('.card').last().clone(true);
                $pointer.insertAfter($('.leftcolumn').find('.card').eq(1));
            }

            $pointer.find('h2').eq(0).html(removeWhitespaces(stripTags(h2)));
            $pointer.find('h5').eq(0).html(removeWhitespaces(stripTags(h5)) + ', ' + getToday()); // anders machen, sonst mehrfach nach bearbeiten!!
            $pointer.find('p').eq(0).html(removeWhitespaces(stripTags(text)));

            var date = Date(Date.now());
            console.log(date);

            $('.leftcolumn').find('.card').show();
            $('#editEntry').remove();

            // hier Schleife für rightcolumn!!

            return false;
        }

        function showForm(form) {

            // clone div.card, id hinzufügen, leeren, form anhängen, sichtbar machen:
            var $form = $('.leftcolumn').find('.card').eq(0).clone(true).removeClass('newEntry').attr('id', 'editEntry').empty().append(form).show();


            if ($pointer.hasClass('newEntry') === false) {

                // value aus clicked card übernehmen:
                $($form).find('input').eq(0).val(removeWhitespaces(stripTags($pointer.find('h2')[0].innerText))); // Überschrift
                $($form).find('input').eq(1).val(removeWhitespaces(stripTags($pointer.find('h5')[0].innerText))); // Description
                $($form).find('textarea').eq(0).val(removeWhitespaces(stripTags($pointer.find('p')[0].innerText))); // Blindtext
                // neuen div.card inkl. form als erstes Element einfügen:
                $('.leftcolumn').prepend($form);

            } else {
                // Formular leer aufrufen:
                var $form = $('.leftcolumn').find('.card').eq(0).clone(true).removeClass('newEntry').attr('id', 'editEntry').empty().append(form).show();
                $('.leftcolumn').prepend($form);
            }

            // form.on('submit' , handleSubmit);
            $('#editEntry').on('submit', 'form', handleSubmit);
        }
        // Ende Formular /////////////////////////////////////////

        function handleClick(e) {
            e.preventDefault();

            switch (this.textContent) {

                // EDIT & NEW ENTRY /////////////////////////////////////////////

                case 'Neuer Eintrag':
                case 'Edit':
                    $pointer = ($(this).parent());
                    $('.leftcolumn').find('.card').hide();

                    $.ajax({
                        url: 'form.html',
                        dataType: 'html',
                        success: function (data) {
                            var $form = $(data).find('form');
                            showForm($form);
                        }
                    })
                    break;

                // DELETE ////////////////////////////////////////////

                case 'Delete':
                    $(this).parent().remove();
                    break;

                // MOVE ////////////////////////////////////////////

                case 'UP':

                    var $target = $(this).parent().prev();
                    if (!$target.hasClass('newEntry')) {
                        $target.before($(this).parent());
                    }
                    break;

                case 'DOWN':
                    var $target = $(this).parent().next();
                    $target.after($(this).parent());
                    break;
            }
            console.log($pointer);

        }

        $('.card').on('click', 'a', handleClick);

    }

    $(function () {
        blog();
    });

})(window, document, jQuery);