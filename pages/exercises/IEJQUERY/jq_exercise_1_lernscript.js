;
(function (window, document, $) {
  'use strict';

  function lernskript() {
    /* 
    //////////////////////////////////////////////////////////////////////////
    Lernskript Factory Funktion, jQuery - Objekt, Selektion und Accessoren
    //////////////////////////////////////////////////////////////////////////
    */
    console.log('_________________________ 1 __________________________');
    /* 1 */
    /* HTMLCollection ist eine native Sammlung */
    var links = document.getElementsByTagName('a'); //=> HTMLCollection 
    console.log('var links = document.getElementsByTagName(\'a\');');
    console.log('links //=> ', links);
    console.log('links[0] //=> ', links[0]);
    console.log('$(links) //=> ', $(links));
    /*  */
    console.log('Array.from(links) //=> ', Array.from(links));

    /* jQ-Collection/Sammlung ist ein JS-Objekt, mit numerischen Indizes. Die nativen DOM-Knoten werden im Objekt in numerischen Indizes gespeichert.  */
    var $links = $('a'); //=> jQ-Collection/Sammlung
    console.log('var $links = $(\'a\');');
    console.log('$links //=> ', $links);
    console.log('$links[0] //=> ', $links[0]); //=> Natives JS-Objekt Typ Element
    console.log('$links.eq(0) //=> ', $links.eq(0)); //=> jQ-Collection/Sammlung

    /* Für HTMLCollection und jQ-Collection gilt folgendes:
    Es handelt sich dabei auch um eine Sammlung wenn nur ein Element enthalten ist.  
    
    jQ - Collection
    {…}​
    0: <a id = "link-01" href = "https://www.google.de"> ​
    1: <a id = "link-02" href = "#"> ​
    2: <a id = "link-03" href = "#"> ​
    3: <a id = "link-04" href = "#"> ​
    length: 4​
    prevObject: Object {
      0: HTMLDocument http: //localhost/kurs/javascript/pages/exercises/IEJQUERY/exercise_jq_01.html, length: 1 }
    <prototype>: Object { jquery: "3.5.1", constructor: S(e, t), length: 0, … }
    */

    console.log('_________________________ 2 __________________________');
    /* 2 */
    /* Transparente jQuery-Methoden
    Diese Methoden ändern die DOM - Knoten innerhalb der Sammlung / Selektion aber die Sammlung / Selektion bleibt unverändert.Die Methode css verändert die DOM - Knoten, den Inhalt der Sammlung / Selektion, aber nicht die Sammlung / Selektion selbst.*/
    var $hauptsammlung = $('p');
    console.log(
      $hauptsammlung
      .css({
        color: 'pink'
      }) // => Transparente Methoden geben die ursprüngliche Sammlung zurück
    );

    /* Destruktive jQuery-Methoden
    Diese Methoden verändern die Sammlung / Selektion und gibt eine neue Sammlung / Selektion zurück.*/
    console.log(
      $hauptsammlung
      .eq(0) //=> Destruktive Methoden verändern die ursprüngliche Sammlung / Selektion und geben eine neue Sammlung / Selektion zurück
    );

    /* Terminierende jQuery-Methoden 
    Geben keine Sammlung / Selektion zurück. Die Methode html gibt eine Zeichenkette zurück. */
    console.log(
      $hauptsammlung.html() //=> Terminierende Methoden geben keine Sammlung / Selektion zurück
    );


    /* Die Art der Methode spielt beim Chaining/Verketten eine Rolle */
    console.log(
      $hauptsammlung
      .eq(0)
      .html()
      //=> Zeichenkette, kein weiterer jQuery-Methodenaufruf möglich. Hier kann mit nativen JS Stringmethoden gearbeitet werden.
      .toUpperCase()
    );

    console.log('_________________________ 3 __________________________');
    /* 3 */
    /* Der DOM-Knoten wird mit nativen JS eingelesen und an die Factory-FN übergeben.  */
    var p = document.getElementsByTagName('p')[0];
    var $p0 = $(p);
    var $body = $(document.body);
    console.log($body);

    var $newEl = $(createEl('p', 'Hello World'));
    console.log($newEl);




    console.log('_________________________ 4 __________________________');
    /* 4 */
    /* Alle CSS-Selektoren und zusätzlich die Filter/Pseudoselektoren von jQuery 
    https: //overapi.com/jquery im BEREICH Selectors zu sehen. */


    console.log('_________________________ 5 __________________________');
    /* 5 */
    /* Filter/Pseudoselektoren von jQuery die als zusatz im Selektorstring stehen können. */
    console.log('$(\'a:eq(3)\') //=> ', $('a:eq(3)'));
    console.log('$(\'a:even()\') //=> ', $('a:even()'));

    console.log('_________________________ 6 __________________________');
    /* 6 */
    /* Je genauer/spezifischer die Selektion desto besser die Performance.
    TIPP: Der beste Einstiegspunkt in das DOM ist immer eine ID. */
    console.log('$(\'a\').eq(0) //=> ', $('a').eq(0));
    console.log('$(\'a\').even() //=> ', $('a').even());

    console.log('_________________________ 7 __________________________');
    /* 7 */
    /* Die Factroy FN erzeugt einen DOM-Knoten und verpackt diesen in ein jQuery-Objekt. */
    var $newElement = $('<p><b>Das ist ein HTMLString</b></p>');

    console.log(
      'var $newElement = $(\'<p><b>Das ist ein HTMLString</b></p>\');\n',
      $newElement, '\n',
      '$newElement[0]',
      $newElement[0]
    );

    console.log('_________________________ 8 __________________________');
    /* 8 */
    /*  */
    var $newElement = $('<h2/>', {
      text: 'Das ist eine Überschrift',
      id: 'headline',
      title: 'Der Titel des Elements',
      click: (e) => alert(e)
    });

    console.log('var $newElement = $(\'<h2/>\',{\n\ttext: \'Das ist eine Überschrift\',\n\tid: \'headline\',\n\ttitle: \'Der Titel des Elements\',\n\tclick: (e) => alert(e);\n})', $newElement);

    $('.column').eq(1).append($newElement);

    console.log($('#headline'));
    console.log($('#headline').attr('id'));
    console.log($('#headline')[0].id);

    console.log($('#headline').attr('title'));
    $('#headline').attr('title', 'Neuer Titel')
    console.log($('#headline').attr('title'));


    console.log('_________________________ x __________________________');
    /* x */
    /*  */

    console.log('_________________________ x __________________________');
    /* x */
    /*  */

    console.log('_________________________ x __________________________');
    /* x */
    /*  */

    console.log('_________________________ x __________________________');
    /* x */
    /*  */

    console.log('_________________________ x __________________________');
    /* x */
    /*  */

    console.log('_________________________ x __________________________');
    /* x */
    /*  */

    console.log('_________________________ x __________________________');
    /* x */
    /*  */

  }

  $(function () {
    lernskript();
  });
})(window, document, jQuery);


/* 
 console.log('___________________________________________________');
 console.log('links //=> ', links);
 console.log('links.item(0) //=> ', links.item(0));
 console.log('links.namedItem(\'link-01\') //=> ', links.namedItem('link-01'));
 console.log('links[0] //=> ', links[0]);
 console.log('Object.values(links) //=> ', Object.values(links));
//  Das folgende Beispiel unterscheidet zwischen direkten Kind-Attributen eines Objekts und den Attributen die durch die prototype - Verarbeitung entstehen. length ist vererbt und keine direkte Kindeigenschaft der HTMLCollection, darum false.
console.log('links.hasOwnProperty(\'length\') //=> ', links.hasOwnProperty('length'));

*/