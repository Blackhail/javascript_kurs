let bubbleSort = (inputArr) => {
    let len = inputArr.length;
    for (let i = 0; i < len; i++) {
        for (let j = 0; j < len; j++) {
            if (inputArr[j] > inputArr[j + 1]) {
                let tmp = inputArr[j];
                inputArr[j] = inputArr[j + 1];
                inputArr[j + 1] = tmp;
            }
        }
    }
    return inputArr;
};
/////////////////////////////////////////////////////////////////////

            for ( var i = 0 ; i < $tbodyRows.length ; i++ ) {

                    for ( var j = 0 ; j < $tbodyRows.length-1 ; j++ ) {

                        if ($tbodyRows[j].children[1].innerText > $tbodyRows[j+1].children[1].innerText) {
                            var $tmp = $tbodyRows[j];
                            $tbodyRows[j] = $tbodyRows[j+1];
                            $tbodyRows[j+1] = $tmp;
                            console.log($tmp);
                    }


                
            }

        }


//////////////////////////////////////////////////////////////////////////////////////////////

        function sortTable(table, col, reverse) {
            var tb = table.tBodies[0], // use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
                tr = Array.prototype.slice.call(tb.rows, 0), // put rows into array
                i;
            reverse = -((+reverse) || -1);
            tr = tr.sort(function (a, b) { // sort rows
                return reverse // `-1 *` if want opposite order
                    * (a.cells[col].textContent.trim() // using `.textContent.trim()` for test
                        .localeCompare(b.cells[col].textContent.trim())
                       );
            });
            for(i = 0; i < tr.length; ++i) tb.appendChild(tr[i]); // append each row in order
     }
     
     function makeSortable(table) {
            var th = table.tHead, i;
            th && (th = th.rows[0]) && (th = th.cells);
            if (th) i = th.length;
            else return; // if no `<thead>` then do nothing
            while (--i >= 0) (function (i) {
                var dir = 1;
                th[i].addEventListener('click', function () {sortTable(table, i, (dir = 1 - dir))});
            }(i));
     }
     
     function makeAllSortable(parent) {
            parent = parent || document.body;
            var t = parent.getElementsByTagName('table'), i = t.length;
            while (--i >= 0) makeSortable(t[i]);
     }

     //////////////////////////////////////////////////////////////////////////////////////

     sortTable = function(tableName, rowClass, columnNumber, ascending) {
        var row, cell, cellContent;
        var comparisonRow, comparisonCell, comparisonContent;
 
        $("#" + tableName + " tr." + rowClass).each(function(i) {
            row = $("#" + tableName + " tr." + rowClass + ":eq(" + i + ")");
            cell = $(row).find("td:eq(" + columnNumber + ")");
            cellContent = $(cell).html();
 
            $("#" + tableName + " tr." + rowClass).each(function(j) {
                comparisonRow = $("#" + tableName + " tr." + rowClass + ":eq(" + j + ")");
                comparisonCell = $(comparisonRow).find("td:eq(" + columnNumber + ")");
                comparisonContent = $(comparisonCell).html();
 
                if ( (ascending && cellContent < comparisonContent) || (!ascending && cellContent > comparisonContent) ) {
                    $(row).insertBefore(comparisonRow);
                    return false;
                }
            });
        });
 };
 
 //////////////////////////////////////////////////////////////////////////////////////


//  {"data" [ {
//     "stuff": [
//             {"onetype":[
//                 {"id":1,"name":"John Doe"},
//                 {"id":2,"name":"Don Joeh"}
//             ]},
//             {"othertype":[
//                 {"id":2,"company":"ACME"}
//     ]}]
//             },
//             {
//     "otherstuff": [
//             {"thing": [
//                 [1,42],
//                 [2,2]]
//     }]
// }]}