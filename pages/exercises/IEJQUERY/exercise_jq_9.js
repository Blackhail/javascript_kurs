;(function(window,document,$){
    'use strict';

function ajax1_kunden () {

    function getCompany (e) {
        e.preventDefault();
        var index =  $(this).index('a');
        console.log(index);

        var client = $(this)[0].innerText;
        $('#info').empty();
       
        var html = '<h3>' + client + '</h3>';
        html += '<ul>';
        $.get('../../../lib/data/clients.json', function (data) {

            var len = $(data)[0].clients.length;
            for (var i = 0; i < len; i++) {

                if (client == $(data)[0].clients[i].company) {
                    html += '<li>name: ' + $(data)[0].clients[i].name + '</li>';
                    html += '<li>id: ' + $(data)[0].clients[i].id + '</li>';
                    html += '<li>age: ' + $(data)[0].clients[i].age + '</li>';
                    html += '<li>gender: ' + $(data)[0].clients[i].gender + '</li>';
                    html += '<li>email: ' + $(data)[0].clients[i].email + '</li>';
                    html += '<li>phone: ' + $(data)[0].clients[i].phone + '</li>';
                    html += '<li>address: ' + $(data)[0].clients[i].address + '</li>';
                    html += '<li>isActive: ' + $(data)[0].clients[i].isActive + '</li>';
                    html += '</ul>'
                }
        }

        $('#info').append(html);
        });       
    }

    function isChanged (e) {
        e.preventDefault();
        $('#info').empty();

        var $active = $('#isActive').is(':checked');
        var $links = $('.column').eq(1).find('li');

        $.get('../../../lib/data/clients.json', function (data) {
            if ($active) {

                for ( var i = 0 ; i < $links.length ; i++ ) {
                    if (!$(data)[0].clients[i].isActive ) {
                        $links.eq(i).hide();
                    }
                }
            } else {
                for ( var i = 0 ; i < $links.length ; i++ ) {
                $links.eq(i).show();
            }}
        })
    }

    $('.column').eq(1).find('ul').on('click' , 'a' , getCompany);
    $('#isActive').on('change', isChanged);
}

    $(function(){
        ajax1_kunden ();
    });

})(window,document,jQuery);