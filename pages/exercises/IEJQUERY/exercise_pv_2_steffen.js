;
(function (window, document, $) {
  'use strict';

  //// Var 1 ///////////////////////////////////////////////////////////////////////////

  function handleClick( /* e */ ) {
    var clicked = this.textContent.trim();
    $.getJSON('../../../lib/data/IEJQUERY/exercise_pv2.json', function (data) {
      var heroData;
      data.members.forEach(function (hero) {
        if (clicked === hero.name) {
          heroData = hero;
        }
      });
      var html = '<article>';
      html += '<h2>' + heroData.name + '</h2>';
      html += '<p>Secret identity: ' + heroData.secretIdentity + '</p>';
      html += '<p>Age: ' + heroData.age + '</p>';
      html += '<p>Superpowers:</p>';
      html += '<ul><li>' + heroData.powers.join('</li><li>') + '</li></ul>';

      html += '</article>';
      $('#ausgabe').empty().append(html);
    });
    return false;
  }

  function handleRequestOnLoad(data) {
    var members = data.members;
    var html = '<h2>Super Hero Squad</h2>';
    html += '<ul id="list">';
    members.forEach(function (hero) {
      html += '<li><a href="#">' + hero.name + '</a></li>';
    });
    html += '</ul>';
    $('#overview').append(html);
  }

  $(function () {
    $.getJSON('../../../lib/data/IEJQUERY/exercise_pv2.json', handleRequestOnLoad);
    $('#overview').on('click', 'a', handleClick);
  });
})(window, document, jQuery);


//// Var 2 ///////////////////////////////////////////////////////////////////////////

;
(function (window, document, $) {
  'use strict';
  var heroes = null;

  function handleClick( /* e */ ) {
    var clicked = this.textContent.trim();
    var heroData;
    heroes.members.forEach(function (hero) {
      if (clicked === hero.name) {
        heroData = hero;
      }
    });
    var html = '<article>';
    html += '<h2>' + heroData.name + '</h2>';
    html += '<p>Secret identity: ' + heroData.secretIdentity + '</p>';
    html += '<p>Age: ' + heroData.age + '</p>';
    html += '<p>Superpowers:</p>';
    html += '<ul><li>' + heroData.powers.join('</li><li>') + '</li></ul>';
    html += '</article>';

    $('#ausgabe').empty().append(html);
    return false;
  }

  function handleRequestOnLoad(data) {
    /* Einmalig angefordertes Objekt in semi-globaler Variable heroes gespeichert. */
    heroes = data;
    var members = heroes.members;
    var html = '<h2>Super Hero Squad</h2>';
    html += '<ul id="list">';
    members.forEach(function (hero) {
      html += '<li><a href="#">' + hero.name + '</a></li>';
    });
    html += '</ul>';
    $('#overview').append(html);
  }

  $(function () {
    $.getJSON('../../../lib/data/IEJQUERY/exercise_pv2.json', handleRequestOnLoad);
    $('#overview').on('click', 'a', handleClick);
  });
})(window, document, jQuery);

//// Var 3 ///////////////////////////////////////////////////////////////////////////

;
(function (window, document, $) {
  'use strict';

  function findHero(data, searchValue) {
    for (var index = 0; index < data.members.length; index++) {
      var element = data.members[index];
      if (searchValue === element.name) {
        return element;
      }
    }
  }

  function createHTML(heroes, heroName) {
    var heroData = findHero(heroes, heroName);
    var html = '<article>';
    html += '<h2>' + heroData.name + '</h2>';
    html += '<p>Secret identity: ' + heroData.secretIdentity + '</p>';
    html += '<p>Age: ' + heroData.age + '</p>';
    html += '<p>Superpowers:</p>';
    html += '<ul><li>' + heroData.powers.join('</li><li>') + '</li></ul>';
    html += '</article>';
    return html;
  }

  function handleRequestClick(heroes) {
    /* Hier zeigt der this Pointer auf das Element/Objekt, das wir in den settings der Methode ajax über context gespeichert haben. */
    var heroName = $(this).data('heroName');
    $(this).removeData('heroName');
    var html = createHTML(heroes, heroName);
    $('#ausgabe').empty().append(html);
  }

  function handleClick( /* e */ ) {
    var clicked = this.textContent.trim();
    $('body').data('heroName', clicked);
    startRequest(handleRequestClick)
    return false;
  }

  function handleRequestOnLoad(data) {
    var members = data.members;
    var html = '<h2>Super Hero Squad</h2>';
    html += '<ul id="list">';
    members.forEach(function (hero) {
      html += '<li><a href="#">' + hero.name + '</a></li>';
    });
    html += '</ul>';
    $('#overview').append(html);
  }

  function startRequest(fnSuccess) {
    $.ajax({
      /* Gibt an welches Element/Objekt im this pointer der Callback-FN-Referenzen liegt. Z.b.: in success zeigt this jetzt auf das body-Element. */
      context: $('body')[0],
      url: '../../../lib/data/IEJQUERY/exercise_pv2.json',
      type: 'GET',
      dataType: 'json',
      success: fnSuccess,
      error: function (xhr, status, error) {
        console.groupCollapsed('__________ handleError __________');
        console.log(arguments);
        console.log('xhr: ', xhr); //XHR-Objekt
        console.log('error: ', error);
        console.log('status: ', status); //XHR-Status-Message 
        console.groupEnd('_____ handleError _____');
      }
    });
  }


  $(function () {
    startRequest(handleRequestOnLoad);
    $('#overview').on('click', 'a', handleClick);
  });
})(window, document, jQuery);

////////////////////////////////////////////////////////////////////////////////

// ;
// (function (window, document, $) {
//   'use strict';

//   function handleRequestClick(heroes, heroName) {
//     var heroData;
//     heroes.members.forEach(function (hero) {
//       if (heroName === hero.name) {
//         heroData = hero;
//       }
//     });
//     var html = '<article>';
//     html += '<h2>' + heroData.name + '</h2>';
//     html += '<p>Secret identity: ' + heroData.secretIdentity + '</p>';
//     html += '<p>Age: ' + heroData.age + '</p>';
//     html += '<p>Superpowers:</p>';
//     html += '<ul><li>' + heroData.powers.join('</li><li>') + '</li></ul>';

//     html += '</article>';
//     $('#ausgabe').empty().append(html);
//   }

//   function handleClick( /* e */ ) {
//     var clicked = this.textContent.trim();
//     $.ajax({
//       url: '../../../lib/data/IEJQUERY/exercise_pv2.json',
//       type: 'GET',
//       dataType: 'json',
//       success: function (data) {
//         handleRequestClick(data, clicked);
//       },
//       error: handleError
//     });
//     return false;
//   }

//   function handleRequestOnLoad(data) {
//     var members = data.members;
//     var html = '<h2>Super Hero Squad</h2>';
//     html += '<ul id="list">';
//     members.forEach(function (hero) {
//       html += '<li><a href="#">' + hero.name + '</a></li>';
//     });
//     html += '</ul>';
//     $('#overview').append(html);
//   }

//   function handleError(xhr, status, error) {
//     console.groupCollapsed('__________ handleError __________');
//     console.log(arguments);
//     console.log('xhr: ', xhr); //XHR-Objekt
//     console.log('error: ', error);
//     console.log('status: ', status); //XHR-Status-Message 
//     console.groupEnd('_____ handleError _____');
//   }


//   $(function () {
//     $.ajax({
//       url: '../../../lib/data/IEJQUERY/exercise_pv2.json',
//       type: 'GET',
//       dataType: 'json',
//       success: handleRequestOnLoad,
//       error: handleError
//     });
//     $('#overview').on('click', 'a', handleClick);
//   });
// })(window, document, jQuery);