;
(function (window, document, $) {
  'use strict';

  function task1() {
    /////////////////////////////////////////////////////////
    /* Binden eines Handlers am neuem Element */
    // function handleSubmit(e) {
    //   /* var value = form.elements.neu.value.trim();
    //   var value = $('#neu')[0].value.trim(); */
    //   var value = $('#neu').val().trim();
    //   if (value !== '') {
    //     /* $('<li><a href="#">' + value + '</a></li>')
    //       .appendTo('#a1 ul')
    //       .find('a')
    //       .on('click', handleClick);*/
    //     $('#a1')
    //       .find('ul')
    //       .append('<li><a href="#">' + value + '</a></li>')
    //       .find('a')
    //       .last()
    //       .on('click', handleClick);

    //     $('#neu').val('');
    //   }
    //   return false;
    // }      
    // }

    /////////////////////////////////////////////////////////
    /* Clonen eines der Element, an dem wir bereits einen Handler gebunden haben. */
    function handleSubmit(e) {
      var value = $('#neu').val().trim();
      if (value !== '') {
        var $node = $('#a1')
          .find('ul')
          .first()
          .find('li')
          .first()
          .clone(true);

        // $node.find('a').empty().append(value);
        $node.find('a').text(value);
        $('#a1').find('ul').append($node);
        $('#neu').val('')
      }
      return false;
    }


    function handleClick(e) {
      console.log(e.target, this);
      console.log(e.target.textContent, this.textContent);
      $('.marker').removeClass('marker');
      $(this).addClass('marker');
      /* e.preventDefault(); e.stopPropagation() */
      return false;
    }

    // $('#a1')
    //   .find('ul')
    //   .find('a')
    //   .on('click', handleClick);
    /////////////////////////////////////////////////////////
    /* Option 3: Delegieren 
    Der EventListener wird am Elternelement gebunden. Der Clone hat keinen Handler, er erbt diesen von der UL in der er sich befindet.*/
    $('#a1').find('ul').on('click', 'a', handleClick);

    $('#a1')
      .find('form')
      .on('submit', handleSubmit);
  }

  var $selected = null;

  function task2() {
    function handleSubmit(e) {
      e.preventDefault();
      var value = $('#edit').val().trim();
      if ($selected !== null) {
        $selected.text(value);
        $selected = null;
        $('#edit').val('');
      }
      return false;
    }

    function handleClick() {
      $selected = $(this);
      console.log(this);
      $('#edit').val($(this).text());
      return false;
    }

    $('#a2').find('a').on('click', handleClick);

    $('#a2').find('form').on('submit', handleSubmit);

  }

  $(function () {
    task1();
    task2();
  });
})(window, document, jQuery);