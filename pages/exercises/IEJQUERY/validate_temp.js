;
(function (window, document, $) {
  'use strict';
  $.validator.addMethod("letterswithbasicpunc", function (value, element) {
    return this.optional(element) || /^[a-zäöüß\-.,()'"\s]+$/i.test(value);
  }, 'Bitte nur Buchstaben und Interpunktion');

  var settings = {
    // debug: true,
    errorClass: 'error',
    normalizer: function (value) {
      return $.trim(value);
    },
    errorPlacement: function ($errorElement, $element) {
      $element.after($errorElement);
    },
    rules: {
      fname: {
        letterswithbasicpunc: true
      },
      lname: {
        letterswithbasicpunc: true
      },
      title: {
        letterswithbasicpunc: true
      },
      org: {
        letterswithbasicpunc: true
      },
      eaddress: {
        email: true
      },
      phone: {
        pattern: /^\+(?:[0-9\x20]⋅?){6,14}[0-9]$/
      },
      fax: {
        pattern: /^\+(?:[0-9\x20]⋅?){6,14}[0-9]$/
      }
    }
  }

  $(function () {
    $('form').eq(0).validate(settings);
  });
})(window, document, jQuery);