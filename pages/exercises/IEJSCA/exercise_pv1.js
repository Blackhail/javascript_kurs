(function (window, document) {
    'use strict';

/////// Variablen //////////////////////////////////////////////////



/////// Funktionen /////////////////////////////////////////////////

    function changeImg (e) {
    
    }



/////// bindHandles ////////////////////////////////////////////////

    function bindHandles () {
    // clickHandle for thumbs:

        var thumbs = document.getElementsByClassName('demo'); // collection of thumbs in var
        var thumb;
        var len = thumbs.length;

        for (var i = 0 ; i < len ; i++) { // with FOR >> create clickHandle for each thumb
            // thumb = document.createElement(thumbs[i].id = i.toString());
            thumb = thumbs[i];
            thumb.addEventListener('click' , changeImg);
            console.log(thumb); // clicked thumb = THIS in FN changeImg
        }
    }

/////// EventListener ///////////////////////////////////////////////

    window.addEventListener('load', function () {
        // code, der auf DOM zugreift:
        bindHandles();
        var slideImages = window.getComputedStyle(document.querySelector('.mySlides'), ':not(:first-child)').getPropertyValue('block');
        // var slideImages = window.getComputedStyle(document.getElementsByClassName('mySlides'), ':not(:first-child)').getPropertyValue('block');
        // var slideImages = window.getComputedStyle(document.getElementsByClassName('mySlides'), ':not(:first-child)'):;
        console.log (slideImages);
    });


})(window, document);




