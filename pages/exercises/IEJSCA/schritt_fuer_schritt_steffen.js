(function (window, document) {
  'use strict';

  function createTaskHeadline(txt) {
    var h2 = document.createElement('h2');
    h2.appendChild(document.createTextNode(txt));
    return h2;
  }


  // Funktionen und Variablen deklarieren
  function task1() {
    var ausgabe = document.getElementById('ausgabe');
    ausgabe.appendChild(createTaskHeadline('Aufgabe 1'));

    var li;
    var txt;
    var ul = document.createElement('ul');

    var animal;
    var len = animals.length;

    for (var i = 0; i < len; i++) {
      animal = animals[i];
      li = document.createElement('li');
      txt = document.createTextNode(animal.name + ' ' + animal.species);
      li.appendChild(txt);
      ul.appendChild(li);
    }
    ausgabe.appendChild(ul);


    ul = document.createElement('ul');
    for (var i = 0; i < len; i++) {
      animal = animals[i];
      li = document.createElement('li');
      txt = document.createTextNode(animal.name);
      li.appendChild(txt);
      ul.appendChild(li);
    }
    ausgabe.appendChild(ul);
  }

  function task2() {
    /* Ausgabe Container einlesen */
    var ausgabe = document.getElementById('ausgabe');
    /* Überschrift der ausgabe erzeugen und einfügen */
    ausgabe.appendChild(createTaskHeadline('Aufgabe 2'));
    /* for-in-Schleife läuft über äußeres Objekt */
    for (var key in order) {
      /* Das Array in der Objekteigenschaft wird in orderList gespeichert */
      var orderList = order[key];
      var h2 = document.createElement('h2');
      /* key enthält den Wert aus den Objekteigenschaften (Der Name des Kunden) */
      h2.appendChild(document.createTextNode(key));
      /* Die Überschrift mit dem Namen des Kunden ausgelesen aus key vor der UL ausgeben */
      ausgabe.appendChild(h2);
      /* for-Schleife läuft über das Array aus der Objekteigenschaft */
      for (var i = 0; i < orderList.length; i++) {
        /* Index i aus dem Array orderList enhält ein weiteres Objekt mit dem jeweiligen Produkt. Als Eigenschaften dieses Produkts finden wir name, price und quantity, in denen dann die zugehörigen Werte liegen. */
        var product = orderList[i];
        /* Jedes Produkt wird in einer eigenen UL ausgegeben. */
        var ul = document.createElement('ul');
        /* for-in-Schleife läuft über das Objekt gespeichert in product. 
        in key2 liegen die Eigenschaftsnamen name, price und quantity  */
        for (var key2 in product) {
          /* value enthält den Wert aus den Eigenschaften name, price und quantity */
          var value = product[key2];
          /* LI erzeugen */
          var li = document.createElement('li');
          /* Text für LI erzeugen */
          var txt = document.createTextNode(key2 + ': ' + value);
          /* Text in LI einfügen */
          li.appendChild(txt);
          /* LI in UL einfügen */
          ul.appendChild(li);
        }
        /* Erst nach der Schleife wird die UL in div#ausgabe eingefügt. */
        ausgabe.appendChild(ul);
      }

    }
  }

  function task3() {
    function bindHandler() {
      var elements = document
        .getElementById('customers')
        .getElementsByTagName('a');
      for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', handleClick);
      }
    }

    function handleClick(e) {
      e.preventDefault();
      var ausgabe = document.getElementById('ausgabe');
      var name = this.textContent;
      var orderList = order[name];
      var list = document.getElementById('container');
      if (list) {
        list.remove();
      }

      var container = document.createElement('div');
      container.setAttribute('id', 'container');

      var h2 = document.createElement('h2');
      h2.appendChild(document.createTextNode(name));
      container.appendChild(h2);

      for (var i = 0; i < orderList.length; i++) {
        var product = orderList[i];
        var ul = document.createElement('ul');
        for (var key in product) {
          var value = product[key];
          var li = document.createElement('li');
          li.appendChild(document.createTextNode(key + ': ' + value));
          ul.appendChild(li);
        }
        container.appendChild(ul);
      }
      ausgabe.appendChild(container);
    }

    function createList() {
      var ausgabe = document.getElementById('ausgabe');
      ausgabe.appendChild(createTaskHeadline('Aufgabe 3'));

      var h2 = document.createElement('h2');
      h2.appendChild(document.createTextNode('Kunden'));
      ausgabe.appendChild(h2);

      var ul = document.createElement('ul');
      // ul.id = 'customers';
      ul.setAttribute('id', 'customers');
      for (key in order) {
        var li = document.createElement('li');
        var a = document.createElement('a');
        a.setAttribute('href', '#');
        // a.addEventListener('click', handleClick);
        a.appendChild(document.createTextNode(key));
        li.appendChild(a);
        ul.appendChild(li);
      }
      ausgabe.appendChild(ul);
    }

    createList();
    bindHandler();
  }

  function task4() {
      
    function handleRequest(order, name) {
      var orderList = order[name];
      var container = document.getElementById('container');
      while (container.lastChild) {
        container.lastChild.remove();
      }

      var h2 = document.createElement('h2');
      h2.appendChild(document.createTextNode(name));
      container.appendChild(h2);

      for (var i = 0; i < orderList.length; i++) {
        var product = orderList[i];
        var ul = document.createElement('ul');
        for (var key in product) {
          var value = product[key];
          var li = document.createElement('li');
          li.appendChild(document.createTextNode(key + ': ' + value));
          ul.appendChild(li);
        }
        container.appendChild(ul);
      }
    }

    function handleClick(e) {
      e.preventDefault();
      var key = e.target.textContent; //this.textContent
      // var xhr = new XMLHttpRequest();
      // xhr.open('GET', '../../../lib/data/order.json', true);
      // xhr.onreadystatechange = function () {
      //   if (xhr.readyState === 4 && xhr.status === 200) {
      //     var data = JSON.parse(xhr.responseText);
      //     handleRequest(data, key);
      //   }
      // }
      // xhr.send();
      ajaxGetJSON('../../../lib/data/order.json', function (data) {
        handleRequest(data, key);
      });
    }

    function bindHandler() {
      var links = document.getElementById('customers').getElementsByTagName('a');

      for (var i = 0; i < links.length; i++) {
        links[i].addEventListener('click', handleClick);
      }
    }


    bindHandler();
  }

  function createHTMLTask4() {
    var ausgabe = document.getElementById('ausgabe');
    var h2 = document.createElement('h2');
    h2.appendChild(document.createTextNode('Kunden'));
    ausgabe.appendChild(h2);

    var ul = document.createElement('ul');
    ul.setAttribute('id', 'customers');

    var li = document.createElement('li');
    var a = document.createElement('a');
    a.appendChild(document.createTextNode('Jan Waffles'));
    a.setAttribute('href', '#');
    li.appendChild(a);
    ul.appendChild(li);

    li = document.createElement('li');
    a = document.createElement('a');
    a.appendChild(document.createTextNode('Jana Smith'));
    a.setAttribute('href', '#');
    li.appendChild(a);
    ul.appendChild(li);

    ausgabe.appendChild(ul);
    var container = document.createElement('div');
    container.setAttribute('id', 'container');

    ausgabe.appendChild(container);
  }

  window.addEventListener('load', function () {
    // task1();
    // task2();
    // task3();

    /* Dieser Teil wird normalerweise auf dem Server durch PHP erzeugt. */
    createHTMLTask4();
    task4();
  });
})(window, document);


//////////////////////////////////////////////////////////////////////////
/* 1
Ausgaben im Dokument, legen Sie dazu einen div#ausgabe an den sich nachträglich mit JavaScript befüllen.

a. Geben Sie alle Tiere des Array animals in einer ungeordneten Liste aus.
b. Geben Sie nur die Namen der Tiere in einer weiteren ungeordneten Liste aus.
*/

var animals = [{
    name: 'fluffy',
    species: 'rabbit'
  },
  {
    name: 'caro',
    species: 'cat'
  }, {
    name: 'Jimmy',
    species: 'fish'
  }, {
    name: 'Heinrich',
    species: 'elkdog'
  }
];

console.log('_____ Array/Objekt _____');
console.log(animals);

console.log('_____ Manueller Zugriff über [] _____');
console.log(animals[0]);
console.log(animals[1]);
console.log(animals[2]);
console.log(animals[3]);

console.log('_____ for-Schleife _____');
for (var i = 0; i < animals.length; i++) {
  console.log(animals[i]);
}

console.log('_____ forEach-Methode _____');
animals.forEach(function (animal) {
  console.log(animal);
});

console.log('_____ for-of _____');
for (var animal of animals) {
  console.log(animal);
}

console.log('--------- Objekteigenschaft im Arrayindex ---------');
console.log('_____ Manueller Zugriff über [] _____');
console.log(animals[0].name, animals[0].species);
console.log(animals[1].name, animals[1].species);
console.log(animals[2].name, animals[2].species);
console.log(animals[3].name, animals[3].species);

console.log('_____ for-Schleife _____');
for (var i = 0; i < animals.length; i++) {
  console.log(animals[i].name, animals[i].species);
}

console.log('_____ forEach-Methode _____');
animals.forEach(function (animal) {
  console.log(animal.name, animal.species);
});

console.log('_____ for-of _____');
for (var animal of animals) {
  console.log(animal.name, animal.species);
}

//////////////////////////////////////////////////////////////////////////
/* 2
Ausgaben im Dokument, legen Sie dazu einen div#ausgabe an den sich nachträglich mit JavaScript befüllen.

a. Geben Sie alle Namen und die zugehörige Bestellung im Dokument nach folgenden Schema aus.

<h2>Jan Waffles</h2>
<ul>
  <li>name: waffle maker</li>
  <li>price: 80</li>
  <li>quantity: 2</li>
</ul>
*/
var order = {
  'Jan Waffles': [{
    name: 'waffle maker',
    price: '80',
    quantity: '2'
  }, {
    name: 'blender',
    price: '200',
    quantity: '1'
  }, {
    name: 'knife',
    price: '10',
    quantity: '4'
  }],
  'Jana Smith': [{
    name: 'waffle maker',
    price: '80',
    quantity: '1'
  }, {
    name: 'knife',
    price: '10',
    quantity: '2'
  }, {
    name: 'pot',
    price: '20',
    quantity: '3'
  }]
};

console.log('--------- order ---------');
console.log(order);
console.log('_____ for-in mit for mit for-in _____');
for (var key in order) {
  console.log(key, order[key]);
  for (var i = 0; i < order[key].length; i++) {
    console.log(i, order[key][i]);
    for (var key2 in order[key][i]) {
      console.log(key2, order[key][i][key2]);
    }
  }
}

console.log('_____ Object.keys und Object.values _____');
Object.keys(order).forEach((key) => {
  console.log(key);
  Object.values(order[key]).forEach((obj) => {
    console.log(obj);
    console.log(obj.name);
    console.log(obj.price);
    console.log(obj.quantity);
    // for (var key in obj) {
    //   console.log(key, obj[key]);
    // }
  });
});

console.log('_____ Object.keys und Object.values _____');
var names = Object.keys(order);
names.forEach((name) => {
  console.log(name);
  var orderList = order[name];
  orderList.forEach((product) => {
    console.log(product);
    console.log(product.name);
    console.log(product.price);
    console.log(product.quantity);
    // for (var key in product) {
    //   console.log(key, product[key]);
    // }
  });
});


//////////////////////////////////////////////////////////////////////////
/* 3
Ausgaben im Dokument, legen Sie dazu einen div#ausgabe an den sich nachträglich mit JavaScript befüllen.

a. Erzeugen Sie im HTML-Dokument eine Liste aus den Eigenschaftsnamen des Objekts order.

<h2>Kunden</h2>
<ul id="customers">
  <li><a href="#">Jan Waffles</a></li>
  <li><a href="#">Jana Smith</a></li>
</ul>

b. Binden Sie an den Hyperlinks in der Liste einen Eventlistener (click).
Wenn ein Name angeklickt wird geben Sie die zugehörige Bestellung als ungeordnete Liste im div#ausgabe aus.

Jan Waffles wurde geklickt:

<h2>Jan Waffles</h2>
<ul>
  <li>name: waffle maker</li>
  <li>price: 80</li>
  <li>quantity: 2</li>
</ul>
*/

//////////////////////////////////////////////////////////////////////////
/* 4
a. Erstellen Sie aus dem Objekt order eine JSON-Datei.
b. Erzeugen Sie im HTML-Dokument eine Liste aus den Eigenschaftsnamen des Objekts order.

<h2>Kunden</h2>
<ul id="customers">
  <li><a href="#">Jan Waffles</a></li>
  <li><a href="#">Jana Smith</a></li>
</ul>

c. Binden Sie an den Hyperlinks in der Liste einen Eventlistener (click).
Wenn ein Name angeklickt wird, fordern sie die in Aufgabe 3.a erzeugte JSON-Datei an und geben Sie die zugehörige
Bestellung als ungeordnete Liste im div#ausgabe aus.

Jan Waffles wurde geklickt:

<h2>Jan Waffles</h2>
<ul>
  <li>name: waffle maker</li>
  <li>price: 80</li>
  <li>quantity: 2</li>
</ul>
*/