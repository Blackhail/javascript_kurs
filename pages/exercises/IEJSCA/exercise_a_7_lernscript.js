///////// Lernskript Javascript XMLHttpRequest (XHR) und Ajax ////////////

// 1)
// alle textbasierten Formate
// Bsp.: xml, json, Antworten von php-script als requests-answer

// 2)
// öffnet Verbindung zu url auf server, http-request

// 3)
// synchron: zeilich hintereinander
// asynchron: script muss nicht warten, bis Anfrage beantwortet ist
// parallel andere Aufgaben

// 4) Was sind HTTP-Statuscodes und wo finden Sie diese im XHR-Objekt?
// XMLHttpRequest.readyState - gibt Status zurück
// HTTP-Antwortstatuscodes geben an, ob eine bestimmte HTTP- Anforderung 
// erfolgreich abgeschlossen wurde. 
// Die Antworten sind in fünf Klassen unterteilt: 
// Informationsantworten, erfolgreiche Antworten, Weiterleitungen, Clientfehler und Serverfehler.

// 5) Nennen Sie die 4 mindestens benötigten Schritte, um einen XHR-Request zu starten.
// - erzeugen einer Instanz var xhr = new XMLHttpRequest();
// - Aufruf der open-Methode
// - Abfragen von onreadystatechange + Aufruf einer Funktion, die bei Antwort die Verarbeitung übernimmt.
// - senden

// 6) Nennen Sie die 5 Möglichen Werte der Eigenschaft readyState, 
//   erklären Sie was diese bedeuten und zu welchem Zeitpunkt sie erreicht werden.
// - unsent - Der XMLHttpRequest-Client wurde erstellt, die open () -Methode wurde jedoch noch nicht aufgerufen.
// - opened - Die Methode open () wurde aufgerufen.
// - headers_received - send () wurde aufgerufen und die Antwortheader wurden empfangen.
// - loading - Der Körper der Antwort wird empfangen.
// - done - Der Abrufvorgang ist abgeschlossen. (erfolgreich abgeschlossen oder fehlgeschlagen)

// 7) In welcher Eigenschaft des XHR-Objekts befindet sich die Antwort, wenn der ReadyState 4 erreicht hat?
// - siehe 6)

// 8) Welchen Datentyp hat die Antwort vom Server in fast allen Fällen?
// - Text

// 9) Wie wandeln Sie eine Anfrage die im JSON-Format geliefert wurde in ein JavaScript-Objekt um diese auszugeben?
// JSON.parse(xhr.responseText);

// 10) Wie kann man die Antwort einer XML-Datei auslesen und verarbeiten?
// For-Schleife, getElement-Methoden, parse.From

// 11) Welche Möglichkeiten gibt es, um Daten über den XHR-Request an den Server zu senden. 
// Beschreiben Sie jeweils die Möglichkeit für eine GET und eine POST Übertragung. 
// Wie muss die Anfrage aufgebaut werden?

// var xhr = new XMLHttpRequest(); // instanz erzeugen
// xhr.open('POST', url, true); oder instanz.open('GET', url, true); // method festlegen

//////// Beispiel: //////// 
// (function(window, document) {
//     'use strict';

//     // function ajaxGET() {
//     //   var xhr = new XMLHttpRequest();
//     //   /* $_GET['term'] = 'Ajax' $_GET['kurs'] = 'Aufbau' */
//     //   xhr.open('GET', 'lib/data/request.php?term=Ajax&kurs=Aufbau', true);
//     //   /* Bei GET istsetRequestHeader Optional */
//     //   xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
//     //   xhr.onreadystatechange = function() {
//     //     if (xhr.readyState === 4) {
//     //       document.body.innerHTML += '<h2>Ajax GET</h2>' + xhr.responseText + '<hr>';
//     //     }
//     //   };
//     //   xhr.send();
//     // }


//     /* formData selbst anlegen */
//     function ajaxGET() {
//       var formData = new FormData(); // leer
//       formData.append('term', 'Ajax');
//       formData.append('kurs', 'Aufbau');
//       var xhr = new XMLHttpRequest();
//       xhr.open('GET', 'lib/data/request.php', true);
//       /* Bei GET istsetRequestHeader Optional */
//       xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
//       xhr.onreadystatechange = function() {
//         if (xhr.readyState === 4) {
//           document.body.innerHTML += '<h2>Ajax GET</h2>' + xhr.responseText + '<hr>';
//         }
//       };
//       xhr.send(formData);
//     }



//     function ajaxPOST() {
//       var xhr = new XMLHttpRequest();
//       xhr.open('POST', 'lib/data/request.php', true);
//       /* Bei POST ist setRequestHeader erforderlich  */
//       xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
//       xhr.onreadystatechange = function() {
//         if (xhr.readyState === 4) {
//           document.body.innerHTML += '<h2>Ajax POST</h2>' + xhr.responseText + '<hr>';
//         }
//       };
//       /* $POST['term'] = 'Ajax' $POST['kurs'] = 'Aufbau' */
//       xhr.send('term = Ajax & kurs = Aufbau');
//     }

//     window.addEventListener('load', function() {
//       // ajaxGET();
//       // ajaxPOST();

//     });
//   })(window, document);
//////// Ende Beispiel: ////////

// 12) Wie können Sie das formData-Objekt für eine XHR-Request verwenden.
// Schnittstelle für Erstellung von Schlüssel-Wert-Paaren