(function (window, document) {
  'use strict';
  var article = [
    {
      headline:
        'Samsung MZ-75E250B/EU 850 EVO interne SSD 250GB (6,4 cm (2,5 Zoll), SATA III) Marke	Samsung',
      Marke: 'Samsung',
      'Modell/Serie': 'SSD 850 EVO 250GB',
      Artikelgewicht: '1,18 Kg',
      Produktabmessungen: '7 x 10 x 0,7 cm',
      Modellnummer: 'MZ-75E250B/EU',
      Farbe: 'Schwarz',
      Formfaktor: '2.5 Inches',
      'Speicher-Art': 'DDR3 SDRAM',
      'Größe Festplatte': '250 GB',
      Festplatteninterface: 'Serial ATA-600',
      'Unterstützte Software': 'Ja',
      'Unverb. Preisempf.:': '',
      'Preis:': 'EUR 81,88',
    },
    {
      headline:
        'Samsung MZ-75E250B/EU 850 EVO interne SSD 500GB (6,4 cm (2,5 Zoll), SATA III) Marke	Samsung',
      Marke: 'Samsung',
      'Modell/Serie': 'SSD 850 EVO 500GB',
      Artikelgewicht: '1,18 Kg',
      Produktabmessungen: '7 x 10 x 0,7 cm',
      Modellnummer: 'MZ-75E250B/EU',
      Farbe: 'Schwarz',
      Formfaktor: '2.5 Inches',
      'Speicher-Art': 'DDR3 SDRAM',
      'Größe Festplatte': '500 GB',
      Festplatteninterface: 'Serial ATA-600',
      'Unterstützte Software': 'Ja',
      'Unverb. Preisempf.:': 'EUR 189,00 ',
      'Preis:': 'EUR 95,80',
    },
    {
      headline:
        'Samsung MZ-75E250B/EU 850 EVO interne SSD 1TB (6,4 cm (2,5 Zoll), SATA III) Marke	Samsung',
      Marke: 'Samsung',
      'Modell/Serie': 'SSD 850 EVO 1TB',
      Artikelgewicht: '1,18 Kg',
      Produktabmessungen: '7 x 10 x 0,7 cm',
      Modellnummer: 'MZ-75E250B/EU',
      Farbe: 'Schwarz',
      Formfaktor: '2.5 Inches',
      'Speicher-Art': 'DDR3 SDRAM',
      'Größe Festplatte': '1TB',
      Festplatteninterface: 'Serial ATA-600',
      'Unterstützte Software': 'Ja',
      'Unverb. Preisempf.:': 'EUR 389,00 ',
      'Preis:': 'EUR 189,00',
    },
  ];
  // Funktionen und Variablen deklarieren
  function lernskript() {
    console.log('Node //=>', Node);
    console.log('Node.ELEMENT_NODE //=> ', Node.ELEMENT_NODE);
    console.log(
      "document.getElementById('switcher').nodeType //=>",
      document.getElementById('switcher').nodeType
    );

    console.log(
      document.getElementById('switcher').getElementsByTagName('button')
    );

    var body = document.body;
    // var kids = body.children;
    var kids = body.childNodes;
    var kid;
    for (var i = 0; i < kids.length; i++) {
      kid = kids[i];
      console.log(
        kid,
        kid.nodeType,
        kid.nodeName,
        kid.textContent.substr(0, 10)
      );
    }
  }

  window.addEventListener('load', function () {
    // Der Code der auf das DOM zugreift hier
    // lernskript();
    var container = document.getElementById('info');
    emptyEl(container);
    /* Leerer DOM-Knoten, sammelt alle Knoten die eingefügt werden sollen *1* */
    var fragment = document.createDocumentFragment();

    var h2 = createEl('h2', article[0].headline);
    fragment.appendChild(h2);
    /* Referen/Zeiger wie das einlesen */
    var ul = createEl('ul');
    var li = createEl('li', 'Test');
    ul.appendChild(li);
    fragment.appendChild(ul);

    var a = createEl('a', 'KLICK MICH');
    a.setAttribute('href', '#');
    a.addEventListener('click', function () {
      alert('Hello');
    });

    fragment.appendChild(a);
    /* *1* Im Browser wird effektiv nur einmal eingefügt! */
    container.appendChild(fragment);

    // var html = '<h2>' + article[0].headline + '</h2>';
    // container.appendChild(fragmentFromString(html));
  });
})(window, document);

///////////////////////////////////////////////////////////
/* Lernskript DOM-Knoten */
///////////////////////////////////////////////////////////
/* 1 */
/* Abgefragt über nodeType:
1	ELEMENT_NODE
3	TEXT_NODE
8	COMMENT_NODE
4	CDATA_SECTION_NODE

7	PROCESSING_INSTRUCTION_NODE
9	DOCUMENT_NODE
10	DOCUMENT_TYPE_NODE
11	DOCUMENT_FRAGMENT_NODE

5	ENTITY_REFERENCE_NODE           !Deprecated 
6	ENTITY_NODE                     !Deprecated 
2	ATTRIBUTE_NODE                  !Deprecated 
12	NOTATION_NODE                 !Deprecated 

https://www.w3schools.com/jsref/prop_node_nodetype.asp
https://developer.mozilla.org/de/docs/Web/API/Node/nodeType


element.nodeType gibt den Knotentyp als Zahl zurück.
*/

/* 2 */
/* childNodes: Alle Kindknoten eins Elements unabhängig vom Typ innerhalb einer Nodelist. 

https://developer.mozilla.org/de/docs/Web/API/Node/childNodes
*/

/* 3 */
/* children: Nur die Kindknoten vom Typ Element (1	ELEMENT_NODE) 

https://developer.mozilla.org/de/docs/Web/API/ParentNode/children
*/

/* 4 */
/* 
1	ELEMENT_NODE
Ein Elementknoten wie z.B. ein DIV

2	ATTRIBUTE_NODE
Ein Attribut eines Elements. !Deprecated - Veraltet

3	TEXT_NODE
Der Text innerhalb eines Elements
*/

/* 5 */
/* 
element.childNodes[0]
element.firstChild
*/

/* 6 */
/* document.createTextNode() 
https://developer.mozilla.org/de/docs/Web/API/Document/createTextNode 
*/

/* 7 */
/* document.createElement() 
https://developer.mozilla.org/de/docs/Web/API/Document/createElement
*/

/* 8 */
/* 
appendChild 
https://developer.mozilla.org/de/docs/Web/API/Node/appendChild

insertBefore
https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore
*/

/* 9 */
/* setAttribute 
https://developer.mozilla.org/de/docs/Web/API/Element/setAttribute

Alternativ:
element.attributename = 'attributwert'
*/

/* 10 */
/* getAttribute 
https://developer.mozilla.org/en-US/docs/Web/API/Element/getAttribute
element.attributename
 */

/* 11 */
/* Der Knoten wird automatisch an seiner ursprünglichen Position entfernt. Ein Knoten kann immer nur an einer Stelle im DOM vorhanden sein. */

/* 12 */
/* cloneNode 
https://developer.mozilla.org/de/docs/Web/API/Node/cloneNode 
*/

/* 13 */
/* replaceWith
https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/replaceWith  */

/* 14 */
/* remove
https://developer.mozilla.org/de/docs/Web/API/ChildNode/remove

removeChild
https://developer.mozilla.org/de/docs/Web/API/Node/removeChild
*/

/* 15 */
/* Siehe helpers.js Funktion <emptyElement> */
