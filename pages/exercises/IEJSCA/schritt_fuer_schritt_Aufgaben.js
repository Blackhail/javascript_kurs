//////////////////////////////////////////////////////////////////////////
'use strict'; 
console.log('test');     
/* 1 
      Ausgaben im Dokument, legen Sie dazu einen div#ausgabe an den sich nachträglich mit JavaScript befüllen.

      a. Geben Sie alle Tiere des Array animals in einer ungeordneten Liste aus. 
      b. Geben Sie nur die Namen der Tiere in einer weiteren ungeordneten Liste aus.  
      */

      var animals = [{
          name: 'fluffy',
          species: 'rabbit'
        },
        {
          name: 'caro',
          species: 'cat'
        }, {
          name: 'Jimmy',
          species: 'fish'
        }, {
          name: 'Heinrich',
          species: 'elkdog'
        }
      ];
      console.log(animals);

      var html = '';
      console.log(html);
      // var ul = document.createElement('ul');
      // console.log(ul);
      var len = animals.length;
      console.log(len);
      var html = document.getElementById('ausgabe');
      console.log(html);

      function createList(data) { 
        var ul = document.createElement('ul');

        for(var key in data) {
          ul.appendChild(createEl('li', key + ': ' + data[key]));
        }
      }
      console.log(ul);
      
 

      //////////////////////////////////////////////////////////////////////////
      /* 2
      Ausgaben im Dokument, legen Sie dazu einen div#ausgabe an den Sie nachträglich mit JavaScript befüllen.

      a. Geben Sie alle Namen und die zugehörige Bestellung im Dokument nach folgenden Schema aus.

      <h2>Jan Waffles</h2>
      <ul>
        <li>name: waffle maker</li>
        <li>price: 80</li>
        <li>quantity: 2</li>
      </ul>
      */
      var order = {
        'Jan Waffles': [{
          name: 'waffle maker',
          price: '80',
          quantity: '2'
        }, {
          name: 'blender',
          price: '200',
          quantity: '1'
        }, {
          name: 'knife',
          price: '10',
          quantity: '4'
        }],
        'Jana Smith': [{
          name: 'waffle maker',
          price: '80',
          quantity: '1'
        }, {
          name: 'knife',
          price: '10',
          quantity: '2'
        }, {
          name: 'pot',
          price: '20',
          quantity: '3'
        }]
      };
      //////////////////////////////////////////////////////////////////////////
      /* 3
      Ausgaben im Dokument, legen Sie dazu einen div#ausgabe an, den Sie nachträglich mit JavaScript befüllen.

      a. Erzeugen Sie im HTML-Dokument eine Liste aus den Eigenschaftsnamen des Objekts order.
      
      <h2>Kunden</h2>
      <ul id="customers">
        <li><a href="#">Jan Waffles</a></li>
        <li><a href="#">Jana Smith</a></li>
      </ul>

      b. Binden Sie an den Hyperlinks in der Liste einen Eventlistener (click).
      Wenn ein Name angeklickt wird geben Sie die zugehörige Bestellung als ungeordnete Liste im div#ausgabe aus.

      Jan Waffles wurde geklickt: 

      <h2>Jan Waffles</h2>
      <ul>
        <li>name: waffle maker</li>
        <li>price: 80</li>
        <li>quantity: 2</li>
      </ul>
      */

      //////////////////////////////////////////////////////////////////////////
      /* 4
      a. Erstellen Sie aus dem Objekt order eine JSON-Datei. 
      b. Erzeugen Sie im HTML-Dokument eine Liste aus den Eigenschaftsnamen des Objekts order.

      <h2>Kunden</h2>
      <ul id="customers">
        <li><a href="#">Jan Waffles</a></li>
        <li><a href="#">Jana Smith</a></li>
      </ul>

      c. Binden Sie an den Hyperlinks in der Liste einen Eventlistener (click).
      Wenn ein Name angeklickt wird, fordern sie die in Aufgabe 4.a erzeugte JSON-Datei an und geben Sie die zugehörige Bestellung als ungeordnete Liste im div#ausgabe aus.

      Jan Waffles wurde geklickt: 
      
      <h2>Jan Waffles</h2>
      <ul>
        <li>name: waffle maker</li>
        <li>price: 80</li>
        <li>quantity: 2</li>
      </ul>
      */
