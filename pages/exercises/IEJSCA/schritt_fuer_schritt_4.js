(function (window, document) {
    'use strict';

    /////// Variablen //////////////////////////////////////////////////
    // Object liegt in order.json


    /////// Funktionen //////////////////////////////////////////////////

    function createLinks(object) {
        // Linkliste Customers Name
        var div = document.getElementById('orders');
        var ul = document.createElement('ul');
    
        for (var key in order) {
            var li = document.createElement('li');
            var a = document.createElement('a');
            a.href = "#";
            ul.appendChild(li);
            a.appendChild(document.createTextNode(key));  
            li.appendChild(a);  
        }
        div.appendChild(ul);
    }

    function handleClick(e) {
        // Bestelliste des customers erzeugen // bei link-Wechsel oder zweitem click löschen // unter link customer positionieren
        var clickedButton = this;
        ajaxGETJSON('../../../lib/data/order.json' , function (response) {
            handleRequest(response, clickedButton);  
        })
    }

    /////// ajaxGETJSON ///////////////////////////////////////////////
    // FN liegt in helpers - Zugriff darüber
    // ajaxGetJSON(../../../lib/data/order.json, fn)


    /////// handleRequest ///////////////////////////////////////////////

    function handleRequest(order , thisPointer) {
        var customerList = createLinks(thisPointer , order);
        createLinks(customerList);
        handleClick(e);
    }

    /////// bindHandles ///////////////////////////////////////////////




    /////// EventListener ///////////////////////////////////////////////


    window.addEventListener('load', function () {
        // Der Code der auf das DOM zugreift hier

    });


})(window, document);




