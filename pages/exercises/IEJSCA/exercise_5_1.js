(function (window, document) {

    'use strict';
    // Funktionen und Variablen deklarieren
    function toggleContainerDisplay() {
      document.getElementById('container').classList.toggle('hide');
    }
  
    function showNewEntryAndFocus(e){
      toggleContainerDisplay();
      document.getElementById('input').focus();
    }
  
    function createAndAppendLi(value) {
      var target = document.getElementById('todo');
      var li = document.createElement('li');
      var txt = document.createTextNode(value);
      li.appendChild(txt);
      li.classList.add('todo');
      target.insertBefore(li, target.lastElementChild);
    }
  
    function createNewEntry(e){
      e.preventDefault(); //=> Standardverhalten (submit) verhindern
      /* this ist das Formular an dem der submit-EventListener gebunden wurde. Jedes Formularelement ist über die DOT-Notation als Eigenschaft im Form-Objekt vorhanden.
      Der Eigenschaftsname ist das name- und/oder id-Attribut. 
      < input type = "text"
      name = "input"
      id = "input" >
  
      this.input
      */
      var value = this.input.value.trim();
      if(value !== '') {
        createAndAppendLi(value);
        this.input.value = '';
      }
      /* Der Container mit dem Formular wird unabhängig davon ob etwas eingetragen wurde wieder ausgeblendet. */
      toggleContainerDisplay();
    }
  
    function moveLiToTOtherList(e){
      /* WENN ein LI angeklickt wird das keinen Wert im ID-Attribut hat.
      Damit wird sichergestellt das nur ein click auf ein LI-Elenment das verschiebben auslöst und es wird verhindert das li#newEntry verschoben wird. */
      if(e.target.nodeName === 'LI' && e.target.id == '') {      
        console.log(this.id);
        switch(this.id){
          case 'todo': 
             var ulDone = document.getElementById('done');
             var li = e.target;
             li.classList.remove('todo');
             li.classList.add('done');
             ulDone.appendChild(li);
          break;
          case 'done': 
             var ulTodo = document.getElementById('todo');
             var li = e.target;
             li.classList.remove('done');
             li.classList.add('todo');
             ulTodo.insertBefore(li, ulTodo.lastElementChild);
          break;
        }
      }
    }
  
    function bindHandler(){
      var newEntry = document.getElementById('newEntry');
      // newEntry.addEventListener('click', toggleContainerDisplay);
      newEntry.addEventListener('click', showNewEntryAndFocus);
      
      // var form = document.getElementById('container').getElementsByTagName('form');
      var form = document.forms[0];
      form.addEventListener('submit', createNewEntry);
  
      /* EventListener wird von ul#todo, ul#done auf alle Kindelement delegiert. */
      var ulTodo = document.getElementById('todo');
      var ulDone = document.getElementById('done');
      ulTodo.addEventListener('click', moveLiToTOtherList);
      ulDone.addEventListener('click', moveLiToTOtherList);
    }
  
  
    window.addEventListener('load', function () {
      // Der Code der auf das DOM zugreift hier
      toggleContainerDisplay();
      bindHandler();
    });
  })(window, document);
  