(function (window, document) {
    'use strict';

/////// Variablen //////////////////////////////////////////////////


/////// Funktionen /////////////////////////////////////////////////


/////// JSON einlesen ////////////////////////////////////////////////

    function handleLoad(e) {

        var xhr = new XMLHttpRequest();  // reicht hier, da ganzes Objekt geholt werden soll
        xhr.open('GET', '../../../lib/data/randomNews.json', true);
        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            var data = JSON.parse(xhr.responseText);
            handleRequest(data);
            
          }
        }
        xhr.send();
      }




/////// handleRequest ////////////////////////////////////////////////

    function handleRequest(article) { // article kann wie schon deklarierte Variable angesprochen werden
        console.log(article);

        var randomArticle = article[Math.floor(Math.random() * (article.length))];
        console.log(randomArticle);
        console.log(randomArticle['h1']);
        createContent();
    }

/////// createContent ////////////////////////////////////////////////

    function createContent(article) {
        var article = document.getElementById('inhalt').getElementsByTagName('section')[0];
        emptyEl(article);
        
        var hgroup = createEl('hgroup');
        hgroup.appendChild(createEl('h2', article.h1));
        hgroup.appendChild(createEl('h1', 'News & Themen'));
        article.appendChild(hgroup);
    
        var p = createEl('p', article.p0);
        article.appendChild(p);
    
        p = createEl('p', article.p1);
        article.appendChild(p);
    
        p.appendChild(createEl('br'));
    
        var a = createEl('a', 'mehr');
        a.setAttribute('href', '#');
        a.setAttribute('title', article.atitle);
        p.appendChild(a);
    
      }

        
    



/////// EventListener ///////////////////////////////////////////////
    
    window.addEventListener('load', function () {
        handleLoad();
    });


})(window, document);




