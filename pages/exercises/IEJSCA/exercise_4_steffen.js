(function (window, document) {
    'use strict';
    // Funktionen und Variablen deklarieren
    function toggleContainer(){
      document
      .getElementById('container')
      .classList.toggle('hide');
    }
  
    /* moveToDone und moveToTodo können in einer FN zusammengefasst werden. */
    function moveToDone(e){
      var clickedLi = this; // e.target
      var target = document.getElementById('done');
      /* Klassen austauschen */
      clickedLi.classList.remove('todo')
      clickedLi.classList.add('done');
      /* Eventlistener austauschen */
      clickedLi.removeEventListener('click', moveToDone);
      clickedLi.addEventListener('click', moveToTodo);
      target.appendChild(clickedLi);
    }
  
    function moveToTodo(e) {
      var clickedLi = this; // e.target
      var target = document.getElementById('todo');
      /* Klassen austauschen */
      clickedLi.classList.remove('done')
      clickedLi.classList.add('todo');
      /* Eventlistener austauschen */
      clickedLi.removeEventListener('click', moveToTodo);
      clickedLi.addEventListener('click', moveToDone);
      /* target.lastElementChild oder document.getElementById('newEntry') */
      target.insertBefore(clickedLi, target.lastElementChild);
    }
  
    function createNewLi(value) {
      var target = document.getElementById('todo');
      var newLi = document.createElement('li');
      var txt = document.createTextNode(value);
      newLi.appendChild(txt);
      newLi.classList.add('todo');
      newLi.addEventListener('click', moveToDone);
      target.insertBefore(newLi, target.lastElementChild);
    }
    
    function handleSubmit(e){
      e.preventDefault();
      // var value = document.getElementById('input').value.trim();
      /* Beim submit-Event steckt in this das gesamte Formular */
      var value = this.input.value.trim();
      if(value/*  !== '' */) {
        createNewLi(value);
        this.input.value = '';
      }
  
      toggleContainer();
    }
  
    function bindHandler(){
      document
      .getElementById('newEntry')
      .addEventListener('click', toggleContainer);
  
      // document.getElementsByTagName('form')[0]
      document
      .forms[0]
      .addEventListener('submit', handleSubmit);
  
      var todo = document
                .getElementById('todo')
                .getElementsByClassName('todo');
  
      var done = document
                .getElementById('done')
                .getElementsByClassName('done');
  
      for(var i = 0; i < todo.length; i++) {
        todo[i].addEventListener('click', moveToDone);
      }
  
      for (var i = 0; i < done.length; i++) {
        done[i].addEventListener('click', moveToTodo);
      }
    }
  
    window.addEventListener('load', function () {
      // Der Code der auf das DOM zugreift hier
      toggleContainer();
      bindHandler();
    });
  })(window, document);
  