(function (window, document) {
  'use strict';
  // 2
  function changeImage(e) {
    /* Großes Bild in dem das kleine Bild angezeigt werden soll, über dem sich die Maus gerade befindet. */
    var img = document //=> document Objekt
      .getElementById('mainimage') //=> #mainimage
      .getElementsByTagName('img') //=> img (HTMLCollection)
    [0]; //=> erstes img aus HTMLCol
    /* Das kleine Bild über dem sich die Maus gerade befindet. */
    var imgUnderMouse = this; // e.target

    /* Großes Bild src austauschen gegen src von kleinem Bild */
    img.src = imgUnderMouse.src;

    /* Oder in einer Zeile: */
    // document
    //   .getElementById('mainimage')
    //   .getElementsByTagName('img')[0].src 
    //   = this.src;
  }

  function markLinkAsActive(clickedButton) {
    /* ...das Element finden an dem die Klasse .active gesetzt ist. */
    var active = document.getElementsByClassName('active')[0];
    /* WENN: in der Variable active ein anderer Wert als undefined steht. 
    HTMLCollection[0] enhält entweder ein JS-Objekt-Typ-Element (im if true) oder
    den JavaScript Wert für nichts,  undefined (im if false) */
    if (active /* !== false */ ) {
      active.classList.remove('active');
    }
    /* Am angeklickten Element die Klasse active setzten */
    clickedButton.classList.add('active');
  }

  function generateHTML(ssd) {
    var info = document.getElementById('info');
    /////////////////////////////////////////////////////////////////
    /* Jeden Knoten in div#info löschen */
    /* SOLANGE es ein letztes Kind in div#info gibt... */
    while (info.lastChild) {
      /* ...wird dieses Kind entfernt. */
      info.lastChild.remove();
    }
    /* Gibt es keine Kindknoten in div#info ist der Rückgabewert von
      info.lastChild null, die Schleife wird beendet.* /
    /////////////////////////////////////////////////////////////////
    /* Headline erzeugen und einfügen */
    /* DOM-Element-Knoten h2 erzeugen  */
    var h2 = document.createElement('h2');
    /* DOM-Text-Knoten mit Inhalt der Eigenschaft headline erzeugen */
    var txt = document.createTextNode(ssd.headline);
    /* DOM-Text-Knoten einfügen in den DOM-Element-Knoten  */
    h2.appendChild(txt);
    /* Die Kombination von Element- und Textknoten in den div#info als neues letztes Kind einfügen. */
    info.appendChild(h2);
    /////////////////////////////////////////////////////////////////
    /* ul erzeugen*/
    var ul = document.createElement('ul');
    var li;
    /* Alle Eigenschaften von Hand auslesen ist möglich aber nicht modular  */
    // li = document.createElement('li');
    // txt = document.createTextNode('Marke: ' + ssd.Marke);
    // li.appendChild(txt);
    // ul.appendChild(li);
    // li = document.createElement('li');
    // txt = document.createTextNode('Artikelgewicht: ' + ssd.Artikelgewicht);
    // li.appendChild(txt);
    // ul.appendChild(li);
    var value;
    for(var key in ssd) {
      value = ssd[key];
      // console.log(key, value);
      if(key !== 'headline'){
        li = document.createElement('li');
        txt = document.createTextNode(key + ': ' + value);
        li.appendChild(txt);
        ul.appendChild(li);
      }
    }

    /* Die erzeugte ul mit Ihren li-Elemente im div#info einfügen */
    info.appendChild(ul);

    /* Object.entries ist Stand 2020 neu, kommt mit ECMA 2017, und funktioniert nicht in allen Browser. Für Object.entries gibt es einen polyfill, siehe polyfills.js. Da für diesen polyfill object.keys verwendet wird, dieser wiederum funktioniert nicht in allen Browsern. Darum benötigen wir auch für Object.keys den polyfill. */
    var array = Object.entries(ssd);
    var key,value, len = array.length;
    for(var i = 1; i < len; i++) {
      key = array[i][0];
      value = array[i][1];
      console.log(key, value);
    }       
  }

  function findSSD(clickedButton) {
    var buttons = document
      .getElementById('switcher')
      .getElementsByTagName('button');
    /* Position des buttons innerhalb der Sammlung aller buttons entspricht der Indexpositon des Objekts in article das ausgegeben werden soll */
    var index = Array.from(buttons).indexOf(clickedButton);
    return article[index];
  }

  //2
  function handleClick(e) {
    /* In der FN-Referenz handleClick ist this in diesem Fall das angeklickte button-Element */
    // console.log(
    //   'this', this,
    //   'this.classList ', this.classList,
    //   'this.classList.contains(\'active\')', this.classList.contains('active')
    // );
    /* WENN: der angeklickten Button (this) die Klasse .active nicht hat... */
    if (!this.classList.contains('active') /* === false */ ) {
      markLinkAsActive(this);
      var ssd = findSSD(this);
      generateHTML(ssd);
    }
  }

  //1
  function bindClickHandler() {
    var buttons = document
      .getElementById('switcher')
      .getElementsByTagName('button');
    var button, len = buttons.length;
    for (var i = 0; i < len; i++) {
      button = buttons[i];
      button.addEventListener('click', handleClick);
    }
  }

  //1
  function bindMouseHandler() {
    var images = document
      .getElementById('vorschau')
      .getElementsByTagName('img');
    var img, len = images.length;
    for (var i = 0; i < len; i++) {
      img = images[i];
      img.addEventListener('mouseover', changeImage);
    }
  }

  /* load wird ausgelöst sobald die Seite vollständig geladen wurde.
  Dadurch stellen wir sicher das alle HTML-Elemente bereits geladen wurden. */
  window.addEventListener('load', function () {
    // Eventhandler für spätere Interaktion durch den Nutzer
    bindMouseHandler(); //1 
    bindClickHandler(); //1
  });
})(window, document);


/* 
Bildwechsel Vorschaubilder
Wird in der #vorschau die Maus über ein Bild bewegt soll das kleine Bild über dem sich die Maus befindet im Bereich #mainimage im src des IMG-Elements angezeigt werden. Die Anzeige bleibt bestehen bis die Seite neugeladen wird oder die Maus über ein anderes Vorschaubild bewegt wird.


1. Aktionen sobald die Seite geladen wurde:
  Eventhandler an den benötigten Elementen binden.
  Jedem img-Element innerhalb der #vorschau einen mouseover Handler geben, der auf eine FN für die spätere Interaktion referenziert 2.

2. Interaktion mit Benutzer die vorher definiert wurde.
  Aktion die beim auslösen des Events gefeuert werden soll vordefinieren.
  Die FN schreiben die in 1 referenziert wird:
  Das src des Bildes auslesen über dem sich die Maus befindet. Diese Zeichenkette ausgelesen aus dem src in das src-Attribut des Bildes #mainimage img schreiben.

Produktwechsel Daten austauschen

Wird einer der Buttons gedrückt sollen die Informationen der zugehörigen Festplatte aus dem Array article aus der Datei data.js ausgegeben werden.Zusätzlich erhält der Button die Klasse.active. Es darf immer nur ein Button aktiv sein, der aktuell aktive Button soll keine neue Ausgabe erzeugen wenn er erneut gedrückt wird.

1. Aktionen sobald die Seite geladen wurde:
Eventhandler an den benötigten Elementen binden.
Jedem button-Element in der #switcher einen click Handler geben, der auf eine FN für die spätere Interaktion referenziert 2.

2. Prüfen ob das angeklickte button-Element (e.target|this) nicht bereits die Klasse .active hat. Hat das angeklickte button-Element bereits die Klasse .active müssen wir nichts tun, da der Inhalt bereis angezeigt wird.
Wurde ein anderes, nicht mit Klasse .active markiertes Element angeklickt, entfernen wir an dem vorher mit .active markierten Element die Klasse .active. und setzen diese am aktuell angeklickten Element.

Ermitteln welche Inhalte aus dem externen Array >article< ausgegeben werden müssen. 

a. Konstrollstruktur if/switch die über ID-Attribut entscheidet was ausgegeben werden soll. 
Nachteil: Nicht modular.

switch(id){
  case 'ssd1' : //dostuff; break;
  case 'ssd2' : //dostuff; break;
  case 'ssd3' : //dostuff; break;
}

b. Indexposition des Buttons innerhalb der Sammlung von buttons ermitteln:
Die Position des Buttons im HTML - Dokument stimmt mit der Position der Daten im Array >article< überein.
button[id = ssd1] { 250GB } => Index 0 => article[0]
button[id = ssd2] { 500 GB } => Index 1 => article[1]
button[id = ssd1] { 1 TB } => Index 2 => article[2]

c. ID-Attribut des angeklickten button-Elements.
button[id = ssd1] => 'ssd1'.split('ssd')[1] -1 //=> 0
button[id = ssd2] => 'ssd2'.split('ssd')[1] -1 //=> 1
button[id = ssd3] => 'ssd3'.split('ssd')[1] -1 //=> 1
TIPP: Im HTML könnte man die IDs direkt bei 0 beginnen.
ssd0,ssd1,ssd2

d. Von angeklickten Button das innerHTML, innerText bzw. den Textcontent mit eine value aus den Objekten vergleichen.

Schleife läuft über Array jedes Objekt auslesen und liest die Eigenschaften, 'Modell / Serie' oder 'Größe Festplatte' für einen Vergleich mit dem innerHTML aus. 

Ideal wäre es die Modelnummer als Attribut im HTML-Element zu speichern.
Bei dieser Variante dürfen nicht zwei Festplatten mit der gleichen Größe vorhanden sein.


e. Wenn die Daten aus einer Datenbank geliefert werden haben bereits alle Element ein eindeutige ID, diese kann im HTML-Element als ID-Attribut gesetzt werden.


Nach dem wir ermitteln haben welches Objekt ausgegeben werden muss, sollte man dieses in einer Variable speichern.

div#info leeren.

Neue Ausgabe aus dem vorher gespeicherten Objekt aufbauen und im Dokuemnt ausgeben.

*/





///////////////////////////////////////////////////////////
/* Lernskript DOM-Knoten */
///////////////////////////////////////////////////////////
/* 1 */
/* Abgefragt über nodeType:
1	ELEMENT_NODE
3	TEXT_NODE
8	COMMENT_NODE
4	CDATA_SECTION_NODE

7	PROCESSING_INSTRUCTION_NODE
9	DOCUMENT_NODE
10	DOCUMENT_TYPE_NODE
11	DOCUMENT_FRAGMENT_NODE

5	ENTITY_REFERENCE_NODE           !Deprecated 
6	ENTITY_NODE                     !Deprecated 
2	ATTRIBUTE_NODE                  !Deprecated 
12	NOTATION_NODE                 !Deprecated 

https://www.w3schools.com/jsref/prop_node_nodetype.asp
https://developer.mozilla.org/de/docs/Web/API/Node/nodeType


element.nodeType gibt den Knotentyp als Zahl zurück.
*/

/* 2 */
/* childNodes: Alle Kindknoten eins Elements unabhängig vom Typ innerhalb einer Nodelist. 

https://developer.mozilla.org/de/docs/Web/API/Node/childNodes
*/

/* 3 */
/* children: Nur die Kindknoten vom Typ Element (1	ELEMENT_NODE) 

https://developer.mozilla.org/de/docs/Web/API/ParentNode/children
*/

/* 4 */
/* 
1	ELEMENT_NODE
Ein Elementknoten wie z.B. ein DIV

2	ATTRIBUTE_NODE
Ein Attribut eines Elements. !Deprecated - Veraltet

3	TEXT_NODE
Der Text innerhalb eines Elements
*/

/* 5 */
/* 
element.childNodes[0]
element.firstChild
*/

/* 6 */
/* document.createTextNode() 
https://developer.mozilla.org/de/docs/Web/API/Document/createTextNode 
*/

/* 7 */
/* document.createElement() 
https://developer.mozilla.org/de/docs/Web/API/Document/createElement
*/

/* 8 */
/* 
appendChild 
https://developer.mozilla.org/de/docs/Web/API/Node/appendChild

insertBefore
https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore
*/

/* 9 */
/* setAttribute 
https://developer.mozilla.org/de/docs/Web/API/Element/setAttribute

Alternativ:
element.attributename = 'attributwert'
*/

/* 10 */
/* getAttribute 
https://developer.mozilla.org/en-US/docs/Web/API/Element/getAttribute
element.attributename
 */

/* 11 */
/* Der Knoten wird automatisch an seiner ursprünglichen Position entfernt. Ein Knoten kann immer nur an einer Stelle im DOM vorhanden sein. */

/* 12 */
/* cloneNode 
https://developer.mozilla.org/de/docs/Web/API/Node/cloneNode 
*/

/* 13 */
/* replaceWith
https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/replaceWith  */

/* 14 */
/* remove
https://developer.mozilla.org/de/docs/Web/API/ChildNode/remove

removeChild
https://developer.mozilla.org/de/docs/Web/API/Node/removeChild
*/

/* 15 */
/* Siehe helpers.js Funktion <emptyElement> */