(function (window, document) {
    'use strict';
  
    function toggleContainerDisplay() {
      document.getElementById('container').classList.toggle('hide');
    }
  
    function showNewEntryAndFocus(e) {
      toggleContainerDisplay();
      document.getElementById('input').focus();
    }
  
    function createAndAppendLi(value) {
      var li = document.createElement('li');
      var txt = document.createTextNode(value);
      li.appendChild(txt);
      li.classList.add('todo');
      ulTodo.appendChild(li);
    }
  
    function createNewEntry(e) {
      e.preventDefault();
      var value = this.input.value.trim();
      if (value !== '') {
        createAndAppendLi(value);
        this.input.value = '';
      }
      toggleContainerDisplay();
    }
  
    function moveLiToTOtherList(e) {
      if (e.target.nodeName === 'LI') {
        var classToRemove = 'done';
        var classToSet = 'todo';
        var target = ulTodo;
  
        if (this.id === 'todo') {
          classToRemove = 'todo';
          classToSet = 'done';
          target = ulDone;
        }
  
        var li = e.target;
        li.classList.remove(classToRemove);
        li.classList.add(classToSet);
        target.appendChild(li);
  
      }
    }
  
    function bindHandler() {
      newEntry.addEventListener('click', showNewEntryAndFocus);
      form.addEventListener('submit', createNewEntry);
      ulTodo.addEventListener('click', moveLiToTOtherList);
      ulDone.addEventListener('click', moveLiToTOtherList);
    }
  
    /* Variablen die im Gültigkeitsbereich der IIFE überall verfügbar sind. */
    var newEntry;
    var form;
    var ulTodo;
    var ulDone;
  
    window.addEventListener('load', function () {
      /* Seite vollständig gelden, DOM ist verfügbar */
      newEntry = document.getElementById('newEntry');
      form = document.forms[0];
      ulTodo = document.getElementById('todo');
      ulDone = document.getElementById('done');
      toggleContainerDisplay();
      bindHandler();
    });
  })(window, document);