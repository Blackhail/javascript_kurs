(function (window, document) {
    'use strict';



 

 /////// changeContent ////////////////////////////////////////////////

 function changeContent(vcard, clickedEl) {  // für click auf card this verwenden, nicht e.target
    console.log(vcard);
    console.log(clickedEl);
    var overview = document.getElementById('info');
    var img = document.getElementById('info').getElementsByTagName('img')[0];
    var name = document.getElementById('info').getElementsByTagName('span')[0];
    console.log(name);
    name.textContent = vcard['name'];
    var position = document.getElementById('info').getElementsByTagName('p')[0];
    console.log(position);
    var email= document.getElementById('info').getElementsByTagName('p')[1];
    console.log(email);
    var name = clickedEl.getElementsByTagName('b')[0].textContent;

    for (var i = 0 ; i < vcard.length ; i++) {
        var person = vcard[i];
        console.log(person.first_name + ' ' + person.last_name);
        var fullname = person.first_name + ' ' + person.last_name;
        console.log(fullname, name);
        // console.log(person['id']);
        // var j = 0;
        if (name == (person.first_name + ' ' + person.last_name) ) {
            
            console.log('passt');
            if (person['gender'] === 'Male') {
                img.src = '../../../lib/img/avatar/jon_doe.png';
            } else {
                img.src = '../../../lib/img/avatar/jane_doe.png';
            }
            var name = document.getElementById('info').getElementsByTagName('span')[0];
            name.textContent = person['name'];
        }
        // name.textContent = person['name'];
    }

    // img.src;
    // name.textContent;
    // position.textContent;
    // email.textContent;

  }

    /////// handleRequest ////////////////////////////////////////////////

    function handleRequest(data) {  // für click auf card this verwenden, nicht e.target

        var clicked = this;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', '../../../lib/data/vcard.json', true);
        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            var data = JSON.parse(xhr.responseText);
            changeContent(data, clicked);
          }
        }
        xhr.send();
    }


    /////// bindHandles ////////////////////////////////////////////////


    function bindHandle() {
        var overview = document.getElementById('overview').getElementsByClassName('card');
        var len = overview.length;
        console.log(overview);
        
        for(var i = 0 ; i < len ; i++) {
            var link = overview[i];
            link .addEventListener('click' , handleRequest);
        }   
    }


    /////// EventListener ///////////////////////////////////////////////
    
    window.addEventListener('load', function () {
        bindHandle();
      
    });


})(window, document);




