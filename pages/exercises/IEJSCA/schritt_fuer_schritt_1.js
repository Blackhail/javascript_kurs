(function (window, document) {
    'use strict';

/////// Funktionen und Variablen deklarieren //////////////////////////
  
    var animals = [{
      name: 'fluffy',
      species: 'rabbit'
    },
    {
      name: 'caro',
      species: 'cat'
    }, {
      name: 'Jimmy',
      species: 'fish'
    }, {
      name: 'Heinrich',
      species: 'elkdog'
    }
  ];
  console.log(animals);

  var len = animals.length;

  for(var i = 0; i < len ; i++) {   // Testschleife für Objekt-Array //
  console.log(animals[i]['species']);
  }

/////// Funktion //////////////////////////////////////////////////////

  function createList(data) {
    var div = document.getElementById('ausgabe'); // var div kann erst eingelesen werden, wenn FN geladen ist, deshalb hier + in eventlistener
    var ul = document.createElement('ul');
    for(var i = 0 ; i < len ; i++) {
      ul.appendChild(createEl('li', 'a ' + data[i]['species'] + ' called ' + data[i]['name'])); // li anlegen und in ul einfügen (createEL in helpers.js)
    }
    div.appendChild(createEl('h2' , 'animal species:'));
    div.appendChild(ul);
  }

//////// Variante FOR IN Schleife - Key in Data //////////////////////
//////// nicht für arrays verwenden!!!!!! ////////

    // function createList(data) { 
    //   var div = document.getElementById('ausgabe');
    //   var ul = document.createElement('ul');
    //   for(var key in data) {
    //     ul.appendChild(createEl('li', data['key']));
    //   console.log(key + data[key]);
    //   } 
    //   div.appendChild(ul);
    // }

  /////// EventListener ///////////////////////////////////////////////

  
  window.addEventListener('load', function () {
    // Der Code der auf das DOM zugreift hier
    createList(animals);
  });


  })(window, document);
  



