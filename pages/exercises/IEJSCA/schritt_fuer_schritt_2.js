(function (window, document) {
    'use strict';

/////// Funktionen und Variablen deklarieren //////////////////////////
  
    var animals = [{
      name: 'fluffy',
      species: 'rabbit'
    },
    {
      name: 'caro',
      species: 'cat'
    }, {
      name: 'Jimmy',
      species: 'fish'
    }, {
      name: 'Heinrich',
      species: 'elkdog'
    }
  ];
  console.log(animals);

  var len = animals.length;

  for(var i = 0; i < len ; i++) {   // Testschleife für Objekt-Array // kann wieder raus
  console.log(animals[i]['name']); // Testschleife für Objekt-Array // kann wieder raus
  }

/////// Funktion //////////////////////////////////////////////////////

  function createList(data) {
    var div = document.getElementById('ausgabe');
    var ul = document.createElement('ul');

    for (var i = 0 ; i < len ; i++) {
        ul.appendChild(createEl('li' , animals[i]['name']));
    }
    div.appendChild(createEl('h2' , 'animals name:'));
    div.appendChild(ul);
  }


  /////// EventListener ///////////////////////////////////////////////

  
  window.addEventListener('load', function () {
    // Der Code der auf das DOM zugreift hier
    createList(animals);
  });


  })(window, document);
  



