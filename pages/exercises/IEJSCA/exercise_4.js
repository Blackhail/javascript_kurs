(function (window, document) {
    'use strict';
    
    function hideContainer() {
        document.getElementById('container').className = 'hide';
    }

    function handleClick () {
        document.getElementById('container').className = '';
    }

    function createTodo (form) {
        var input = document.getElementById('input').value.trim();
     if (input !== '') {
            var ulTodo = document.getElementById('todo'); // var ul#todo definieren
            var li = document.createElement('li'); // neues Listenelement
            var liText = document.createTextNode(input); // neuer textknoten
            li.appendChild(liText);
            ulTodo.insertBefore(li, ulTodo.lastElementChild);
        }
        hidecontainer();
    }

    function handleSubmit(e) {
        e.preventDefault();
        createTodo(e.target);
      }

    function bindSubmitHandler () {
        var form = document.getElementById('container').getElementsByTagName('form')[0];
        form.addEventListener('submit' , createTodo )
    }
    

    function bindClickHandler () {
        var button = document.getElementById('newEntry');
        button.addEventListener('click' , handleClick);
    }

    function moveTodo (e) {
        var doneBox = document.getElementById('done');

    }


    

    
    
    
    window.addEventListener('load', function () {
    // Eventhandler für spätere Interaktion durch den Nutzer
    hideContainer();
    bindClickHandler();
    bindSubmitHandler();
    });



  })(window, document);
  

