(function (window, document) {
    'use strict';

/////// Variablen //////////////////////////////////////////////////

var order = {
    'Jan Waffles': [{
        name: 'waffle maker',
        price: '80',
        quantity: '2'
    }, {
        name: 'blender',
        price: '200',
        quantity: '1'
    }, {
        name: 'knife',
        price: '10',
        quantity: '4'
    }],
    'Jana Smith': [{
        name: 'waffle maker',
        price: '80',
        quantity: '1'
    }, {
        name: 'knife',
        price: '10',
        quantity: '2'
    }, {
        name: 'pot',
        price: '20',
        quantity: '3'
    }]
};
    console.log(order);
    var len = order.length;

/////// Funktionen //////////////////////////////////////////////////
// Zugriff auf object - FOR IN
// Zugriff auf array - FOR, FOREACH usw. ...

function createOrderList() {

    var div = document.getElementById('ausgabe');
    var ul = document.createElement('ul');
    
    for (var key in order) {
        var len2 = order[key].length;
        console.log(order[key]);
        ul.appendChild(createEl('h2' , key))
            for (var i = 0 ; i < len2 ; i++) {     
                for (var key2 in order[key][i]) {
                    console.log(order[key][i][key2]);                  
                    ul.appendChild(createEl('li' , key2 + order[key][i][key2]))
            }
            div.appendChild(ul);
        }
    }
}

function createCustomersLinkList() {

    var div = document.getElementById('linklist');
    var ul = document.createElement('ul');
    var h2 = document.createElement('h2');
    h2.appendChild(document.createTextNode('Aufgabe 3'));
    div.appendChild(h2);

    for (var key in order) {
        var li = document.createElement('li');
        var a = document.createElement('a');
        a.href = "#";
        ul.appendChild(li);
        a.appendChild(document.createTextNode(key));  
        li.appendChild(a);  
    }
    div.appendChild(ul);
}

// function createCustumersOrderList() {
//     var div = document.getElementById('linklist');

// }


/////// EventHandler ///////////////////////////////////////////////

function handleClick(e) {  // hier Ausgabe generieren oder auf FN verweisen, falls ausgelagert
var clickedButton = this;
console.log (order[this.innerHTML]); // hier kommt man an das array des geclickten Namens
var customerList = order[this.innerHTML];
var len = customerList.length;
var div = document.getElementById('linklist');

// hier noch Liste leeren
var productlist = document.getElementById('productlist');
    if (productlist) {
        productlist.remove();
    }
var parent = document.createElement('div');
parent.id = 'productlist';
div.appendChild(parent);

    for(var i = 0 ; i < len ; i++) {        
    var ul = document.createElement('ul');
    var product = customerList[i];   
    console.log(product);
        for(var key in product) {
            console.log(product[key]);
            ul.appendChild(createEl('li' , key + ': ' + product[key]));
        }
    parent.appendChild(ul);
    }
    


}

/////// BindHandler ///////////////////////////////////////////////

function bindHandler() {
  var customersClick = document.getElementById('linklist').getElementsByTagName('a');
  console.log(customersClick);
  var button;
  var len = customersClick.length;
  console.log(len);
  for(var i = 0 ; i < len ; i++) {
    button = customersClick[i];
    console.log(button);
    button.addEventListener('click', handleClick);
    }
}

/////// EventListener ///////////////////////////////////////////////

window.addEventListener('load', function () {
// Der Code der auf das DOM zugreift hier
createOrderList(order);
createCustomersLinkList(order);
bindHandler();
});


})(window, document);
  



