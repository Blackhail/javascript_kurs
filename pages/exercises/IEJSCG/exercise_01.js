'use strict';
/* 

https://en.wikipedia.org/wiki/ECMAScript
https://www.ecma-international.org/publications/standards/Ecma-262.htm


Lernskript Speicherung von Werten

1. 
Aktivierung durch 'use strict' am Anfang des JS-Codes. 
Fehlerkorrektur wird deaktiviert, Syntax wird strikt (genauer) geprüft.

Bisher still ignorierte Fehler führen zum Skriptabbruch.

Behebt Fehler, die Optimierungen durch JavaScript-Interpreter erschweren oder verhindern. Code im strikten Modus kann manchmal schneller ausgeführt werden als identischer Code im nicht-strikten Modus.

Verbietet Syntax, die in zukünftigen Versionen von ECMAScript wahrscheinlich definiert werden könnten.
Zukünftig reservierte Wörter: package, privat, public, protected, static, yield.


2.
https://www.mediaevent.de/javascript/strict-mode.html

Keine impliziten globalen Variablen in Funktionen

apply und call fallen nicht per default an das globale Objekt

with gibt es nicht mehr

arguments.caller oder arguments.callee ist versenkt

Doppelte Namen erzeugen einen Syntax-Fehler

Hinweg mit oktalen Literals (let num = 013 führt zu einem Syntaxfehler: Decimal integer literals with a leading zero are forbidden in strict mode)

3.
Regelsystem:
Syntax im Programm-Code. Beim Programmieren legt die Syntax formale Regeln über die zulässigen Sprachelemente einer Programmiersprache fest.

4. 
Groß- und Kleinschreibung vertauscht.
Semikolon vergessen 
Leerzeichen und Zeilenumbrüche an der falschen Stelle
Fehlende schließende runde geschweifte Klammer
Kommentare vertauscht, z.B. HTML Kommentar in CSS oder JS
Bindestrich im Variablennamen
Zuweisung vertauscht statt a = 12 , 12 = a


Fehlen schließende runde oder geschweifte Klammern?

Fehlt ein schließendes Hochkamma oder wird ein doppeltes Hochkomma durch ein einfaches Hochkomma geschlossen?

Klammer zuviel im Script-Code?

Bindestrich im Variablennamen? Javascript mag keine Bindestriche.

Groß und Kleinschreibung beachtet? Hier ist Javascript tatsächlich empfindlich

Schreibfehler in einer der DOM-Methoden? Vor allem getElementById macht schnell Ärger.


ASI (Automatic Semicolon Inseration)
http://www.ecma-international.org/ecma-262/7.0/index.html#sec-rules-of-automatic-semicolon-insertion

5.
*/

/* 6 */
var string; //=> undefined
string = string + 'Hello';
console.log(string);

/* Deklaration und Initialisiuerng. 
Durch Zuweisung der leeren Zeichenkette ist string2 jetzt mit dem Datentyp string vorbelegt. */
var string2 = '';
string2 = string2 + 'Hello';
console.log(string2);

/* 6 */
var number;
number = number + 5; //=>  NaN
console.log(number);
console.log(typeof number);

var number2 = 0;
number2 = number2 + 5;
console.log(number2);
console.log(typeof number2);

var number3 = 0;
number3 = number3 / 0; //=> NaN
console.log(number3);
console.log(typeof number3);

console.log('schwein' / 'metzger');
console.log('schwein' + 'metzger');

/* 7 
https://www.mediaevent.de/javascript/variablen-identifier.html

 Er kann aus Groß- und Kleinbuchstaben sowie aus Ziffern und dem Unterstrich _
bestehen.
 Am Anfang des Namens darf keine Ziffer stehen.
 Sonderzeichen wie z. B. die deutschen Umlaute ä, ö, ü, Ä, Ö, Ü, das ß oder ein Leerzeichen
im Namen sind nicht erlaubt.
 Der Name darf nicht einem der Schlüsselwörter von JavaScript entsprechen. Eine
Liste der Schlüsselwörter finden Sie im Anhang.
 Zwei verbreitete Konventionen:
 Der Name einer Variablen sollte etwas über den Inhalt aussagen. Falls also ein Nachname
gespeichert werden soll, so sollten Sie die Variable auch nachname nennen.
 Längere Namen für Variablen sind besonders selbsterklärend. Dabei hat sich die sogenannte
camelCase-Schreibweise eingebürgert: Der Name beginnt mit einem Kleinbuchstaben,
jedes neue Wort beginnt mit einem Großbuchstaben, z. B. buttonStart
und buttonStop.

Reserved Words

abstract
boolean
break
byte
case
catch
char
class
const
continue
debugger
default
delete
do
double
else
enum
export
extends
false
final
finally
float
for
function
goto
if
implements
import
in
instanceof
int
interface
long
let
native
new
null
package
private
protected
public
return
short
static
super
switch
synchronized
this
throw
throws
transient
true
try
typeof
var
void
volatile
while
with
*/

/* 8 */
/* 
window.prompt()
window.alert()
window.confirm()

UI Dialogboxen von JavaScript die ohne vorangestelltes window direkt aufgerufen werden können.

prompt('Boxbeschriftung', 'Vorbelegung für Eingabe');
alert('Der Text der angezeigt werden soll');
confirm('Sind Sie sicher?');

Darstellung und Text der vordefinierten Buttons kann nicht geändert werden. 
*/

/* 9 
Alle Eingaben aus dem Dokument haben den Datentyp string.
Zum Rechnen muss der Datentyp string in number umgewandelt werden. Aktuell prüfen wir nicht auf Fehleingaben, folgt später. 
*/

/* 10
Bei Operationen mit dem Plusoperator werden Strings verkettet. 
Stringverkettung oder auch Konkatenation. 

var zahl1 = prompt('Zahl1', '50');
var zahl2 = prompt('Zahl2', '50');
var summe = zahl1 + zahl2;
console.log(summe);
*/

var zahl1 = '50'; // prompt('Zahl1', '50');
var zahl2 = '50'; // prompt('Zahl2', '50');
var summe = zahl1 + zahl2;
/* Die Funktionen erzeugen eine Ausgabe in der Konsole, einem UI Fenster oder dem Dokument. Der Rückgabewert wird nicht beachtet. */
console.log(summe);
alert(summe);
document.write(summe);
/* JavaScript wird von innen nach außen ausgeführt. Die Methode/Funktion log gibt den übergebenen Wert in der Konsole aus und hat keinen vordefinierten Rückgabewert. Wurde kein Rückgabewert vordefiniert ist der Defaultwert: undefined */
console.log(console.log(console.log(summe))); //=> undefined

function myFunction() {
  // Hier gibt es kein return, default ist undefined
}
console.log(myFunction()); //=>  undefined

function myOtherFunction() {
  return 'Hier haben wir einen Rückgabewert';
}
console.log(myOtherFunction()); //=>  'Hier...'
/* Erzeugt eine leere Zeile in der Konsole */
console.log(); //=> undefined
/* Erzeugt eine Zeile in der Konsole: 24 */
console.log(12 + 12); //=> undefined
var x = 12 + 30; //=> 42

/* 11 
Wenn die Ziffern am beginn der Zeichenkette stehen wird der Wert in den Datentyp number umgewandelt. Siehe folgendes Beispiel
*/

/* ######################### */
/* Typprüfung */
/* ######################### */
console.log(isNaN(12)); //=> false
console.log(isNaN('12')); //=> false
console.log(isNaN('abc')); //=> true

/* ######################### */
/* Typumwandlung 11 / 12 / 13*/
/* ######################### */

var input = '12.29';
var n = Number(input); //=> 12.29
var n = parseFloat(input); //=> 12.29
var n = parseInt(input); //=> 12
var n = +input; //=> 12.29
var n = input * 1; //=> 12.29

input = 'abc';
var n = Number(input); //=> NaN
var n = parseFloat(input); //=> NaN
var n = parseInt(input); //=> NaN
var n = +input; //=> NaN

var input = 2.99;
var s = String(input); //=> '2.99'
var s = input.toString(); //=> '2.99'
var s = '' + input; //=> '2.99'

input = '12abc';
var n = Number(input); //=> NaN
var n = parseFloat(input); //=> 12
var n = parseInt(input); //=> 12
var n = +input; //=> NaN

var firstname = 'Jon';
var lastname = 'Snow';
console.log('Vorname: ' + firstname + ', Nachname: ' + lastname);
/* ECMA6 in Zukunft wird alles gut! Template-String*/

var templateString = `Vorname: ${firstname}, Nachname: ${lastname}`;
console.log(templateString);

document.write(
  '<img style="width:10%" src="../../../lib/img/nicht_lustig/bild1.jpg" alt="Wirklich nicht lustig!">'
);
/////////////////////////////////////////////////////////////////////
/* Code Exercise 1 - Einstieg in die Grundlagen */
/////////////////////////////////////////////////////////////////////
var a = 13;
var b = 4;
var sum = a + b;
var division = a / b;
var intDivision = parseInt(division); // division - division % 1
var remainder = a % b; // a - intDivision * b

document.write(
  '<h2>Aufgabe 1: Einfache Arithmetik</h2>' +
    a +
    ' + ' +
    b +
    ' = ' +
    sum +
    '<br>' +
    a +
    ' / ' +
    b +
    ' = ' +
    division +
    '<br>' +
    a +
    ' / ' +
    b +
    ' &asymp; ' +
    intDivision +
    '<br>' +
    a +
    ' % ' +
    b +
    ' = ' +
    remainder +
    '<br>'
);

/////////////////////////////////////////////////////////////////////
/* Code Exercise 2 - Einstieg in die Grundlagen */
/////////////////////////////////////////////////////////////////////

// var number1 = parseInt(prompt('Bitte Zahl 1 eingeben', '10'));
// var number2 = parseInt(prompt('Bitte Zahl 2 eingeben', '10'));

/* Praxis: Eingabe als string und dann konvertiert als number jeweils in einer eigenen Variable speichern. */

// Eingabe von Benutzer holen (Datentyp ist string)
var input1 = prompt('Bitte Zahl 1 eingeben', '10');
var input2 = prompt('Bitte Zahl 2 eingeben', '5');

// Eingelesene Werte in Datentyp number umwandeln
var number1 = parseInt(input1);
var number2 = parseInt(input2);

var total = number1 + number2;
var difference = number1 - number2;
var product = number1 * number2;
var quotient = number1 / number2;

document.write(
  '<h2>Aufgabe 2: Eingabe von zwei Zahlen mit prompt verarbeiten</h2>' +
    number1 +
    ' + ' +
    number2 +
    ' = ' +
    total +
    '<br>' +
    number1 +
    ' - ' +
    number2 +
    ' = ' +
    difference +
    '<br>' +
    number1 +
    ' * ' +
    number2 +
    ' = ' +
    product +
    '<br>' +
    number1 +
    ' / ' +
    number2 +
    ' = ' +
    quotient +
    '<br>'
);
/////////////////////////////////////////////////////////////////////
/* Code Exercise 3 - Einstieg in die Grundlagen */
/////////////////////////////////////////////////////////////////////
/* Mit ECMA6 kann const verwendet werden. Aktuell var mit Bezeichner nur in Großbuchstaben. */
const MINUTE = 60;
const HOUR = 60 * MINUTE; //=> 3600
const DAY = 24 * HOUR; //=> 86400

var days;
var hours;
var minutes;
var seconds;
var rest;
/* Benutzer gibt Anzahl Sekunden ein (Eingabe ist immer ein string) */
var input = prompt('Sekunden eingeben', '924220');
/* Eingabe in Zahlenwert (number) umwandeln */
var satseconds = Number(input);

// var satseconds = Number( prompt('Sekunden eingeben', '924220') );
/* JS wird von innen nach außen, in der Reihenfolge der Operatorenwertigkeit ausgeführt. Erst prompt, dann number und am Ende die Zuweisung.
var satseconds = Number( prompt('Sekunden eingeben', '924220') )
var satseconds = Number( '924220' )
var satseconds = 924220
*/

/* satseconds / DAY gibt einen Kommawert zurück, durch parseInt bekommen wir die Anzahl der Tage als Ganzzahl */
days = parseInt(satseconds / DAY);
console.log(days);
/* Der Restwert ermittelt mit Module sind die verbleibenden Sekunden mit denen wir dann die Stunden ermitteln können */
rest = satseconds % DAY;

/* satseconds / HOUR gibt einen Kommawert zurück, durch parseInt bekommen wir die Anzahl der Stunden als Ganzzahl */
hours = parseInt(rest / HOUR);
console.log(hours);
/* Der Restwert ermittelt mit Module sind die verbleibenden Sekunden mit denen wir dann die Minuten ermitteln können */
rest = rest % HOUR;
/* satseconds / MINUTE gibt einen Kommawert zurück, durch parseInt bekommen wir die Anzahl der Minuten als Ganzzahl */
minutes = parseInt(rest / MINUTE);
console.log(minutes);
/* Der Restwert ist die Anzahl der Sekunden */
seconds = rest % MINUTE;

document.write(
  '<h2>Aufgabe 3: Satellitenzeit </h2>' +
    'Anzahl Sekunden: ' +
    satseconds +
    '<br>' +
    'Tage: ' +
    days +
    '<br>' +
    'Stunden: ' +
    hours +
    '<br>' +
    'Minuten: ' +
    minutes +
    '<br>' +
    'Sekunden: ' +
    seconds +
    '<br>'
);
