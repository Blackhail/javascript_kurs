'use strict'

'use strict';
/* 
Lernskript Verschiedene Zweige eines Programms / Operatoren
1.

1.a) 
if(a > b) {}
if(a < b) {}
if(a === b) {}


1.b)
if(a > b) {}
else if(a < b) {}
else {}

1.a) Hier werden IMMER alle if-Anweisungen geprüft. 
WENN 
WENN 
WENN 
WENN 
WENN 

Hier werden alle Bedingung (WENN) geprüft, auch wenn die erste bereits erfüllt war.


1.b) else if wird nur geprüft wenn die Bedingung im if nicht true ergab. else wird nur ausgeführt wenn alle vorherigen Bedingungen nicht true ergaben. 

if
else if
else if
else if
else if
else if
else

WENN 
ANDERENFALLS WENN
ANDERENFALLS WENN
ANDERENFALLS WENN
ANDERENFALLS WENN
ANDERENFALLS WENN
IN ALLEN ANDEREN FÄLLEN

Sobald eine Bedingung (WENN) zutrifft werden die folgenden nicht mehr geprüft. 


Beispiel: Prüfung einer E-Mail-Adresse. 
1. @ Zeichen, 1 muss vorhanden sein
2. Am ende .com, .de usw. 

GEFORDERT: steffen.rech@wbs-training.de 

EINGEGEBEN: steffen.rechwbs-training 

WENN kein @-Zeichen vorhanden
WENN mehr als ein @-Zeichen vorhanden
WENN nicht .com, .de am Ende 

Alle Fehlermeldungen werden gleichzeitig angezeigt. 
kein @-Zeichen vorhanden
nicht .com, .de am Ende 


WENN kein @-Zeichen vorhanden
ANDERENFALLS WENN mehr als ein @-Zeichen vorhanden
ANDERENFALLS WENN nicht .com, .de am Ende 

Fehlermeldungen werden nacheinander angezeigt, immer nur dann wenn der vorherige Fehler korrigiert wurde.

kein @-Zeichen vorhanden


2. == vs === 
Prüfung auf Gleichheit mit ==, prüft nur den Inhalt(Wert/Value). 
Unterscheiden sich die Datentypen wird konvertiert.  
12 == 12 
'12' == 12 

Prüfung auf strikte Gleicheit mit ===, prüft zuerst den Datentyp und nur wenn dieser übereinstimmt auf den Inhalt(Wert/Value). 

MERKE: Immer mit strikler Gleichheit prüfen um unerwartete Konvertierungen zu vermeiden.

3. 
NaN === NaN
NaN == NaN 
Diese Prüfungen werden nicht funktionieren. 

isNaN - is Not a Number 
Der an isNaN übergebene Wert kann nicht in eine gültige Zahl umgewandelt werden. 

Ergebnis:
true: Wenn der Wert nicht in eine gültige Zahl umgewandelt werden kann. 
false: Wenn der Wert in eine gültige Zahl umgewandelt werden kann. 


isNaN()
Hier wird er übergeben Wert konviert und dann geprüft1

Number.isNaN()
Hier wird der Wert direkt geprüft. 
Vorher sollte an dieser Stelle mit Number, parseInt, parseFloat konvertiert werden. 
*/
console.log(isNaN('abc'));
console.log(isNaN('12'));
console.log(isNaN('abc') === NaN);
console.log(12 / 'zehn' === NaN);
console.log(Number(12 / 'zehn') === NaN);
console.log(NaN === NaN);

if (isNaN('12abc')) {
  console.log('Keine Zahl');
} else {
  console.log('Eine Zahl');
}

/* isNaN('12') === false  */
if (!isNaN('12abc')) {
  console.log('Eine Zahl');
} else {
  console.log('Keine Zahl');
}

var input = '12';
var number = parseInt(input); //=> 12

function realNaN(x) {
  x = Number(x);
  return x !== x;
}

var a = NaN;
var b = 'foo';
var c = undefined;
var d = {};
var e = [];
console.log('a !== a', a !== a);
console.log('b !== b', b !== b);
console.log('c !== c', c !== c);
console.log('d !== d', d !== d);
console.log('e !== e', e !== e);

///////////// Aufgaben: ///////////////////////////////////////////////////////////////
// var a = -5;
// var b = -2;
// var c = -6;
// var d = 0;
// var e = -1;

// if (a > b && a > c && a > c && a > d && a > e) {
//   document.write('die größte Zahl ist: ' + a + '<br>');
// } else if (b > a && b > c && b > d && b > e) {
//   document.write('die größte Zahl ist: ' + b + '<br>');
// } else if (c > a && c > b && c > d && c > e) {
//   document.write('die größte Zahl ist: ' + c + '<br>');
// } else if (d > a && d > b && d > c && d > e) {
//   document.write('die größte Zahl ist: ' + d + '<br>');
// } else {
//   document.write('die größte Zahl ist: ' + e + '<br>');
// }

// var fsk;
// var input = prompt('Bitte Alter eingeben' , ''); // prompt('Bitte Alter eingeben')
// var age = Number(input);
// console.log('Alter: ', age);

// if(age >= 18) {
//   console.log('FSK 18');
//   fsk = 'FSK 18';
// } else if (age >= 16) {
//   console.log('FSK 16');
//   fsk = 'FSK 16';
// } else if (age >= 12) {
//   console.log('FSK 12');
//   fsk = 'FSK 12';
// } else if (age >= 6) {
//   console.log('FSK 6');
//   fsk = 'FSK 6';
// } else /* if (age >= 0) */ {
//   console.log('FSK 0');
//   fsk = 'FSK 0';
// }
// document.write(fsk);
// console.log(fsk);


// var dice = rand(1,6);
// // var dice = 1;
// if (dice = 1) {
//   document.write('<img src="../../../lib/img/wuerfel/wuerfel_1.gif">');
// }
// if (dice = 2) {
//   document.write('<img src="../../../lib/img/wuerfel/wuerfel_2.gif">');
// }
// if (dice = 3) {
//   document.write('<img src="../../../lib/img/wuerfel/wuerfel_3.gif">');
// }
// if (dice = 4) {
//   document.write('<img src="../../../lib/img/wuerfel/wuerfel_4.gif">');
// }
// if (dice = 5) {
//   document.write('<img src="../../../lib/img/wuerfel/wuerfel_5.gif">');
// }
// if (dice = 6) {
//   document.write('<img src="../../../lib/img/wuerfel/wuerfel_6.gif">');
// }

////////////////////////////////////////////////////////////////////////////////////

// var min = 1, max = 6;
// function rand (min, max) {
// return Math.floor(Math.random() * (max - min + 1)) + min;
// }
// switch(random) {
//   case 1 : document.write('<img src="../../../lib/img/wuerfel/wuerfel_1.gif">'); break;
//   case 2 : document.write('<img src="../../../lib/img/wuerfel/wuerfel_1.gif">'); break;
//   case 3 : document.write('<img src="../../../lib/img/wuerfel/wuerfel_1.gif">'); break;
//   case 4 : document.write('<img src="../../../lib/img/wuerfel/wuerfel_1.gif">'); break;
//   case 5 : document.write('<img src="../../../lib/img/wuerfel/wuerfel_1.gif">'); break;
//   case 6 : document.write('<img src="../../../lib/img/wuerfel/wuerfel_1.gif">'); break;
// }

///////////////////////////////////////////////////////////////////////////////////
// let min = 1;
// let max = 6;
// let number_dec;
// let number_int;
// number_dec = (Math.random() * (max - min + 1)) + min;

// // gefaked:
// // number_dec = (0.999999999999 * (max - min + 1)) + min;

// document.write(number_dec + '<br>');

// number_int = Math.floor( number_dec );
// document.write(number_int);
document.write('<hr>');
let min = 1;
let max = 6;
let number_dec1;
let number_dec2;
let number_int1;
let number_int2;

number_dec1 = (Math.random() * (max - min + 1)) + min;
document.write(number_dec1 + '<br>');
number_int1 = Math.floor( number_dec1 );
document.write(number_int1);

// number_dec2 = (Math.random() * max) + 1; // wenn min = 1
// document.write(number_dec2 + '<br>');
// number_int2 = Math.floor( number_dec2 );
// document.write(number_int2);


/////////////////////////////////////////////////////////////////////////////////////

function Wuerfeln() {
  /* simuliert das n-malige Würfeln eines idealen Würfels */
  
  n = document.f_tab.e_n.value;
  h = [0, 0, 0, 0, 0, 0];
  
  for (i = 1; i <= n; i++) {
     az = 6*Math.random();
     az = Math.round(az + 0.5);
     h[az-1]++;
  }
  
  document.f_tab.e_h1.value = h[0];
  document.f_tab.e_h2.value = h[1];
  document.f_tab.e_h3.value = h[2];
  document.f_tab.e_h4.value = h[3];
  document.f_tab.e_h5.value = h[4];
  document.f_tab.e_h6.value = h[5];
  
  r = [0, 0, 0, 0, 0, 0];
  for (i = 0; i <= 5; i++) {
     r[i] = Math.round(10000*h[i]/n)/10000;
  }
  
  document.f_tab.e_r1.value = r[0];
  document.f_tab.e_r2.value = r[1];
  document.f_tab.e_r3.value = r[2];
  document.f_tab.e_r4.value = r[3];
  document.f_tab.e_r5.value = r[4];
  document.f_tab.e_r6.value = r[5];
  
  var m = Math.max(r[0],r[1],r[2],r[3]);
  var c = Math.round(246/rmax);
  
  document.f_sln.r1.width = r[0]*c;
  document.f_sln.r2.width = r[1]*c;
  document.f_sln.r3.width = r[2]*c;
  document.f_sln.r4.width = r[3]*c;
  document.f_sln.r5.width = r[4]*c;
  document.f_sln.r6.width = r[5]*c;
  
  }

///////////////////////////////////////////////////////////////////////

