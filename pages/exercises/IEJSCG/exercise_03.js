'use strict';
/////////////////////////////////////////////////////////////////////
/*  Lernskript Schleifen */
/////////////////////////////////////////////////////////////////////
/* 1 */
/* Initialisierung ; Bedingung ; Befehlsfolge */

/* 2 */
/* Mit var ist der Wert nach der Schleife um 1 größer als im letzten Schleifendurchlauf. Hier 11. */
console.log('************ var ************');
for (var j = 1; j < 11; j++) {
  // j <= 10
  console.log(j);
}
console.log('Nach der Schleife: ', j); //=> 11

console.log('************ let ************');

/* Arbeitet man mit let ist die Variable nach der Schleife nicht mehr verfügbar!  */
for (let x = 1; x < 11; x++) {
  // i <= 10
  console.log(x);
}
/*  Uncaught ReferenceError: x is not defined */
// console.log(x);
console.log('x gibt es nach der Schleife nicht!');
/* 3 */
/* Eine Schleife die 4 Durchläufe hat, beginnend mit Zählervariable 0.
Das Komma wird vor jeder Zahl außer der ersten geschrieben. Die Zahl wird in jedem Schleifendurchlauf geschrieben. Die Zeichenkette wird innerhalb der Schleife gesammelt/gebildet und nach der Schleife einmalig ausgegben.
WICHTIG: Bitte keine Ausgabemethoden innerhalb der Schleife verwenden, immer Stringverkettung in Hilfsvariable und nur eine Ausgabe nach der Schleife!*/
var str = '';
for (var i = 0; i < 4; i++) {
  if (i) {
    //=> 0 | 1 | 2 | 3 bzw. false | true | true | true
    str += ', ';
  }
  str += i; // 0 | 1 | 2 | 3
}

document.write(str); //=> '0, 1, 2, 3'

/* 4 */
/* i > 4, i ist 0 Schleife läuft nie los.  */

/* 5 */
/* Endlosschleife, hört nicht auf. Absturz Browser */
var save = 0;
for (var i = 10; i > 4; i++) {
  /* Alternative zu save-Variable. Schleifenzähler Modulo z.B. 1000 für 1000 Durchläufe. Funktioniert nur wenn eine Zählervariable vorhanden ist. */
  console.log(i % 1000, i % 1000 === 0);
  if (save++ > 1000) {
    console.log('Endlosschleife mit break verlassen');
    break;
  }
}

/* 6  */
/* 
Kopfgesteuerte Schleife, Bedingung wird vor dem Schleifendurchlauf geprüft. for bzw. while

Fußgesteuerte Schleife, Bedingung wird das erste mal nach dem ersten Schleifendurchlauf geprüft. do-while
*/

/* 7 */
/* while ist kopfgesteuert,dowhile fußgesteuert.
Kopfgesteuert beduetet vorprüfend, fußgesteuert nachprüfend. 
dowhile-Schleife läuft immer einmalig bevor die Bedinung geprüft wird. */

/* 8 */
/* Anzahl der Durchläufe/Wiederholungen bekannt.  */

/* 9 */
/* Anzahl der Durchläufe/Wiederholungen nicht bekannt oder der Abbruch erfolgt auf Grund einer vorher definierten Bedingung die zu einem unbekannten Zeitpunkt auftretten kann.   */

/* 10 */
for (var i = 1; i <= 10; i++) {
  console.log(i);
}

var i = 1;
/* Das Semikolon vor und nach der Bedingung müssen stehen bleiben, ohne erhalten wir einen Syntax-Error */
for (; i <= 10; ) {
  console.log(i);
  i++;
}

/* Einfacher: */
while (i <= 10) {
  console.log(i);
  i++;
}

/* 11 */
/* Beide Schleifen können alle Aufgaben erledigen. Die while ist flexibler und kann für syntaktischen Zucker verwendet werden. */

var sum = 0;
while (sum < 4) {
  sum += Math.random();
}
console.log(sum);

/* Die while Schleife ist flexibler weil die Syntax einfacher zu lesen ist wenn nur mit einer Bedinung bei unbekannter Anzahl Durchläufe gearbeitet wird. */

// var zeichen;
// var eingabe;
// do {
//   zeichen = '123';
//   alert('Zeichen: ' + zeichen);
//   eingabe = prompt('Bitte Zeichen wiederholen');
// } while (eingabe === zeichen);

// var zeichen = '';
// var eingabe;
// /*  */
// while (eingabe === zeichen) {
//   zeichen = '123';
//   alert('Zeichen: ' + zeichen);
//   eingabe = prompt('Bitte Zeichen wiederholen');
// }

// var zahl = 3;
// var eingabe;
// while (eingabe !== zahl) {
//   eingabe = prompt('Bitte Zahl raten');
// }

// var zahl = 3;
// var eingabe;
// do {
//   eingabe = prompt('Bitte Zahl raten');
// } while (eingabe !== zahl);

console.log('***********************************');
/* 52 Zeichen */
for (var i = 0; i < 11; i++) {
  console.log(i);
}

var i = 0;
while (i++ < 10) {
  console.log(i);
}

var i = 11;
while (i--) {
  console.log(i);
}

var sum = 0;
while (sum < 4) {
  sum += Math.random();
}
console.log(sum);

for (sum = 0; sum < 4 /* bleibt leer */; ) {
  sum += Math.random();
}
console.log(sum);

//////////////////////////////////////////////////////
var html = '<h3>Ausgabe der Zeichenfolgen mit while</h3>';
// 1,2,3,4,5,6,7,8,9,10
var i = 1;
while (i < 10) {
  html += i + ', ';
  i++;
}
// Zahlen von 1 bis 9 mit Komma in der Schleife ausgeben
// i ist nach der Schleife 10 und kann ohne Komma ausgegeben werden
html += i + '<hr>';

i = 1;
while (i <= 10) {
  html += i;
  // Nach jeder Zahl außer der letzten ein Komma ausgeben
  if (i < 10) {
    html += ', ';
  }
  i++;
}
html += '<hr>';

i = 1;
while (i <= 10) {
  if (i > 1) {
    // Vor jeder Zahl außer der ersten ein Komma ausgeben
    html += ', ';
  }
  html += i;
  i++;
}
html += '<hr>';

i = 1;
while (i <= 10) {
  //Postinkrement nutzt den Wert von i und erhöht dann, nicht so gut zu lesen wie ein abgetrenntes i++ in den vorherigen Beispielen
  html += i++ + ', ';
}
console.log(html);
console.log(html.slice(0, -2));
// Nach der Schleife, im String letztes Leerzeichen und Komma abschneiden
html = html.slice(0, -2);
html += '<hr>';

// 2,4,6,8,10
i = 2;
while (i <= 10) {
  if (i > 2) {
    html += ', ';
  }
  html += i;
  i += 2; // i = i + 2;
}

html += '<hr>';

// 49,42,35,28,21
i = 49;
while (i >= 21) {
  if (i < 49) {
    html += ', ';
  }
  html += i;
  i -= 7; // i = i - 7;
}

html += '<hr>';

// 1,2,4,8,16,32,64,128,256,512,1024
i = 1;
while (i <= 1024) {
  if (i > 1) {
    html += ', ';
  }
  html += i;
  i *= 2; // i = i * 2;
}

html += '<hr>';
html += '<h3>Nutzereingabe und while Schleife</h3>';

var input = prompt('Bitte Anzahl gewünschte Durchläufe eingeben', '10');
var end = Number(input); //=> 10
i = 1;
while (i <= end) {
  if (i > 1) {
    html += ', ';
  }
  html += i;
  i++; // Wichtig!
}

html += '<hr>';
html += '<h3>Kontrollstrukturen Schleifen for</h3>';

// 1,2,4,8,16,32,64,128,256,512,1024
//var -> Duplicate declaration, für das Beispiel gewollt
for (/* var */ i = 1; i <= 1024; i *= 2) {
  if (i > 1) {
    html += ', ';
  }
  html += i;
}

html += '<hr>';

// 49,42,35,28,21
for (i = 49; i >= 21; i -= 7) {
  if (i < 49) {
    html += ', ';
  }
  html += i;
}

html += '<hr>';
html += '<h3>Nutzereingabe und for Schleife</h3>';

/* var */ input = prompt('Bitte Anzahl gewünschte Durchläufe eingeben', '10');
/* var */ end = Number(input); //=> 10
for (/* var */ i = 1; i <= end; i++) {
  if (i > 1) {
    html += ', ';
  }
  html += i;
}

html += '<hr>';
html += '<h3>Sternchen ausgeben</h3>';

var input; //duplicate declartion
var end; //duplicate declartion

var check = true;
while (check /* === true */) {
  input = prompt('Bitte Anzahl gewünschte Durchläufe eingeben', '55');
  end = Number(input);
  console.log(input);

  // WENN input GLEICH null, wurde ABBRECHEN gedrückt...
  if (input === null) {
    //... Die Schleife beenden (ausschalten)
    check = false;
  } else if (isNaN(input) /* === true */) {
    alert('Eingabe muss eine Zahl sein');
  } else if (end > 55 || end < 1) {
    alert('Bitte nur Zahlen von 1 bis 55 eingeben');
  } else {
    //Eingabe ist eine gültige Zahl im Bereich 1 bis 55. Schleife kann beendet werden (ausschalten)
    check = false;
  }
}
//hier geht es nach der Schleife weiter.
/**
 * Erste Idee if-Anweisung die alle Umbrüche abfragt
 */
// for(var i = 1; i <= end;i++) {
//   html += '*';
//   if(
//     i === 10 || //=> **********    // Umbruch 10
//     i === 19 || //=> *********     // Umbruch 9
//     i === 27 || //=> ********      // Umbruch 8
//     i === 34 || //=> *******       // Umbruch 7
//     i === 40 ||
//     i === 45 ||
//     i === 49 ||
//     i === 52 ||
//     i === 54    //=> **
//     ) {
//     html += '<br>';
//   }
// }

/**
 * Vereinfachen mit Hilfsvariable Umbruch
 */
var umbruch = 10;
var anzahl = 0;
for (var i = 1; i <= end; i++) {
  html += '*';
  anzahl++;
  if (anzahl === umbruch) {
    html += '<br>';
    umbruch--;
    anzahl = 0;
  }
}

/**
 * Umbruch und Modulo
 */
// var umbruch = 10;
// for(var i = 1; i <= end;i++) {
//   html += '*';
//   if(i % umbruch === 0 ) {
//     html += '<br>';
//     umbruch = umbruch - 0.5;
//     // 1 Zeile 10 % 10 === 0
//     // 2 Zeile 19 % 9.5 === 0
//     // 3 Zeile 27 % 9 === 0
//     // ...
//     // 10 Zeile 55 % 5.5 === 0
//   }
// }

html += '<hr>';
// Äußer Schleife läuft 1 bis 55 und gibt jeweils nach der Zeile das br aus
for (var i = 1; i <= end; i++) {
  // Innere Schleife gibt jeweils die Zeile aus, die um 1 verringert wird
  for (var j = 10; j >= i; j--) {
    html += '*';
  }
  html += '<br>';
}

document.write(html);
