'use strict';
/* task1: 1 Eigenschaften und Attribute auslesen/schreiben */
function task1() {
  function markLink(clickedLink) {
    /* Alle Elemente mit der Klasse marker einlesen. 
    Wurde die Seite gerade neugeladen, gibt es kein Element mit der Klasse marker. Nach dem ersten Klick auf einen der Links, hat das angeklickte Element die Klasse marker.  */
    var marker = document.querySelector('.marker');
    /* marker enthält entweder ein Objekt oder null. Objekt im if ist true, null wird zu false. Wenn es ein Objekt gibt an ... */
    if (marker) {
      /* ... an diesem Objekt den Klassennamen leeren.  */
      marker.className = '';
    }
    /* Am aktuell angeklickten Element die Klasse marker setzten. */
    clickedLink.className = 'marker';
  }

  function generateHTML(clickedLink) {
    var html = 'Die id des geklickten links <b>' + clickedLink.id + '</b><br>';
    html += 'Die Zahl <b>' + clickedLink.id.split('-')[1] + '</b><br>';
    html += 'Der Textknoten: <b>' + clickedLink.innerHTML + '</b><br>';
    html +=
      'Er wurde durch JavaScript mit der Klasse <b>' +
      clickedLink.className +
      '</b> versehen.';
    document.querySelector('#ausgabe').innerHTML = html;
  }

  function handleClick() {
    /* Wird die referenzierte FN vom Browser aufgerufen, erzeugt er this autmomatisch. this ist das Element, an dem bzw. durch das die FN aufgerufen wurde. */
    markLink(this);
    generateHTML(this);
    return false; // Standardverhalten verhindern
  }

  function bindHandler() {
    var elemente = document.querySelectorAll('#einstieg ul a');
    console.log(elemente);
    var element;
    for (var i = 0; i < elemente.length; i++) {
      element = elemente[i];
      /* FN handleClick wird referenziert. Registriert der Browser einen Klick auf das Element ruft er die FN auf und erzeugt this. */
      element.onclick = handleClick;
    }
  }

  bindHandler();
  /* div#ausgabe leeren. Inhalt zwischen Start- und Endtag (innerHTML) entfernen.*/
  document.querySelector('#ausgabe').innerHTML = '';
}
/* task2: 2 Geburtstagsliste */
function task2() {
  function handleSubmit() {
    /* Formularfelder für JS verfügbar machen */
    var inputVorname = document.querySelector('#vorname');
    var inputNachname = document.querySelector('#nachname');
    var inputGeburtstag = document.querySelector('#geburtstag');
    var divFehler = document.querySelector('#fehler');
    var fehler = false;

    /* Hier wird der Fehlercontainer vor jeder neuen Prüfung geleert.*/
    divFehler.innerHTML = '';

    if (inputVorname.value.trim() === '') {
      // alert('Vorname darf nicht leer sein');
      divFehler.innerHTML += 'Vorname darf nicht leer sein<br>';
      fehler = true;
    }
    if (inputNachname.value.trim() === '') {
      // alert('Nachname darf nicht leer sein');
      divFehler.innerHTML += 'Nachname darf nicht leer sein<br>';
      fehler = true;
    }
    if (inputGeburtstag.value.trim() === '') {
      // alert('Geburtstag darf nicht leer sein');
      divFehler.innerHTML += 'Geburtstag darf nicht leer sein<br>';
      fehler = true;
    }

    console.log(divFehler.innerHTML);

    /* Wenn es keine Fehler gibt, ist der Container #fehler leer */
    if (fehler === false) {
      /* Liste verfügbar machen */
      var liste = document.querySelector('#liste');
      /* Neues Listenelement als Zeichenkette anlegen */
      var newLi =
        '<li>' +
        inputVorname.value.trim() +
        ' ' +
        inputNachname.value.trim() +
        ' ' +
        inputGeburtstag.value.trim() +
        '</li>';
      /* Stringverkettung: Neues Listenelement zu bestehenden innerHTML der ul#liste hinzufügen */
      liste.innerHTML = liste.innerHTML + newLi;
      /* Alle INPUT-Elemente zurücksetzten */
      inputVorname.value = '';
      inputNachname.value = '';
      inputGeburtstag.value = '';
    }
    return false; // Standardverhalten (submit)
  }

  function bindHandler() {
    document.querySelector('#dom form').onsubmit = handleSubmit;
  }

  bindHandler();
}
/* task3: 3 Bildwechsler (beim laden und onmouseover) */
function task3() {
  function generateRandomSrc() {
    var images = ['bild1.jpg', 'bild2.jpg', 'bild3.jpg'];
    var randomImage = images[Math.floor(Math.random() * images.length)];
    return '../../../lib/img/nicht_lustig/' + randomImage;
  }

  function changeImage() {
    this.src = generateRandomSrc();
  }

  function bindHandler() {
    img.onmouseover = changeImage;
  }

  var img = document.querySelector('#bilder img');
  img.src = generateRandomSrc();
  bindHandler();
}

function init() {
  //Code wird ausgeführt wenn Seite vollständig geladen wurde
  task1();
  task2();
  task3();
}

/* onload-Event: Seite wurde vollständig geladen.
Hier wird die FN init auf das onload-Event refernziert. 
Der Browser ruft die FN init auf, wenn er die Seite vollständig geladen hat.  */
window.onload = init;

//////////////////////////////////////////////////////////////////////////////
/* return false in der auf das Event refernzierten FN verhindert das Standardverhalten. Bei Hyperlinks die Weiterleitung zum href Ziel und bei Formularen den submit zu action Ziel.


HTML:
<input type="text" id="vorname" name="vorname" placeholder="Max">

DOM: 
<input type="text" id="vorname" name="vorname" placeholder="Max">

JS: 
var input_vorname = document.querySelector('#vorname');

JS-Objekt-Typ-Elmenent, JS-Objekt-Typ-Input-Element. 
Alle Attribut die dieses HTML-Element (input) haben könnte werden im Objekt repräsentiert. Alle Attribute die einen Wert haben, haben auch Werte in der Eigenschaft. Zusätzlich werden spezielle DOM-Eigenschaft hinzugefügt, z.B. innerHTML.



button[type=button] //=> Standardverhalten (schön aussehen!)
button[type=submit] //=> Standardverhalten (submit/Weiterleitung)

input[type=button] //=> Standardverhalten (schön aussehen!)
input[type=submit] //=> Standardverhalten (submit/Weiterleitung)

a[href=http://www.google.de] //=> Standardverhalten (Weiterleitung)
a[href=#] //=> Standardverhalten (Weiterleitung: Springt an Seitenanfang)

 */

// window.onload = function () {
//   'use strict';
//   //Code wird ausgeführt wenn Seite vollständig geladen wurde
//   console.log(document.querySelector('h2'));
// };
