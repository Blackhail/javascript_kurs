'use strict';
console.log('hallo test');
function init() {
    var html = '';

    var person = {
        name: 'Xavier',
        vorname: 'Charles',
        hobbys: ['Mutanten retten' , 'fremde Gedanken stalken' , 'Weltfrieden']
    };
    console.log(person.name);
    console.log(person.vorname);
    console.log(person.hobbys);

    console.log(person['name']);
    console.log(person['vorname']);
    console.log(person['hobbys']);
    // console.log(person[2]);

    var keys = Object.keys(person);
    var values = Object.values(person);

    html += '<b>Object.keys(person)</b>' + '<ul><li>' + keys.join('</li><li>') + '</li></ul>';
    html += '<b>Object.values(person)</b> ' + '<ul><li>' + values.join('</li><li>') + '</li></ul>';

    for (var key in person) {
        var keys = Object.keys(person);
        console.log(key, ' ', person[key]);

        html += 'key: ' + key + " <b>|</b> person[key] -> person['" + key + "'] //=> " + person[key] + '<br>';
    }

    for (var key in person) {
        console.log(key, ' : ', person[key]); // hier geht die Variante mit Punktnotation nicht!!
      }

    document.querySelector('#ausgabe').innerHTML = html;

}



window.onload = init;