'use strict';
///////////////////////////////////////////////////////////////////
/* Lernskript Funktionen  */
////////////////////////////////////////////////////////////////////
/* 1 */
/* Funktionsdeklaration könnte theoretisch aufgerufen werden bevor Sie im Skript deklariert wurde. Dies sollte aber vermieden werden. Funktionsausdruck in Variable ist durch Hoisting beeinflusst und kann nicht vor der Initalisierung der Variable aufgerufen erden  */

/* Deklaration */
function namederFunktion() {}
/* Aufruf: Name der FN gefolgt von FN-Call-Operator */
namederFunktion();

var keineFunktion = 12;
// keineFunktion(); //=> keineFunktion is not a function

/* 2 */
/* Funktionssignatur bezieht sich auf die Funktionsdeklaration.
1. Schlüsselwort function
2. Bezeichner (Name der Funktion)
3. Runde Klammern für Parameter, Klammern müssen auch ohne Parameter gesetzt werden
4. Geschweifte Klammern, der Funktionskörper. Sammlung von Anweisungen die bei Aufruf ausgeführt werden.
5. Optional der return, Standardvalue ist undefined 
 */

function nameOfFn1(param1, param2) {
  return 'value';
}

var nameOfVarForFN = function (param1, param2) {
  return value;
}; // Hier muss ein SEMIKOLON gesetzt werden

/* 3 */
/* Parameter werden bei der Deklaration als Platzhalter für die Argumente angegeben. Argumente sind die Werte die beim Aufruf der Funktion in die Parameter übergeben werden. Die Parameter werden in der Reihenfolge befüllt in der Sie beim Aufruf als Argumente notiert worden sind. Gibt es für einen Parameter kein Argument ist der Wert im Parameter innerhalb der FN undefined.*/

/* platzhalter ist ein Parameter der durch das Argument befüllt wird */
function xyz(platzhalter) {
  // Hier kann man auch über arguments auf die Argumente zugreifen
  // arguments[0] hat den selben Inhalt wie platzhalter
}

xyz(12); //=> Zahl 12 ist das Argument
var str = 'hello';
xyz(str); //=> Der Inhalt von str 'hello' ist das Argument, kurz str ist das Argument, der Inhalt von str landet in parameter

/* 4 */
/* Standard return einer FN ist undefined. Wenn durch uns oder bereits bei vordefinierten Funktionen nicht anders angegeben bleibt der Returnwert undefined. */

function myFn123() {}

console.log(myFn123()); //=> undefined

/* 5 */
/* Keine Parameter bei der Funktionsdeklaration angeben und eine beliebige Anzahl von Argumenten in die FN übergeben. Dort landen diese wie alle überschüssigen in einem arrayartigen Objekt <arguments> */

//  function sum2() {
//     var erg = 0;
//     for(var i = 0; i < arguments.length; i++) {
//       erg += arguments[i]; // erg = erg + arguments[i];
//     }
//     return erg;
//  }

//  console.log( sum2( 12,5,6,78,69,10 ) );

// function sum2() {
//   var numbers;

//   if(Array.isArray(arguments[0])) {
//     numbers = arguments[0];
//   } else {
//     numbers = arguments;
//   }

//   var erg = 0;
//   for(var i = 0; i < numbers.length; i++) {
//     erg += numbers[i];
//   }
//   return erg;
// }

function sum2(numbers) {
  if (Array.isArray(numbers) === false) {
    numbers = arguments;
  }
  var erg = 0;
  for (var i = 0; i < numbers.length; i++) {
    erg += numbers[i];
  }
  return erg;
}

console.log(sum2(12, 5, 6, 78, 69, 10));
console.log(sum2([12, 5, 6, 78, 69, 10]));

function sum3(zahl1, zahl2, ...zahlen) {
  // console.log(zahl1,zahl2,zahlen);
  var erg = zahl1 + zahl2;
  if (zahlen.length) {
    for (var i = 0; i < zahlen.length; i++) {
      erg += zahlen[i];
    }
  }
  return erg;
}

console.log(sum3(12, 5));
console.log(sum3(12, 5, 6, 78, 69, 10));

/* 6 */
/* Alles was außerhalb einer Funktion deklariert wurde befindet sich im globalen Gültigkeitsbereich/Geltungsbereich. 
Variablen die global erzeugt wurden sind automatisch auch Eigenschaften im window-Objekt. */

console.log(window); //=> Globaler Gültigkeitsbereich/Geltungsbereich
console.log(window.hoisting, hoisting);

var hoisting;
/* 7 */
/* Alles was innerhalb einer Funktion deklariert wird befindet sich im lokalen Gültigkeitsbereich der umgebenden Funktion. 
Als Beispiel dient hier Zeile 98 die Variable erg aus FN sum3.
Diese Variable lokal, nur innerhalb der FN während dieser abgearbeitet wird verfügbar.  */

/* 8 */
/* Abhängig von der Verwendung des Schlüsselwortes var.
Wir eine Variable mit var innerhalb der FN deklariert ist sie lokal.
Ohne var wird erst nach einer Variablen im globalen Bereich gesucht: STICHTWORT: TIEFENSUCHE!
Wird im globalen Bereich keine gefunden wird eine globale Variable deklariert.  */

var x = 'Global';
function doSillyStuff1() {
  /* Ohne var wird keine lokale Variable angelegt sondern die globale Variable überschrieben! */
  x = 'BLUB'; // Zugriff auf die globale Variable
}

console.log(x); //=> 'Global'
console.log(doSillyStuff1()); //=> undefined
console.log(x); //=> 'BLUB'

console.log('************** ');

var x = 'Global';
function doSillyStuff2(x) {
  /* Dieses x bezieht sich auf dem Parameter x der FN und nicht auf die globale Variable x. Gibt es einen Parameter x wird der Parameter überschrieben und nicht die globale Variable. GIBT ES EINEN PARAMETER x wird nicht nach einer globalen Variable gesucht!*/
  x = 'BLUB';
  return x;
}

console.log(x); //=> 'Global'
/* return in FN gibt den Wert zurück aber nicht die Variable */
console.log(doSillyStuff2()); //=> 'Blub'
console.log(x); //=> 'Global'

/* 9 */
/* Eine Funktionsreferenz ist eine Zeiger/Pointer auf die FN. FN-Referenzen werden z.B. in einer Variable gespeichert oder als Argument in eine andere FN übergeben. */

function doStuff() {
  console.log('Hello World from doStuff');
}

/* Eine Funktionsreferenz, hier keine runden Klammern. Es findet keine Funktionsaufruf durch den Funktionsaufrufsoperater () statt. */
var referenz = doStuff;

referenz();

function rufeUebergebeneFnAuf(funktion) {
  funktion('Test aus rufeUebergebeneFnAuf');
}

rufeUebergebeneFnAuf(console.log);
// rufeUebergebeneFnAuf(alert);

/* 10 */
/* Eine Funktion die keinen Namen hat.
1. Ein Funktionsausdruck der einer Variablen zugewiesen wird. */
var x = function () /* x */ {};

/* Anonyme FN als Referenz */
console.log(function () {
  return 'JavaScript ist CRAZY!';
});
/* Anonyme FN die aufgerufen wird. */
console.log(
  (function () {
    return 'JavaScript ist CRAZY!';
  })()
);

/* Anonyme FN können nicht direkt im Code notiert werden, Sie müssen entweder:
1. Eine Variabe zugewiesen werden
2. Einer FN übergeben werden 
3. In Klammern stehen */

/* Anonyme FN im Quellcode, kann später nicht mehr aufgerufen werden da Sie keinen Namen hat */
(function () {});

/* IIFE: Immediately-invoked Function Expression.
Ein sich selbstaufrufender Funktionsausdruck */
(function () {
  console.log('Anyonyme FN hat sich selbst aufgerufen');
})();

/* 11 */
/* Eine FN kann als Argument in eine anderen FN übergeben werden. 
Die FN wird als Referenz übergeben und nicht aufgerufen! */

/* 12 */
/*  */

// function add(a = 1, b = 2) {
//   return a + b;
// }

function add(/* a = 1, b = 2 */) {
  if (a === undefined) {
    a = 1;
  }

  if (b === undefined) {
    b = 2;
  }

  return a + b;
}

console.log(add());

console.log('*******************');
/* Objektdatentypen: Object, Array, Function, null.
Komplexe Datentypen */
var x1 = [1, 2, 3]; //=> Datei
var x2 = x1; //=> Verknüpfung/Alias

console.log(x1); //=> [ 1, 2, 3 ]
console.log(x2); //=> [ 1, 2, 3 ]
x2.push(4);
console.log(x1); //=> [ 1, 2, 3, 4 ]
console.log(x2); //=> [ 1, 2, 3, 4 ]

function myFn111() {} // Datei
/* Funktionsaufruf! Bezeichner gefolgt von FN-Call-Operator */
myFn111();

/* Funktionsreferenz: Kein FN-Call-Operator */
var referenzFN = myFn111; // Verknüpfung/Alias

function ascending(no1, no2) {
  console.log(no1, no2);
  return no1 > no2;
}

var zahlen = [12, 89, 63, 25, 12, 40, 210, 4000];
/* sort ist Arraymethoden, Funktion eines Objekts */
console.log(zahlen.sort());
/* Funktionsreferenz ascending wird an Methode sort übergeben. 
Vordefiniert ist: 
sort arbeitet mit eine Sortieralgorithmus, abhängig vom Browser. Die callBack-FN die sort übergeben wird hat 2 Parameter. sort übergibt automatisch immer die beiden aktuellen Zahlen an die FN-Referenz (ascending) bis die Sortierung abgeschlossen wurde. 
https://de.wikipedia.org/wiki/Bubblesort*/
console.log(zahlen.sort(ascending));

console.log(
  zahlen.sort(function (no1, no2) {
    console.log(no1, no2);
    return no2 > no1;
  })
);

function print(value, index, array) {}

zahlen.forEach(print);

zahlen.forEach(function print(value, index, array) {
  console.log(value, index);
});

zahlen.forEach(function (value, index, array) {});

zahlen.sort(function (a, b) {
  return Math.random() - 0.5;
});
console.log('***************************************');
/* while mit indexOf läuft nur so oft, wie wir das gesuchte Element haben. */
console.log(zahlen);
var min = Math.min(...zahlen);
console.log(min);
var indizes = [];
/* indexOf gibt entweder die INdexposition des Suchwertes zurück oder wenn keiner gefunden wird -1 */
/* Die erste Indexposition auf der min also unsere 12 vorkommt*/
var pos = zahlen.indexOf(min);
/* Suche solange Treffer gefunden werden. -1 ist der Rückgabewert von indexof wenn kein 12 mehr gefunden wird */
while (pos !== -1) {
  indizes.push(pos);
  /* pos+1 bedeutet: Suche ab der Indexpositon 1 hinter dem letzten Treffers. Ohne +1 finden wir immer wieder die erste Position */
  pos = zahlen.indexOf(min, pos + 1);
}
console.log('Inidzes: ', indizes);
console.log('***************************************');
/* Nachteil: forEach läuft über alle Indizes */
var min = Math.min(...zahlen);
var indizes = [];
zahlen.forEach(function print(value, index, array) {
  if (value === min) {
    indizes.push(index);
  }
});

console.log('***************************************');

/* Funktionsaufruf! Bezeichner gefolgt von FN-Call-Operator */
referenzFN(); //=> myFn111();

/* Elementardatentypen (String, Number, Boolean, undefined, NaN).Einfache Datentypen */
var y1 = 12;
var y2 = y1; //=> var y2 = 12;
/* Der Wert von y1 wird in y2 kopiert. */
console.log(y1); //=> 12
console.log(y2); //=> 12
y1 = 100;
console.log(y1); //=> 100
console.log(y2); //=> 12
