'use strict';
/////////////////////////////////////////////////////////////////////
/* Lernskript Arrays  */
/////////////////////////////////////////////////////////////////////
/* 1 */
/* Literalschreibweise */
var data = [];
/* Konstrukturschreibweise */
var data2 = new Array();

console.log([12]);
console.log(new Array(12));

/* 2 */
/* Die Eigenschaft .length */

/* 3 */
/* Anzahl der Element (length) minus 1.
Der letzte Index ist immer um 1 kleiner als die Anzahl der Elemente. */

/* 4 */
/* Eckigen Klammern/Computed-Member-Access

array[0] 

*/

/* 5 */
/* Eckigen Klammern/Computed-Member-Access in Kombination mit dem Zuweisungsoperator und einem Wert der zugewiesen wird.

array[0] = 'Hello World' 

VORSICHT: PHP-Schreibweise mit leere Klammer funktionert nicht.
array[] = 'Funktinoniert nicht'

*/

/* 6 */
/* for-Schleife. i++.
Initialisierung Zählervariable: Startwert wo beginnen wir.
Inkrementierung bzw. Dekrementierung ändert die Schrittfolge.
i++ beginnend bei 0: Von 0 jeden Index
i+=2 beginnend bei 0: Von jeden zweiten Index*/

//Index          0         1        2         3         4        5
var staedte = ['Berlin', 'Paris', 'New York', 'Tokyo', 'Amsterdam', 'Wien'];

for (var i = 0; i < staedte.length; i++) {
  var element = staedte[i];
  console.log(element);
}
console.log('************************');
for (var i = 0; i < staedte.length; i += 2) {
  var element = staedte[i];
  console.log(element);
}

console.log('************************');
for (var i = 1; i < staedte.length; i += 2) {
  var element = staedte[i];
  console.log(element);
}

console.log('************************');
var personal = [
  ['Jon', 'Snow', 12345, 'max@mustermann.de'],
  ['Barbara', 'Meier', 22245, 'b@meyer.de'],
  ['Max', 'Herre', 54321, 'b@meyer.de'],
];

for (var i = 0; i < personal.length; i++) {
  var person = personal[i];
  console.log(person[2]);
}

console.log('************************');
var csv =
  'Jon, Snow, 12345, max@mustermann.de, Barbara, Meier, 22245, b@meyer.de, Max, Herre, 54321, b@meyer.de';

var arrayAusCSV = csv.split(', ');
console.log(csv);
console.log(arrayAusCSV);
/* Personalnummer beginnen auf Index 2 alle 4 Schritte */
for (var i = 2; i < arrayAusCSV.length; i += 4) {
  console.log(arrayAusCSV[i]);
}

/* 7 */
var staedteCopy = staedte.slice();
console.log(staedteCopy);

/* VORSICHT: Mehrdimensionale Arrays werden mit slice nicht kopiert.
Hier wird nur eine Referenz auf den Datenspeicher im neuen Array abgelegt. In example und exampleCopy zeigen beide Arrays auf den gleichen Speicher. */
var example = [
  'a', // index 0
  'b', // index 1
  [
    // index 2
    'x', // index 2 index 0
    'y', // index 2 index 1
  ],
];

var exampleCopy = example.slice();
console.log(example);
console.log(exampleCopy);

exampleCopy[2][0] = 'X';

console.log(example);
console.log(exampleCopy);

/* Workarround: 
Die Methode`stringify wandelt das Objekt/Array im Speicher example in eine Zeichenkette um. Die Zeichenkette hat keinen Bezug zum ursprünglichen Speicherplatz. Die Methode parse macht aus der JSON-Zeichenkette ein JS Objekt/Array das wir durch Zuweisung in einem neuen Speicherplatz ablegen.

realCopy ist eine 1 zu 1 Kopie von example aber ein andere Speicherplatz!*/
var realCopy = JSON.parse(JSON.stringify(example));
realCopy[2][0] = 'Z';
console.log(example);
console.log(realCopy);

/* 8 */
/* unshift () funktioniert wie array.push(), aber beginnt am Anfang des Arrays. array.unshift () kann ein oder mehrere Elemente am Anfang des Arrays einfügen.

Übergibt der Aufruf unshift() mehrere Argumente, werden die Element von rechts nach links an den Anfang des Arrays eingefügt.t */
var a = ['a', 'b', 'c', 'd'];
a.unshift('x', 'y', 'z');
console.log(a);

/* 9 */
//Index        0       1        2         3         4        5
staedte = ['Berlin', 'Paris', 'New York', 'Tokyo', 'Amsterdam', 'Wien'];
console.log(staedte.splice(2, 2)); //=> [ "New York", "Tokyo" ]
console.log(staedte); //=> [ "Berlin", "Paris", "Amsterdam", "Wien" ]
/* 
array.splice(index, howmany, item1, ....., itemX) 

index : Startposition im Array das bearbeitet wird
howmany : Gibt an wieviele Elemente entfernt werden 
         Wert auf 0 setzen wenn nur eingefügt werden soll
item1-X : Die Werte die ab Startposition hinzugefügt werden.

Entfernte Elemente werden als Array zurückgegeben, auch wenn nur ein Element entfernt wird handelt es sich um ein Array.
*/

/* delete:
Möglich aber erzeugt ein lückenhaftes Array. */
staedte = ['Berlin', 'Paris', 'New York', 'Tokyo', 'Amsterdam', 'Wien'];
delete staedte[2];
delete staedte[3];
console.log(staedte);

/* Ein lückenhaftes Array: */
var x = ['a'];
x[1] = 'b'; //=> ['a','b']
x[2] = 'c'; //=> ['a','b','c']
x[5] = 'd'; //=> ['a','b','c',,,'d']
/* ['a','b','c',,,'d'] also ['a','b','c',undefined,undefined,'d'] */

/* 10 */
//Index        0       1        2         3         4        5
staedte = ['Berlin', 'Paris', 'New York', 'Tokyo', 'Amsterdam', 'Wien'];
staedte.splice(3, 0, 'Moskau', 'London');
//=> [ "Berlin", "Paris", "New York", "Moskau", "London", "Tokyo", "Amsterdam", "Wien" ]
console.log(staedte);

/* 11 */
staedte = ['Berlin', , 'New York', 'Tokyo', , 'Wien', , , ,];
console.log(staedte);
for (var i = 0; i < staedte.length; i++) {
  var element = staedte[i];
  if (element !== undefined) {
    console.log(element);
  }
}

console.log('*************************');
/* forEach überspringt lücken Automatisch */
staedte.forEach(function (element) {
  console.log(element);
});

console.log('*************************');
/* for in ist ein Objektschleife und sollte bei Arrays mit Vorsicht angewendet werden. Hier nutzen wir den Seiteneffekt das undefined Werte nicht angezeigt zu unserem Vorteil, dies kann jedoch auch zu Problem führen. */
for (var key in staedte) {
  console.log(staedte[key]);
}

/* 12 */
/* Siehe 7, wurde dort bereits beantwortet */

/* 13 */
/* Eigenschaft length des Arrays auf 0 setzen.

array.length = 0; 
array = [];
*/

/* 14 */
/* Siehe 9, wurde dort bereits beantwortet */

/* Array mit vordefinierten Plätzen anlegen und mit gleichem Wert befüllen. */
var counter = [];
counter.length = 6;
console.log(counter);
counter.fill(0);
console.log(counter);

console.log(['a', 'b', 'c', 'd', 'e', 'c'].indexOf('z')); //=>  -1
console.log(['a', 'b', 'c', 'd', 'e', 'c'].indexOf('c')); //=> 2
console.log(['a', 'b', 'c', 'd', 'e', 'c'].lastIndexOf('c')); //=> 5
console.log(['a', 'b', 'c', 'd', 'e', 'c'].includes('c')); //=> true
/////////////////////////////////////////////////////////////////////
/* Aufgaben */
/////////////////////////////////////////////////////////////////////
/* 1 */

var html = '';

var haustiere = [
  'Hund', //=> 0
  'Katze', //=> 1
  'Adler', //=> 2
  'Meerschweinchen', //=> 3
  'Hamster', //=> 4
  'Amsel', //=> 5
  'Maus', //=> 6
  'Ratte', //=> 7
  'Fink', //=> 8
  'Kaninchen', //=> 9
]; //=> length 10

/* 
Meerschweinchen, Hamster, Maus, Ratte
array[index]
Eckige Klammern (Computed Member Access)
*/

html = html + '<h2>Aufgabe 1 a</h2>';
html = html + haustiere[3] + ', ';
html = html + haustiere[4] + ', ';
html = html + haustiere[6] + ', ';
html = html + haustiere[7] + '<br>';

console.log(haustiere[(3, 4, 6, 7)]); //=> 'Ratte'
/* 
haustiere[3, 4, 6, 7]  
JavaScript wird immer von innen nach außen ausgeführt
haustiere als Bezeichner (Kein Wertigkeit)
[] Operator 
3, 4, 6, 7
, Sequenz wird von links nach rechts ausgeführt
haustiere[7]
*/
html = html + '<h2>Aufgabe 1 b</h2>';
/* 
Adler, Amsel, Fink
for-Schleife
Start bei Index 2 und ab dort jedes 3 Element
*/

for (var i = 2; i < haustiere.length; i += 3) {
  if (i !== 2) {
    html = html + ', ';
  }
  html = html + haustiere[i];
  /* Kurzschreibweise (Syntax Zucker) */
  // html += (i !== 2 ? ', ' : '') + haustiere[i];
}

html = html + '<hr>';

/* Mit Hilfsvariable temp, einem Array, benötigen wir in der Schleife keine Logik für die Ausgabe der Kommata.* */
var temp = [];
for (var i = 2; i < haustiere.length; i += 3) {
  temp.push(haustiere[i]);
}
/* * durch join wird das Array in einem String konvertiert und mit dem Trenner der join übergeben wurde getrennt. */
html = html + temp.join(', ');
/* Join kann simple HTML-Strukturen sehr einfach erzeugen */
html = html + '<ul><li>' + temp.join('</li><li>') + '</li></ul>';

html = html + '<h2>Aufgabe 1 c</h2>';

// var randomIndex = rand(0, haustiere.length - 1);
var randomIndex = Math.floor(Math.random() * haustiere.length);
html = html + 'Zufälliges Haustier: ' + haustiere[randomIndex] + '<hr>';
document.write(html);
