'use strict';
console.log('test');


function init() {

////////// Lösung 1 /////////////////////////////////////////////////////

// function schaltjahr(jahr){
//     var j = parseInt(jahr);
//     if (j % 4 == 0)
//      {if (j % 100 == 0)
//       {if (j % 400 == 0) {return "ist Schaltjahr";}
//                     else {return "ist kein Schaltjahr";}
//       }
//       else {return "Schaltjahr";}
//      }
//     else {return "kein Schaltjahr";};
//     }

// function daysInMonth(month, year) {

//     // function Schaltjahr: mit IF prüfen, antsprechend array füllen
//     var months = []; // 1>31/2>28... ODER 1>31/2>29...

//     // for(var i=heute.getFullYear();i <= heute.getFullYear()+1 ;i++) {
//         if (schaltjahr(year)) {
//             months = {1:31,2:29,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31};
//         } else {
//             months = {1:31,2:28,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31};
//         }
        
// console.log(months);
// return months[month-1];

// }
// console.log(daysInMonth(2, 2019));

// var date = new Date(2019, 1, 1);


////////// Lösung 2 /////////////////////////////////////////////////////


// var year = '20';
// var month = '2';
// var  wieVieletage = new Date(year, month) 
// /* console.log(wieVieletage);  */

// console.log(wieVieletage.getUTCDate()); 

///////// Lösung 3 ////////////////////////////////////////////////////////////////////

function getDayInMonth(month, year) {
    
    var d = new Date(year, month, 0);
    return d.getDate(); // Den letzten Tag auslesen
  }

  function getDayInMonth(month, year) {
    // Nächster Monat Tag 0 setzen, dadurch bekommen wir den letzten Tag des Vormonats
    // 2 wird übergeben und hier gesetzt: Jahr 2020 Monat März
    // 0 als Tag korrigiert das Date-Objekt das Datum auf den letzten Tag des Vormonats
    var d = new Date(year, month, 0);
    return d.getDate(); // Den letzten Tag auslesen
  }

  function task1() {
    console.log(getDayInMonth(2, 2020));
    console.log(getDayInMonth(6, 1982));
    /* Moment.js */
    console.log(moment('2019-01').daysInMonth());
    console.log(moment('2019-02').daysInMonth());
    console.log(moment('2020-02').daysInMonth());

    var ausgabe = document.querySelector('#ausgabe');
    var date = moment(); // Aktuelles Datum (Client)
    var birthday = moment('1982-06-14'); // Geburtsdatum

    ausgabe.innerHTML =
      'Das heutige Datum: ' +
      date.format('DD.MM.YYYY') +
      '<br>Mein Geburtsdatum: ' +
      birthday.format('DD.MM.YYYY') +
      '<br>Mein Alter: ' +
      date.diff(birthday, 'year') +
      '<br>';
  }

  
  function countdownTimer() {
    /* Ausgabecontainer einlesen */
    var countdown = document.querySelector('#countdown');
    /* Zieldatum über moment-js mit der Methode set setzen. */
    var countDownDate = moment().set({
      year: 2021,
      month: 0, //=> Mai ist Monat 5-1 = 4 (0-Januar, ..., 11-Dezember)
      date: 1,
      hours: 0,
      minutes: 0,
      seconds: 0,
    });
    /* Aktuelle Zeit beim Laden der Seite mit moment-js bestimmen */
    var now = moment();
    /* Die Methode unix() des moment-Objekts wandelt ein moment-Datums-Objekt in einem Timestamp um */
    var diffTime = countDownDate.unix() - now.unix();
    var duration = moment.duration(diffTime * 1000, 'milliseconds');
    var interval = 1000;

    var x = setInterval(function () {
      duration = moment.duration(duration - interval, 'milliseconds');

      /* Hier ggf. führende Null für Stunden, Minuten und Sekunden einfügen. */
      var days = parseInt(duration.asDays());
      var hours = duration.hours();
      var minutes = duration.minutes();
      var seconds = duration.seconds();

      countdown.innerHTML =
        days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's ';

      if (duration.asSeconds() <= 0) {
        clearInterval(x);
        countdown.innerHTML = 'EXPIRED';
      }
    }, interval);
  }

  task1();
  countdownTimer();



} ///////// Ende init ////////////////////////////////////////////////////////

window.onload = init;


