'use strict';

function square(x) {
  return x * x;
}

function numberCompare(a, b) {
  if (a > b) {
    return 'Erster Wert x ist größer als zweiter Wert y';
  } else if (b > a) {
    return 'Zweiter Wert y ist größer als erster Wert x';
  } else {
    return 'Zahlen sind gleich';
  }
}

function randomRGB() {
  var rgb = [];
  while (rgb.length < 3) {
    rgb.push(rand(0, 255));
  }
  return 'rgb(' + rgb + ')';
}

console.log(randomRGB());
document.write(
  '<p><b style="color:' + randomRGB() + '">Das ist ein Testtext</b></p>'
);

// function stringreverse(string) {
//   // string
//   //=> 'webmaster'
//   var arrayAusString = string.split('');
//   //=> [ "w", "e", "b", "m", "a", "s", "t", "e", "r" ]
//   var umgedrehtesArray = arrayAusString.reverse();
//   //=> [ "r", "e", "t", "s", "a", "m", "b", "e", "w" ]
//   var stringAusArray = umgedrehtesArray.join('');
//   //=> 'retsambew'
//   return stringAusArray;
// }

function stringreverse(string) {
  var temp = '';
  for (var i = string.length - 1; i >= 0; i--) {
    console.log(i, string[i]);

    temp += string[i];
  }
  return temp;
}

// function ucfirst(s) {
//   var a = s.split('');
//   a[0] = a[0].toUpperCase();
//   return a.join('');
// }

function ucfirst(s) {
  return s.slice(0, 1).toUpperCase() + s.slice(1);
}

function ucwords(s) {
  var words = s.split(' ');
  for (var i = 0; i < words.length; i++) {
    words[i] = ucfirst(words[i]);
  }
  return words.join(' ');
}

function sum(array) {
  var sum = 0;
  var len = array.length; //Anzahl Element im Array
  for (var i = 0; i < len; i++) {
    // array[i] enthält nacheinander alle Elemente/Wert aus dem Array
    sum += array[i];
  }
  return sum;
}

function minInArray(array) {
  var min = array[0];
  var len = array.length; //Anzahl Element im Array
  for (var i = 1; i < len; i++) {
    if (array[i] < min) {
      min = array[i];
    }
  }
  return min;
}

function maxInArray(array) {
  var max = array[0];
  var len = array.length; //Anzahl Element im Array
  for (var i = 1; i < len; i++) {
    if (array[i] > max) {
      max = array[i];
    }
  }
  return max;
}

console.log(minInArray([2, 1, 3, 4, 5, 6, 7, 9, 8, 10]));
console.log(minInArray([0, 2, 1, 3, 4, 5, -6, 7, 9, 8, -10]));
console.log(maxInArray([2, 1, 3, 4, 5, 6, 7, 9, 8, 10]));
console.log(maxInArray([1, 55, 896, 10000]));

/**
 *  Math.max und Math.min
 * Durch den Spread-Operator kann diesen Funktionen ein Array
 * übergeben werden. VORSICHT: Browserkompatibilität beachten! */
console.log(Math.max(...[0, 2, 1, 3, 4, 5, -6, 7, 9, 8, -10]));
console.log(Math.min(...[0, 2, 1, 3, 4, 5, -6, 7, 9, 8, -10]));

/**
 * sort
 * Alternativ kann man das Array sortieren und erhält somit im
 * ersten und letzten Index den kleinsten und größten Wert
 * sort arbeite mit Positiv, Negativ und Nullwert.
 * return a - b
 * a - b = Ergebnis Positive Zahl : a ist größer als b
 * a - b = Ergebnis Negative Zahl : b ist größer als a
 * a - b = Ergebnis Null : a und b sind gleich
 *
 * sort nutze keinen bubblesort aber das Prinzip ist hier sehr
 * schön dargestellt:
 * https://de.wikipedia.org/wiki/Bubblesort
 */
var num = [0, 2, 1, 3, 4, 5, -6, 7, 9, 8, -10];
var temp = num.slice().sort(function (a, b) {
  // console.log(a,b);
  return a - b;
});
var min = temp[0];
var max = temp[temp.length - 1];
console.log(temp, min, max);

// function escapeHTML(string) {
//   // String in Array umwandeln, Stringindizes sind identisch mit Arrayindizes
//   var temp = string.split('');
//   var len = temp.length;
//   for (var i = 0; i < len; i++) {
//     switch (temp[i]) {
//       case '&':
//         temp[i] = '&amp;'
//         break;
//       case '<':
//         temp[i] = '&lt;';
//         break;
//       case '>':
//         temp[i] = '&gt;';
//         break;
//       case '"':
//         temp[i] = '&quot;';
//         break;
//       case "'":
//         temp[i] = '&apos;';
//         break;
//       // default:
//       //   temp[i] = temp[i];
//     }
//   }
//   return temp.join('');
// }

// function escapeHTML(string) {
//   // String in Array umwandeln, Stringindizes sind identisch mit Arrayindizes
//   var temp = string.split('');
//   var len = temp.length;
//   /* Das Objekt hat eine Key/Schlüssel (Assoziativer Index), dieser entspricht dem Zeichen das wir austauschen möchten. Im Schlüssel liegt der Wert gegen der das Zeichen ausgetauscht werden muss. */
//   var entities = {
//     '&': '&amp;',
//     '<': '&lt;',
//     '>': '&gt;',
//     '"': '&quot;',
//     '\'': '&apos;'
//   }

//   for (var i = 0; i < len; i++) {
//     // Aktuelles Zeichen in Variable sign speichern
//     var sign = temp[i]
//     /* Wenn das Zeichen im Objekt entities als Key/Schlüssel (Assoziativer Index) vorkommt ist die Bedinung true.
//     Kommt das Zeichen nicht vor haben wir if(undefined) also false. */
//     if (entities[sign]) {
//       /* Tausche im Array temp auf Index i das Zeichen (sign) gegen die HTML-Entität aus Objekt Key/Schlüssel (Assoziativer Index) sign. */
//       temp[i] = entities[sign];
//     }
//   }
//   // Gibt das Array als String zurück.
//   return temp.join('');
// }

function escapeHTML(string) {
  /* FIXME: Das geht noch einfacher, kommt noch. */
  return string
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/\'/g, '&apos;');
}

function singleLetterCount(word, sign) {
  var count = 0;
  var pos = word.indexOf(sign);
  while (pos !== -1) {
    count++;
    pos = word.indexOf(sign, pos + 1);
  }
  return count;
}

console.log(singleLetterCount('Schifffahrtsgesellschaft', 'f'));

var string =
  '<img src="lib/img/java-vs-javascript.jpg" alt="Iceberg of weird shit">';
console.log(escapeHTML(string));

var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var erg = numbers.reduce(function (a, b) {
  console.log(a, b);
  return a + b;
});
console.log(erg);

console.log(
  numbers.sort(function (a, b) {
    return a - b;
  })[0]
);

console.log(
  numbers.sort(function (a, b) {
    return a - b;
  })[numbers.length - 1]
);

// function(){} ist ein Funktionsausdruck, wird hier auf onsubmit refernziert
/* $('#myForm').onsubmit = function(){
  // Der Code im Körper der Funktion wird onsubmit aufgerufen
  var eingabe = $('#eingabe').value.trim(); //document.querySelector('#eingabe)
  if(eingabe !== '') {
    $('#ausgabe').innerHTML =  stringReverse(eingabe);
  }

  return false; //Verhindert den submit
};
 */

document //=> Aktuelles Dokument (HTML)
  .querySelector('#myForm').onsubmit = function () {
  //=> Element mit #myForm selektieren //=> submit-Event des Formulars abfangen...
  //... referenziert FN aufrufen *
  // Variable eingabe anlegen und Wert aus value speichern (String)
  var eingabe = document //=> Aktuelles Dokument (HTML)
    .querySelector('#eingabe') //=> Element mit #eingabe selektieren
    .value //=> Wert aus Attribut value auslesen
    .trim(); //=> Valuewert (String) trimen

  /**
   *  Variable ausgabe speichert eine Referenz auf das HTML-Element #ausgabe
   *  Der Rückgabewert der Methode/Funktion querySelector ist ein JS-Objekt-Typ-Element
   * Wir speichern hier ein Objekt in der Variable ausgabe
   * */
  var ausgabe = document //=> Aktuelles Dokument (HTML)
    .querySelector('#ausgabe'); //=> Element mit #ausgabe selektieren
  //Wenn der String in Variable eingabe UNGLEICH einem Leerstring ist
  //Wenn im input#eingabe etwas eingegeben wurde...
  if (eingabe !== '') {
    /**
     *  ... ändert die Eigenschaft innerHTML (Textcontent zwischen Start- und Endtag) über das Objekt gespeichert in ausgabe.
     * ausgabe enthält eine Liverepräsentation des HTML-Element als JS-Objekt
     * Ablauf:
     * Ausführung von innen nach außen in Operatorenrangfolge (Wertigkeit)
     * Höchste Priorität hat der FN-Aufrufsoperator ()
     * Der Wert aus der Variable eingabe wird ausgelesen und in die FN stringReverse übergeben. Die FN stringReverse arbeitet ihren Funktionskörper ab und das return gibt den Wert an den Aufruf hier zurück.
     * Danach wird die Zuweisung ausgeführt und der zurückgebene String in die Eigenschaft innerHTML des Objekts in ausgabe geschrieben.
     * Sichtbarer Effekt im Browser: Inhalt in #ausgabe hat sich geändert.
     * */

    ausgabe.innerHTML = stringReverse(eingabe);
  } else {
    // input#eingabe enthielt einen Leerstring, wir löschen den Inhalt von #ausgabe.
    ausgabe.innerHTML = '';
  }
  return false; //*  submit verhindern
};

///////////////////////////////////////////////////////////////////////////////////////////
/* Funktionen benutzen */
///////////////////////////////////////////////////////////////////////////////////////////
var html = '';

html += '<h2>square (quadrat)</h2>';
html += 'square(4) //=> ' + square(4) + '<br>';
html += 'square(square(2)) //=> ' + square(square(2)) + '<br>';

html += '<h2>numberCompare</h2>';
html += 'numberCompare(4,4) //=> ' + numberCompare(4, 4) + '<br>';
html += 'numberCompare(1,4) //=> ' + numberCompare(1, 4) + '<br>';
html += 'numberCompare(4,1) //=> ' + numberCompare(4, 1) + '<br>';

html += '<h2>randomRGB</h2>';
html += 'randomRGB() //=> ' + randomRGB() + '<br>';

html += '<h2>stringreverse </h2>';
html +=
  "stringreverse('webmaster') //=> " + stringreverse('webmaster') + '<br>';

html += '<h2>ucfirst</h2>';
html += "ucfirst('webmaster') //=> " + ucfirst('webmaster') + '<br>';

html += '<h2>ucwords</h2>';
html +=
  "ucfirst('webmaster akademie javascript baustein') //=> " +
  ucfirst('webmaster akademie javascript baustein') +
  '<br>';

html += '<h2>sum</h2>';

html += '<h2>minInArray</h2>';
html += '<h2>maxInArray</h2>';
html += '<h2>escapeHTML</h2>';
html += '<h2>singleLetterCount</h2>';
html += '<h2>Array sortieren</h2>';

document.write(html);
