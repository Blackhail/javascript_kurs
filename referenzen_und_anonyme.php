<!DOCTYPE html>
<html lang="de">

<head>
  <meta charset="UTF-8">
  <title>Playground</title>
  <script src="lib/js/repositories/helpers.js"></script>
  <!-- <script src="lib/js/repositories/helpers.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->


</head>

<body>
  <?php

  ?>
  <script>
  function sum(kaesekuchen) {
    var erg = 0;
    for (var i = 0; i < kaesekuchen.length; i++) {
      erg += kaesekuchen[i];
    }
    return erg;
  }

  var zahlen = [12, 89, 63, 25, 12, 40, 210, 2, 4000];


  console.log(sum(zahlen));
  console.log(sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));

  /* Funktsrefernz auf Funktion sum. 
  WICHTIG: Kein Aufruf mit () */
  console.log(sum);
  /* Funktionsreferenz wird in Variable gespeichert */
  var mySum = sum;

  console.log(mySum([25, 36, 12]));
  /* Funktionsreferenz: Funktionsausdruck wird einer Variable zugewiesen */
  var quadrat = function(x) {
    return x * x;
  };

  /* Funktionsaufruf */
  console.log(quadrat(4));
  console.log(quadrat(12));
  /* Funktsrefernz auf Funktion quadrat. 
  WICHTIG: Kein Aufruf mit () */
  console.log(quadrat);


  // function print(value, index, array) {
  //   console.log(value);
  // }

  // zahlen.forEach(print);

  console.log('***********************');
  /* Anonyme FN wird als Referenz übergeben.
  Der Aufruf der übergebenen FN erfolgt im forEach, für uns nicht sichtbar. */
  zahlen.forEach(function(value, index, array) {
    console.log(value);
  });
  /* Im Browser, innerhalb der JS implementierung, ist hinterlegt das der Methode forEach eine Funktion als Referenz übergeben werden kann. Weiterhin ist vordefinert das diese FN 3 Parameter haben kann/darf! 
  
  Intern läuft hier ein for-Schleife die die Callback-FN aufruft und folgende Parameter befüllt. 
  
  1. Array-Element aus aktuellen Schleifendurchlauf 
  2. Schleifenzähler des aktuellen Schleifendurchlaufs
  3. Ist das Array über das die Schleife läuft 
    */

  function rufeCallBackAuf(callback) {
    callback('Hello World');
  }

  // rufeCallBackAuf(alert);
  // rufeCallBackAuf(prompt);
  // rufeCallBackAuf(confirm);
  rufeCallBackAuf(console.log);
  // rufeCallBackAuf(document.write.bind(document));
  console.log('***********************');

  var x = 'Global';
  var y = 'Global2';

  function xyz1(x) {
    /* Lokale Variable y in xyz1 */
    var y = 'FOOOBAAARRR!';
  }

  console.log(y); //=> 'Global2'
  xyz1();
  console.log(y); //=> 'Global2'


  var x = 'Global';
  var y = 'Global2';

  function xyz2(x) {
    /* Überschreibung der globalen Variable. Innerhalb der FN greifen wir auf die globale Variable zu */
    y = 'FOOOBAAARRR!';
  }

  console.log(y); //=> 'Global2'
  xyz2();
  console.log(y); //=> 'FOOOBAAARRR!'

  var x = 'Global';
  var y = 'Global2';

  function xyz3(x) {
    /* x ist ein Parameter!  Hier kann man nicht auf die globale Variable x zugreifen, weil man nicht am Parameter vorbei kommt. */
    x = 'FOOOBAAARRR!';
  }

  console.log(x); //=> 'Global'
  xyz3();
  console.log(x); //=> 'Global'

  var x = 'Global';

  function xyz4(x) {
    /* x ist ein Parameter!  Hier kann man nicht auf die globale Variable x zugreifen, weil man nicht am Parameter vorbei kommt. */
    x = 'FOOOBAAARRR!';
    return x;
  }

  console.log(x); //=> 'Global'
  console.log(xyz4(x)); //=> 'FOOOBAAARRR!'
  console.log(x); //=> 'Global'

  var numbers = [12, 89, 63, 25, 12, 40, 210, 2, 4000];

  function asc(no1, no2) {
    return no1 > no2;
  }
  console.log(numbers.sort(asc));
  console.log(numbers.sort(function(no1, no2) {
    return no2 > no1;
  }));


  function klingtkomisch() {

  }

  if (klingtkomisch()) {
    console.log('landen wir hier');
  } else {
    console.log('..ist aber so!');
  }


  function doStuffNewBrowser(a = 12, b = 0) {}


  function doStuffOldBrowser(a, b) {
    a = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 12;
    b = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  }



  // var a;
  // if (arguments.length > 0 && arguments[0] !== undefined) {
  //   a = arguments[0];
  // } else {
  //   a = 12;
  // }

  /* Elementare Datentypen */
  var str = 'Hello';
  var no = 12;
  var bool = true;
  var und = undefined;

  /* Objektdatentypen */
  var array = [1, 2, 3];
  var obj = {
    x1: 12,
    x2: 30
  }
  var fn = function() {};



  /* callByValue:
  Der Wert der Variable str wird in die FN übergeben und landet dort im Parameter a.
  Hier wird eine Kopie des Inhalts von str in a gespeichert! KOPIE! */
  function fn1(a) {
    a = '';
  }

  console.log(str); //=> 'Hello'
  fn1(str);
  console.log(str); //=> 'Hello'

  /* callByReference: 
  In Parameter b wird eine Referenz auf den Speicherplatz von Variable array gelegt. 
  Ändert man b ändert sich das Original array außerhalb auch. */
  function fn2(b) {
    array.pop();
    array.pop();
  }
  console.log(array); //=> [ 1, 2, 3 ]
  fn2(array);
  console.log(array); //=> [ 1 ]


  /* Referenzierung: Objekte werden referenziertt */
  var x1 = [1, 2, 3]; // Datei
  var x2 = x1; // Verknüfung/Alias
  x2.push(4);
  console.log(x1);
  console.log(x2);

  /* Kopieren des Wertes: Elementartypen werden kopiert */
  var y1 = 12;
  var y2 = y1; //=> var y2 = 12;

  console.log(y1);
  console.log(y2);
  y2 = 100;
  console.log(y1);
  console.log(y2);
  </script>

</html>