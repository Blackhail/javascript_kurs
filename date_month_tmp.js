'use strict';
function init() {
  'use strict';
  // Globale alle Moment Datumsobjekt auf Deutsch setzen
  moment.locale('de');

  function getDayInMonth(month, year) {
    // Nächster Monat Tag 0 setzen, dadurch bekommen wir den letzten Tag des Vormonats
    // 2 wird übergeben und hier gesetzt: Jahr 2020 Monat März
    // 0 als Tag korrigiert das Date-Objekt das Datum auf den letzten Tag des Vormonats
    var d = new Date(year, month, 0);
    return d.getDate(); // Den letzten Tag auslesen
  }

  function task1() {
    console.log(getDayInMonth(2, 2020));
    console.log(getDayInMonth(6, 1982));
    /* Moment.js */
    console.log(moment('2019-01').daysInMonth());
    console.log(moment('2019-02').daysInMonth());
    console.log(moment('2020-02').daysInMonth());

    var ausgabe = document.querySelector('#ausgabe');
    var date = moment(); // Aktuelles Datum (Client)
    var birthday = moment('1982-06-14'); // Geburtsdatum

    ausgabe.innerHTML =
      'Das heutige Datum: ' +
      date.format('DD.MM.YYYY') +
      '<br>Mein Geburtsdatum: ' +
      birthday.format('DD.MM.YYYY') +
      '<br>Mein Alter: ' +
      date.diff(birthday, 'year') +
      '<br>';
  }

////////// Countdown Timer Var 1 /////////////////////////////////////////////////////////////
//   function countdownTimer() {
//     var countdown = document.querySelector('#countdown');
//     var countDownDate = new Date('Jan 1, 2021 00:00:00').getTime();

//     var x = setInterval(function () {
//       var now = new Date().getTime();

//       var distance = countDownDate - now; // berechnet Differenz

//       var days = Math.floor(distance / (1000 * 60 * 60 * 24));
//       var hours = Math.floor(
//         (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
//       );
//       var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
//       var seconds = Math.floor((distance % (1000 * 60)) / 1000);

//       countdown.innerHTML =
//         days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's ';
//       if (distance < 0) {
//         clearInterval(x);
//         countdown.innerHTML = 'EXPIRED';
//       }
//     }, 1000);
//   }

  ////////// Countdown Timer Var 2 /////////////////////////////////////////////////////////////
  function countdownTimer() {
    /* Ausgabecontainer einlesen */
    var countdown = document.querySelector('#countdown');
    /* Zieldatum über moment-js mit der Methode set setzen. */
    var countDownDate = moment().set({
      year: 2021,
      month: 0, //=> Mai ist Monat 5-1 = 4 (0-Januar, ..., 11-Dezember)
      date: 1,
      hours: 0,
      minutes: 0,
      seconds: 0,
    });
    /* Aktuelle Zeit beim Laden der Seite mit moment-js bestimmen */
    var now = moment();
    /* Die Methode unix() des moment-Objekts wandelt ein moment-Datums-Objekt in einem Timestamp um */
    var diffTime = countDownDate.unix() - now.unix();
    var duration = moment.duration(diffTime * 1000, 'milliseconds');
    var interval = 1000;

    var x = setInterval(function () {
      duration = moment.duration(duration - interval, 'milliseconds');

      /* Hier ggf. führende Null für Stunden, Minuten und Sekunden einfügen. */
      var days = parseInt(duration.asDays());
      var hours = duration.hours();
      var minutes = duration.minutes();
      var seconds = duration.seconds();

      countdown.innerHTML =
        days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's ';

      if (duration.asSeconds() <= 0) {
        clearInterval(x);
        countdown.innerHTML = 'EXPIRED';
      }
    }, interval);
  }

  task1();
  countdownTimer();
}
window.onload = init;
