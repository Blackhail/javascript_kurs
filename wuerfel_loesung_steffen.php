<!DOCTYPE html>
<html lang="de">

<head>
  <meta charset="UTF-8">
  <title>Playground</title>
  <script src="lib/js/repositories/helpers.js"></script>
  <!-- <script src="lib/js/repositories/helpers.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->


</head>

<body>
  <?php

  ?>
  <script>
  //zahl         1  2  3  4  5  6
  //index        0  1  2  3  4  5  
  // var zaehler = [0, 0, 0, 0, 0, 0];
  // var random;
  // var index;
  // // for(var i = 0; i < 100; i++) {}
  // for (var i = 1; i <= 100; i++) {
  //   random = Math.floor(Math.random() * 6) + 1;
  //   /* Der Index ist um 1 kleiner als die Zahl, deren Häufigkeit er erfasst. */
  //   index = random - 1;
  //   zaehler[index]++;
  // }


  // // var min = Math.min(...zaehler);
  // /* https://johnresig.com/blog/fast-javascript-maxmin/ */
  // var min = Math.min.apply(Math, zaehler);

  // /* Wir gehen davon aus das der erste Wert im Array der kleinste ist.  */
  // var min = zaehler[0];
  // for (var i = 1; i < zaehler.length; i++) {
  //   if (zaehler[i] < min) {
  //     min = zaehler[i];
  //   }
  // }
  // console.log(zaehler);
  // console.log(min);

  /* zaehler zum testen fix einstellen: */
  var zaehler = [16, 16, 17, 18, 19, 17];
  var min = Math.min.apply(Math, zaehler);
  var minIndizes = [];
  /* Läuft über das Array zaehler und prüft im Anweisungblock in welchen Indizes der Wert von min vorkommt.  */
  for (var i = 0; i < zaehler.length; i++) {
    /* WENN der aktuelle Wert im Arrayindex GLEICH dem ermittelen Minimum in min ist... */
    if (zaehler[i] === min) {
      /* ...schreibe diesen Index in das Array minInidzes */
      minIndizes.push(i);
    }
  }
  console.log(zaehler);
  console.log(min);
  console.log(minIndizes);

  for (var i = 0; i < minIndizes.length; i++) {
    console.log('Die Zahl ' + (minIndizes[i] + 1) + ' kam ' + zaehler[minIndizes[i]] + 'x vor');
  }



  ///////////////////////////////////////////////
  console.log('******************************');
  var sum; //=> undefined
  sum = sum + 1; //=> NaN
  console.log(sum);


  var sum = 0; //=> 0
  sum = sum + 1; //=> 1
  console.log(sum); //=> 1

  // for (var i = 1; i <= 100; i++) {
  //   random = Math.floor(Math.random() * 6) + 1;
  //   /* Der Index ist um 1 kleiner als die Zahl, deren Häufigkeit er erfasst. */
  //   index = random - 1;
  //   zaehler[index]++;
  //   /* if-Anweisung */
  //   // if (random === 1) zaehler[0] = zaehler[0] + 1;
  //   // if (random === 2) zaehler[1] = zaehler[1] + 1;
  //   // if (random === 3) zaehler[2] = zaehler[2] + 1;
  //   // if (random === 4) zaehler[3] = zaehler[3] + 1;
  //   // if (random === 5) zaehler[4] = zaehler[4] + 1;
  //   // if (random === 6) zaehler[5] = zaehler[5] + 1;

  //   /* switch */
  //   // switch (random) {
  //   //   case 1:
  //   //     zaehler[0]++;
  //   //     break;
  //   //   case 2:
  //   //     zaehler[1]++;
  //   //     break;
  //   //   case 3:
  //   //     zaehler[2]++;
  //   //     break;
  //   //   case 4:
  //   //     zaehler[3]++;
  //   //     break;
  //   //   case 5:
  //   //     zaehler[4]++;
  //   //     break;
  //   //   case 6:
  //   //     zaehler[5]++;
  //   //     break;
  //   // }


  // }


  ///////////////////////////////////////////////
  </script>

</html>