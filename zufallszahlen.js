'use strict';
var html = '';
console.log(Number);
console.log(Math);

var random = Math.random(); //=> [0;1[
html += 'var random = Math.random(); //=> ' + random + '<br>';

random = random * 10; //=> [0;10[

/* Erzwungenes Abrunden [0;9] */
html += 'Math.floor(random) //=> ' + Math.floor(random) + '<br>';

/* Erzwungenes Aufrunden [1;10] */
html += 'Math.ceil(random) //=> ' + Math.ceil(random) + '<br>';

/* Mathematisch Runden bis 0.4 abrunden ab 0.5 aufrunden [0;10] */
html += 'Math.round(random) //=> ' + Math.round(random) + '<hr>';

var min = 5;
var max = 10;
var erg = Math.floor(Math.random() * (max - min + 1)) + min;

html += 'var min = 5;<br>var max = 10;<br>';
html +=
  'var erg = Math.floor(Math.random() * (max - min)) + min; //=> ' +
  erg +
  '<br>';

html += '<hr>';
html += 'rand(1,10); //=> ' + rand(1, 10) + '<br>';
html += 'rand(100,1000); //=> ' + rand(100, 1000) + '<br>';
html += 'rand(25,50); //=> ' + rand(25, 50) + '<br>';

document.write(html);

var min = 5;
var max = 10;
var erg;
for (var i = 0; i < 100; i++) {
  erg = Math.floor(Math.random() * (max - min + 1)) + min;
  console.log(erg);
}

/* 
Math.floor(Math.random() * (max - min + 1)) + min

Math.random() //=> [0;1[

(max - min)
max-min+1 -> 10-5+1 -> 6

Math.random() * (max - min+1)
[0;1[ * 6 //=> [0;5] also 0|1|2|3|4|5 

Math.floor(Math.random() * (max - min+1))  [0;5] (Ganzzahl)

0|1|2|3|4|5 

0 + 5 = 5
1 + 5 = 6
2 + 5 = 7
3 + 5 = 8
4 + 5 = 9
5 + 5 = 10

*/

console.log('****************************************************');
////////////////////////////////////////////////////////////
/* Abrunden, Nachkommastellen verschwinden */
console.log(Math.floor(4.5)); //=> 4
/* In Ganzzahl umwandeln, Nachkommastellen werden abgeschnitten */
console.log(parseInt(4.5)); //=> 4

var x = 4.5;
x = Math.floor(x);
x = 4.5;
x = parseInt(x);
